export default restSendTX;
/**
 * 实现在Node端通过RestAPI向RepChain节点提交已签名的交易数据
 *
 * @param {Buffer | string} tx 待提交的已签名交易数据，可为Buffer类型或hex编码的String类型
 * @param {String} address RepChain节点的所提供的Restful API网络地址
 * @returns {Promise<any>} RepChain节点的反馈信息
 * @memberof RestAPI
 * @private
 */
declare function restSendTX(tx: Buffer | string, address: string): Promise<any>;
