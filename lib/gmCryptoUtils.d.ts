export default GMCryptoUtils;
declare class GMCryptoUtils {
    constructor(gmWebsocketServerAddress: any);
    websocketServerAddress: any;
    sendMessageArray: any[];
    getGMHashVal(plainData: any, cb: any): void;
    getGMSm3HashValCB: any;
    sm3PlainData: any;
    getGMCertificate(userID: any, cb: any): void;
    getGMCertificateCB: any;
    certUserID: any;
    getGMSignature(userID: any, plainData: any, cb: any): void;
    getGMSignatureCB: any;
}
