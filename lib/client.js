"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.Client = exports.RESPONSE_FORMAT = void 0;
// 在package.json中的browser属性中设置{lib/rest.js : lib/browser/rest.js}以使用相应环境下的实现
var lodash_1 = __importDefault(require("lodash"));
var restSendTX_1 = __importDefault(require("./restSendTX"));
var restGet_1 = __importDefault(require("./restGet"));
var rc2_1 = require("./protos/rc2");
var RESPONSE_FORMAT;
(function (RESPONSE_FORMAT) {
    RESPONSE_FORMAT[RESPONSE_FORMAT["JSON"] = 0] = "JSON";
    RESPONSE_FORMAT[RESPONSE_FORMAT["STREAM"] = 1] = "STREAM";
})(RESPONSE_FORMAT = exports.RESPONSE_FORMAT || (exports.RESPONSE_FORMAT = {}));
var Client = /** @class */ (function () {
    /**
     * Creates an instance of RestAPI.
     * <br />
     * 与RepChain区块链节点交互的客户端.
     *
     * @param address RepChain节点的Restful API服务地址
     * @returns Client对象实例
     */
    function Client(address) {
        this._address = this._sanitizeUrl(address);
    }
    Client.prototype._sanitizeUrl = function (address) {
        return address.replace(/\/$/, "");
    };
    /**
     * 获取客户端欲连接的节点Url
     * @returns 客户端欲连接的节点Url
     */
    Client.prototype.getAddress = function () {
        return this._address;
    };
    /**
     * 修改客户端欲连接的节点Url
     * @param address 欲连接的节点Url
     */
    Client.prototype.setAddress = function (address) {
        this._address = this._sanitizeUrl(address);
    };
    /**
     * 获取区块链的当前概要信息
     *
     * @returns Json格式信息
     */
    Client.prototype.chainInfo = function () {
        var url = "".concat(this._address, "/chaininfo");
        return (0, restGet_1["default"])(url).then(function (chainInfo) {
            var blockchainInfoMessage = rc2_1.rep.proto.BlockchainInfo.fromObject(chainInfo);
            return rc2_1.rep.proto.BlockchainInfo.toObject(blockchainInfoMessage, { longs: Number, bytes: String });
        });
    };
    /**
     * 获取区块链最新区块高度
     *
     * @returns 区块高度
     */
    Client.prototype.chainHeight = function () {
        return this.chainInfo().then(function (chainInfo) { return chainInfo.height; });
    };
    /**
     * 获取最新区块哈希值
     *
     * @returns 区块哈希值(base64编码字符串)
     */
    Client.prototype.chainCurrentBlockHash = function () {
        return this.chainInfo().then(function (chainInfo) { return chainInfo.currentBlockHash; });
    };
    /**
     * 获取最新区块的父区块哈希值
     *
     * @returns 最新区块的父区块哈希值(base64编码字符串)
     */
    Client.prototype.chainPreviousBlockHash = function () {
        return this.chainInfo().then(function (chainInfo) { return chainInfo.previousBlockHash; });
    };
    /**
     * 获取当前的世界状态哈希值
     *
     * @returns 世界状态哈希值(base64编码字符串)
     */
    Client.prototype.chainCurrentStateHash = function () {
        return this.chainInfo().then(function (chainInfo) { return chainInfo.currentStateHash; });
    };
    /**
     * 获取区块链中的交易总数量
     *
     * @returns 交易总量
     * @memberof RestAPI
     */
    Client.prototype.chainTotalTransactions = function () {
        return this.chainInfo().then(function (chainInfo) { return chainInfo.totalTransactions; });
    };
    /**
     * 获取区块数据
     *
     * @param id 区块唯一标识，可为区块高度(number)或区块哈希值(base64编码字符串或二进制数据)
     * @param [blockFormat=RESPONSE_FORMAT.JSON] 期望返回的区块数据的格式，可为RESPONSE_FORMAT.JSON(json对象，默认)或RESPONSE_FORMAT.STREAM(二进制数据)
     * @returns 区块数据
     */
    Client.prototype.block = function (id, blockFormat) {
        if (blockFormat === void 0) { blockFormat = RESPONSE_FORMAT.JSON; }
        var blockID = id;
        var url = "".concat(this._address, "/block");
        if (blockFormat === RESPONSE_FORMAT.STREAM) {
            url = "".concat(url, "/stream");
        }
        if (lodash_1["default"].isString(blockID) || Buffer.isBuffer(blockID)) {
            url = "".concat(url, "/hash");
            if (Buffer.isBuffer(blockID))
                blockID = blockID.toString("base64");
        }
        url = "".concat(url, "/").concat(blockID);
        return (0, restGet_1["default"])(url);
    };
    /**
     * 获取交易数据
     *
     * @param id 交易唯一标识，即txid
     * @param [txFormat=REPONSE_FORMAT.JSON] 期望返回的交易数据的格式JSON或STREAM
     * @param [withBlockHeight=false] 返回交易数据中是否包含区块高度，当txFormat=时才有效
     * @returns 交易数据
     */
    Client.prototype.transaction = function (id, txFormat, withBlockHeight) {
        if (txFormat === void 0) { txFormat = RESPONSE_FORMAT.JSON; }
        if (withBlockHeight === void 0) { withBlockHeight = false; }
        var url = "".concat(this._address, "/transaction");
        if (txFormat === RESPONSE_FORMAT.STREAM) {
            url = "".concat(url, "/stream");
        }
        else if (withBlockHeight) {
            url = "".concat(url, "/tranInfoAndHeight");
        }
        url = "".concat(url, "/").concat(id);
        return (0, restGet_1["default"])(url);
    };
    /**
     * 发送签名交易
     *
     * @param tx 待发送的已签名交易数据，支持使用Buffer类型数据或其hex编码字符串数据
     * @returns 接收交易后RepChain节点的返回信息
     */
    Client.prototype.sendTransaction = function (tx) {
        return (0, restSendTX_1["default"])(tx, this._address);
    };
    return Client;
}());
exports.Client = Client;
