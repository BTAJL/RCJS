/// <reference types="node" />
import { rep } from "./protos/rc2";
import txMsgClass = rep.proto.Transaction;
import chaincodeDeployMsgClass = rep.proto.ChaincodeDeploy;
declare type TxType = rep.proto.Transaction.Type;
declare type ISignture = rep.proto.ISignature;
declare type ITransaction = rep.proto.ITransaction;
export interface TxConsArgs {
    /** 交易唯一标识 */
    txid?: string | null;
    /** 交易类型 */
    type: TxType;
    /** 目标合约的名称 */
    chaincodeName: string;
    /** 目标合约的版本号 */
    chaincodeVersion: number;
    /** 交易执行消耗的gas资源上限，允许为null以在序列化时不编码默认值 */
    gasLimit?: number | null;
    /** 目标合约实例，允许为null以在序列化时不编码默认值 */
    oid?: string | null;
}
export interface DeployTxConsArgs extends TxConsArgs {
    chaincodeDeployParams: {
        /** 部署合约过程的超时限制 */
        timeout?: number;
        /** 合约代码 */
        codePackage: string;
        /** 合约规则法律描述 */
        legalProse?: string;
        /** 合约语言类型 */
        codeLanguageType: chaincodeDeployMsgClass.CodeType;
        /** 合约串行/并行方式 */
        runType?: chaincodeDeployMsgClass.RunType;
        /** 合约状态类型 */
        stateType?: chaincodeDeployMsgClass.StateType;
        /** 合约初始化方法参数 */
        initParameters?: string;
    };
}
export interface InvokeTxConsArgs extends TxConsArgs {
    chaincodeInvokeParams: {
        /** 待被调用的合约方法名 */
        chaincodeFunction: string;
        /** 待调用的合约方法的参数 */
        chaincodeFunctionArgs: Array<string>;
    };
}
export interface SetStateTxConsArgs extends TxConsArgs {
    chaincodeSetStateParams: {
        /** 目标合约的新状态，当值为false时表示使该合约无效 */
        state: boolean;
    };
}
export interface SignTxArgs {
    /** 签名者的pem格式私钥 */
    prvKey: string;
    /** 签名者的pem格式公钥 */
    pubKey: string;
    /** 使用的签名算法名称 */
    alg: string;
    /** 私钥解密密码，如果prvKey为已加密的pem格式私钥，则需要提供此解密密码 */
    pass?: string;
    /** 签名者的信用代码 */
    creditCode: string;
    /** 代表签名者的证书名 */
    certName: string;
    /** 签名时使用的签名方法提供者，可为nodecrypto(默认值)或jsrsasign */
    provider?: string;
}
export interface VerifySignatureArgs {
    /** pem格式的公钥 */
    pubKey: string;
    /** 使用的签名算法 */
    alg: string;
    /** 验签时使用的验签方法提供者，可为nodecrypto(默认值)或jsrsasign */
    provider?: string;
}
export declare class Transaction {
    /** 底层protobuf定义的交易对象 */
    private txMsg;
    /** 已签名交易数据 */
    private txSignedBuffer;
    /** 已签名交易的唯一标识（若构建交易时未指定txid，则签名后会自动生成） */
    private txSignedTxid;
    /** 已签名交易数据中的签名数据 */
    private txSignedSignature;
    /**
     * 创建交易对象实例
     * @param iTransaction 创建交易对象所需参数
     */
    protected constructor(iTransaction: ITransaction);
    /**
     * A factory method
     * 创建交易实例
     * @param params 创建交易对象所需参数
     * @returns
     */
    static create(params: TxConsArgs): Transaction;
    /**
     * 解析二进制交易数据，构建交易对象实例
     * @param txBytes 二进制交易数据
     * @returns
     */
    static fromBytes(txBytes: Buffer): Transaction;
    /**
     * 解析json对象格式交易数据，构建交易对象实例
     * @param txJson json格式交易数据
     * @returns
     */
    static fromJson(txJson: {
        [key: string]: any;
    }): Transaction;
    /**
     * 获取交易对象实例内部由protobuf协议定义的底层交易对象
     * @returns 由protobuf协议定义的交易对象
     */
    getTxMsg(): txMsgClass;
    /**
     * 获取交易对象实例经签名后得到的签名交易数据
     * @returns 签名交易数据
     */
    getTxSignedBuffer(): Buffer | null;
    /**
     * 获取已签名交易的唯一标识
     * @returns
     */
    getTxSignedTxid(): string | null;
    /**
     * 获取已签名交易数据中的签名数据
     * @returns
     */
    getTxSignedSignature(): ISignture | null;
    /**
     * 重置交易唯一标识，重置唯一标识后可作为新的不同交易签名后提交给RepChain区块链
     * @param txid 交易唯一标识
     */
    setTxid(txid: string): void;
    /**
     * 获取交易数据的hash值，应只在交易签名方法中调用本方法
     * （因为RepChain交易数据结构中时间戳数据只存在于签名字段（结构体）中，若计算hash值时不包含时间戳数据则不同交易的hash值可能完全相同
     * ，比如调用转账合约方法的两笔交易所调用的合约名、合约版本、合约方法及合约方法参数可能完全相同，但在业务上是两次不同操作）
     * @returns 交易hash值(sha256)(hex format string)
     */
    private _getTxHash;
    /**
     * 获取protobuf协议所定义的时间戳对象
     * @param millis
     * @returns
     */
    private _getTimestamp;
    /**
     * 对新创建的交易对象实例进行签名。产生RepChain区块链可识别的签名交易数据
     * 允许对同一交易对象实例进行多次签名，每次签名将产生新的签名数据并覆盖以前的签名数据，且若交易对象未被指定txid，则每次签名后产生的签名交易具有不同txid，
     * RepChain区块链把具有不同txid的交易视为不同交易
     * @param param0 对交易对象实例进行签名所需参数
     */
    sign({ prvKey, pubKey, alg, pass, creditCode, certName, provider, }: SignTxArgs): void;
    /**
     * 对已被签名的交易对象进行签名验证
     * @param param0 验证交易签名所需参数
     * @returns 验签是否成功
     */
    verifySignature({ pubKey, alg, provider }: VerifySignatureArgs): boolean;
}
export {};
