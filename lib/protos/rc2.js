"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _typeof3 = require("@babel/runtime/helpers/typeof");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.rep = exports.google = exports["default"] = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var $protobuf = _interopRequireWildcard(require("protobufjs/minimal"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof3(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
// Common aliases
var $Reader = $protobuf.Reader,
    $Writer = $protobuf.Writer,
    $util = $protobuf.util; // Exported root namespace

var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
exports["default"] = $root;

var rep = $root.rep = function () {
  /**
   * Namespace rep.
   * @exports rep
   * @namespace
   */
  var rep = {};

  rep.proto = function () {
    /**
     * Namespace proto.
     * @memberof rep
     * @namespace
     */
    var proto = {};

    proto.Event = function () {
      /**
       * Properties of an Event.
       * @memberof rep.proto
       * @interface IEvent
       * @property {string|null} [from] Event from
       * @property {string|null} [to] Event to
       * @property {rep.proto.Event.Action|null} [action] Event action
       * @property {rep.proto.IBlock|null} [blk] Event blk
       */

      /**
       * Constructs a new Event.
       * @memberof rep.proto
       * @classdesc Represents an Event.
       * @implements IEvent
       * @constructor
       * @param {rep.proto.IEvent=} [properties] Properties to set
       */
      function Event(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * Event from.
       * @member {string} from
       * @memberof rep.proto.Event
       * @instance
       */


      Event.prototype.from = "";
      /**
       * Event to.
       * @member {string} to
       * @memberof rep.proto.Event
       * @instance
       */

      Event.prototype.to = "";
      /**
       * Event action.
       * @member {rep.proto.Event.Action} action
       * @memberof rep.proto.Event
       * @instance
       */

      Event.prototype.action = 0;
      /**
       * Event blk.
       * @member {rep.proto.IBlock|null|undefined} blk
       * @memberof rep.proto.Event
       * @instance
       */

      Event.prototype.blk = null;
      /**
       * Creates a new Event instance using the specified properties.
       * @function create
       * @memberof rep.proto.Event
       * @static
       * @param {rep.proto.IEvent=} [properties] Properties to set
       * @returns {rep.proto.Event} Event instance
       */

      Event.create = function create(properties) {
        return new Event(properties);
      };
      /**
       * Encodes the specified Event message. Does not implicitly {@link rep.proto.Event.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.Event
       * @static
       * @param {rep.proto.IEvent} message Event message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Event.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.from != null && Object.hasOwnProperty.call(message, "from")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.from);
        if (message.to != null && Object.hasOwnProperty.call(message, "to")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.to);
        if (message.action != null && Object.hasOwnProperty.call(message, "action")) writer.uint32(
        /* id 3, wireType 0 =*/
        24).int32(message.action);
        if (message.blk != null && Object.hasOwnProperty.call(message, "blk")) $root.rep.proto.Block.encode(message.blk, writer.uint32(
        /* id 4, wireType 2 =*/
        34).fork()).ldelim();
        return writer;
      };
      /**
       * Encodes the specified Event message, length delimited. Does not implicitly {@link rep.proto.Event.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.Event
       * @static
       * @param {rep.proto.IEvent} message Event message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Event.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes an Event message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.Event
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.Event} Event
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Event.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.Event();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.from = reader.string();
              break;

            case 2:
              message.to = reader.string();
              break;

            case 3:
              message.action = reader.int32();
              break;

            case 4:
              message.blk = $root.rep.proto.Block.decode(reader, reader.uint32());
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes an Event message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.Event
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.Event} Event
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Event.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies an Event message.
       * @function verify
       * @memberof rep.proto.Event
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      Event.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.from != null && message.hasOwnProperty("from")) if (!$util.isString(message.from)) return "from: string expected";
        if (message.to != null && message.hasOwnProperty("to")) if (!$util.isString(message.to)) return "to: string expected";
        if (message.action != null && message.hasOwnProperty("action")) switch (message.action) {
          default:
            return "action: enum value expected";

          case 0:
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
          case 7:
          case 8:
          case 9:
          case 10:
          case 11:
            break;
        }

        if (message.blk != null && message.hasOwnProperty("blk")) {
          var error = $root.rep.proto.Block.verify(message.blk);
          if (error) return "blk." + error;
        }

        return null;
      };
      /**
       * Creates an Event message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.Event
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.Event} Event
       */


      Event.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.Event) return object;
        var message = new $root.rep.proto.Event();
        if (object.from != null) message.from = String(object.from);
        if (object.to != null) message.to = String(object.to);

        switch (object.action) {
          case "SUBSCRIBE_TOPIC":
          case 0:
            message.action = 0;
            break;

          case "TRANSACTION":
          case 1:
            message.action = 1;
            break;

          case "BLOCK_NEW":
          case 2:
            message.action = 2;
            break;

          case "BLOCK_ENDORSEMENT":
          case 3:
            message.action = 3;
            break;

          case "ENDORSEMENT":
          case 4:
            message.action = 4;
            break;

          case "MEMBER_UP":
          case 5:
            message.action = 5;
            break;

          case "MEMBER_DOWN":
          case 6:
            message.action = 6;
            break;

          case "CANDIDATOR":
          case 7:
            message.action = 7;
            break;

          case "GENESIS_BLOCK":
          case 8:
            message.action = 8;
            break;

          case "BLOCK_SYNC":
          case 9:
            message.action = 9;
            break;

          case "BLOCK_SYNC_DATA":
          case 10:
            message.action = 10;
            break;

          case "BLOCK_SYNC_SUC":
          case 11:
            message.action = 11;
            break;
        }

        if (object.blk != null) {
          if ((0, _typeof2["default"])(object.blk) !== "object") throw TypeError(".rep.proto.Event.blk: object expected");
          message.blk = $root.rep.proto.Block.fromObject(object.blk);
        }

        return message;
      };
      /**
       * Creates a plain object from an Event message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.Event
       * @static
       * @param {rep.proto.Event} message Event
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      Event.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.from = "";
          object.to = "";
          object.action = options.enums === String ? "SUBSCRIBE_TOPIC" : 0;
          object.blk = null;
        }

        if (message.from != null && message.hasOwnProperty("from")) object.from = message.from;
        if (message.to != null && message.hasOwnProperty("to")) object.to = message.to;
        if (message.action != null && message.hasOwnProperty("action")) object.action = options.enums === String ? $root.rep.proto.Event.Action[message.action] : message.action;
        if (message.blk != null && message.hasOwnProperty("blk")) object.blk = $root.rep.proto.Block.toObject(message.blk, options);
        return object;
      };
      /**
       * Converts this Event to JSON.
       * @function toJSON
       * @memberof rep.proto.Event
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      Event.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };
      /**
       * Action enum.
       * @name rep.proto.Event.Action
       * @enum {number}
       * @property {number} SUBSCRIBE_TOPIC=0 SUBSCRIBE_TOPIC value
       * @property {number} TRANSACTION=1 TRANSACTION value
       * @property {number} BLOCK_NEW=2 BLOCK_NEW value
       * @property {number} BLOCK_ENDORSEMENT=3 BLOCK_ENDORSEMENT value
       * @property {number} ENDORSEMENT=4 ENDORSEMENT value
       * @property {number} MEMBER_UP=5 MEMBER_UP value
       * @property {number} MEMBER_DOWN=6 MEMBER_DOWN value
       * @property {number} CANDIDATOR=7 CANDIDATOR value
       * @property {number} GENESIS_BLOCK=8 GENESIS_BLOCK value
       * @property {number} BLOCK_SYNC=9 BLOCK_SYNC value
       * @property {number} BLOCK_SYNC_DATA=10 BLOCK_SYNC_DATA value
       * @property {number} BLOCK_SYNC_SUC=11 BLOCK_SYNC_SUC value
       */


      Event.Action = function () {
        var valuesById = {},
            values = Object.create(valuesById);
        values[valuesById[0] = "SUBSCRIBE_TOPIC"] = 0;
        values[valuesById[1] = "TRANSACTION"] = 1;
        values[valuesById[2] = "BLOCK_NEW"] = 2;
        values[valuesById[3] = "BLOCK_ENDORSEMENT"] = 3;
        values[valuesById[4] = "ENDORSEMENT"] = 4;
        values[valuesById[5] = "MEMBER_UP"] = 5;
        values[valuesById[6] = "MEMBER_DOWN"] = 6;
        values[valuesById[7] = "CANDIDATOR"] = 7;
        values[valuesById[8] = "GENESIS_BLOCK"] = 8;
        values[valuesById[9] = "BLOCK_SYNC"] = 9;
        values[valuesById[10] = "BLOCK_SYNC_DATA"] = 10;
        values[valuesById[11] = "BLOCK_SYNC_SUC"] = 11;
        return values;
      }();

      return Event;
    }();

    proto.Signer = function () {
      /**
       * Properties of a Signer.
       * @memberof rep.proto
       * @interface ISigner
       * @property {string|null} [name] Signer name
       * @property {string|null} [creditCode] Signer creditCode
       * @property {string|null} [mobile] Signer mobile
       * @property {Array.<string>|null} [certNames] Signer certNames
       * @property {Array.<string>|null} [authorizeIds] Signer authorizeIds
       * @property {Array.<string>|null} [operateIds] Signer operateIds
       * @property {Array.<string>|null} [credentialMetadataIds] Signer credentialMetadataIds
       * @property {Array.<rep.proto.ICertificate>|null} [authenticationCerts] Signer authenticationCerts
       * @property {string|null} [signerInfo] Signer signerInfo
       * @property {google.protobuf.ITimestamp|null} [createTime] Signer createTime
       * @property {google.protobuf.ITimestamp|null} [disableTime] Signer disableTime
       * @property {boolean|null} [signerValid] Signer signerValid
       * @property {string|null} [version] Signer version
       */

      /**
       * Constructs a new Signer.
       * @memberof rep.proto
       * @classdesc Represents a Signer.
       * @implements ISigner
       * @constructor
       * @param {rep.proto.ISigner=} [properties] Properties to set
       */
      function Signer(properties) {
        this.certNames = [];
        this.authorizeIds = [];
        this.operateIds = [];
        this.credentialMetadataIds = [];
        this.authenticationCerts = [];
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * Signer name.
       * @member {string} name
       * @memberof rep.proto.Signer
       * @instance
       */


      Signer.prototype.name = "";
      /**
       * Signer creditCode.
       * @member {string} creditCode
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.creditCode = "";
      /**
       * Signer mobile.
       * @member {string} mobile
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.mobile = "";
      /**
       * Signer certNames.
       * @member {Array.<string>} certNames
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.certNames = $util.emptyArray;
      /**
       * Signer authorizeIds.
       * @member {Array.<string>} authorizeIds
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.authorizeIds = $util.emptyArray;
      /**
       * Signer operateIds.
       * @member {Array.<string>} operateIds
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.operateIds = $util.emptyArray;
      /**
       * Signer credentialMetadataIds.
       * @member {Array.<string>} credentialMetadataIds
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.credentialMetadataIds = $util.emptyArray;
      /**
       * Signer authenticationCerts.
       * @member {Array.<rep.proto.ICertificate>} authenticationCerts
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.authenticationCerts = $util.emptyArray;
      /**
       * Signer signerInfo.
       * @member {string} signerInfo
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.signerInfo = "";
      /**
       * Signer createTime.
       * @member {google.protobuf.ITimestamp|null|undefined} createTime
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.createTime = null;
      /**
       * Signer disableTime.
       * @member {google.protobuf.ITimestamp|null|undefined} disableTime
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.disableTime = null;
      /**
       * Signer signerValid.
       * @member {boolean} signerValid
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.signerValid = false;
      /**
       * Signer version.
       * @member {string} version
       * @memberof rep.proto.Signer
       * @instance
       */

      Signer.prototype.version = "";
      /**
       * Creates a new Signer instance using the specified properties.
       * @function create
       * @memberof rep.proto.Signer
       * @static
       * @param {rep.proto.ISigner=} [properties] Properties to set
       * @returns {rep.proto.Signer} Signer instance
       */

      Signer.create = function create(properties) {
        return new Signer(properties);
      };
      /**
       * Encodes the specified Signer message. Does not implicitly {@link rep.proto.Signer.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.Signer
       * @static
       * @param {rep.proto.ISigner} message Signer message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Signer.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.name != null && Object.hasOwnProperty.call(message, "name")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.name);
        if (message.creditCode != null && Object.hasOwnProperty.call(message, "creditCode")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.creditCode);
        if (message.mobile != null && Object.hasOwnProperty.call(message, "mobile")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).string(message.mobile);
        if (message.certNames != null && message.certNames.length) for (var i = 0; i < message.certNames.length; ++i) {
          writer.uint32(
          /* id 4, wireType 2 =*/
          34).string(message.certNames[i]);
        }
        if (message.authorizeIds != null && message.authorizeIds.length) for (var _i = 0; _i < message.authorizeIds.length; ++_i) {
          writer.uint32(
          /* id 5, wireType 2 =*/
          42).string(message.authorizeIds[_i]);
        }
        if (message.operateIds != null && message.operateIds.length) for (var _i2 = 0; _i2 < message.operateIds.length; ++_i2) {
          writer.uint32(
          /* id 6, wireType 2 =*/
          50).string(message.operateIds[_i2]);
        }
        if (message.credentialMetadataIds != null && message.credentialMetadataIds.length) for (var _i3 = 0; _i3 < message.credentialMetadataIds.length; ++_i3) {
          writer.uint32(
          /* id 7, wireType 2 =*/
          58).string(message.credentialMetadataIds[_i3]);
        }
        if (message.authenticationCerts != null && message.authenticationCerts.length) for (var _i4 = 0; _i4 < message.authenticationCerts.length; ++_i4) {
          $root.rep.proto.Certificate.encode(message.authenticationCerts[_i4], writer.uint32(
          /* id 8, wireType 2 =*/
          66).fork()).ldelim();
        }
        if (message.signerInfo != null && Object.hasOwnProperty.call(message, "signerInfo")) writer.uint32(
        /* id 9, wireType 2 =*/
        74).string(message.signerInfo);
        if (message.createTime != null && Object.hasOwnProperty.call(message, "createTime")) $root.google.protobuf.Timestamp.encode(message.createTime, writer.uint32(
        /* id 10, wireType 2 =*/
        82).fork()).ldelim();
        if (message.disableTime != null && Object.hasOwnProperty.call(message, "disableTime")) $root.google.protobuf.Timestamp.encode(message.disableTime, writer.uint32(
        /* id 11, wireType 2 =*/
        90).fork()).ldelim();
        if (message.signerValid != null && Object.hasOwnProperty.call(message, "signerValid")) writer.uint32(
        /* id 12, wireType 0 =*/
        96).bool(message.signerValid);
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 13, wireType 2 =*/
        106).string(message.version);
        return writer;
      };
      /**
       * Encodes the specified Signer message, length delimited. Does not implicitly {@link rep.proto.Signer.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.Signer
       * @static
       * @param {rep.proto.ISigner} message Signer message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Signer.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a Signer message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.Signer
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.Signer} Signer
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Signer.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.Signer();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.name = reader.string();
              break;

            case 2:
              message.creditCode = reader.string();
              break;

            case 3:
              message.mobile = reader.string();
              break;

            case 4:
              if (!(message.certNames && message.certNames.length)) message.certNames = [];
              message.certNames.push(reader.string());
              break;

            case 5:
              if (!(message.authorizeIds && message.authorizeIds.length)) message.authorizeIds = [];
              message.authorizeIds.push(reader.string());
              break;

            case 6:
              if (!(message.operateIds && message.operateIds.length)) message.operateIds = [];
              message.operateIds.push(reader.string());
              break;

            case 7:
              if (!(message.credentialMetadataIds && message.credentialMetadataIds.length)) message.credentialMetadataIds = [];
              message.credentialMetadataIds.push(reader.string());
              break;

            case 8:
              if (!(message.authenticationCerts && message.authenticationCerts.length)) message.authenticationCerts = [];
              message.authenticationCerts.push($root.rep.proto.Certificate.decode(reader, reader.uint32()));
              break;

            case 9:
              message.signerInfo = reader.string();
              break;

            case 10:
              message.createTime = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
              break;

            case 11:
              message.disableTime = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
              break;

            case 12:
              message.signerValid = reader.bool();
              break;

            case 13:
              message.version = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a Signer message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.Signer
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.Signer} Signer
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Signer.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a Signer message.
       * @function verify
       * @memberof rep.proto.Signer
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      Signer.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.name != null && message.hasOwnProperty("name")) if (!$util.isString(message.name)) return "name: string expected";
        if (message.creditCode != null && message.hasOwnProperty("creditCode")) if (!$util.isString(message.creditCode)) return "creditCode: string expected";
        if (message.mobile != null && message.hasOwnProperty("mobile")) if (!$util.isString(message.mobile)) return "mobile: string expected";

        if (message.certNames != null && message.hasOwnProperty("certNames")) {
          if (!Array.isArray(message.certNames)) return "certNames: array expected";

          for (var i = 0; i < message.certNames.length; ++i) {
            if (!$util.isString(message.certNames[i])) return "certNames: string[] expected";
          }
        }

        if (message.authorizeIds != null && message.hasOwnProperty("authorizeIds")) {
          if (!Array.isArray(message.authorizeIds)) return "authorizeIds: array expected";

          for (var _i5 = 0; _i5 < message.authorizeIds.length; ++_i5) {
            if (!$util.isString(message.authorizeIds[_i5])) return "authorizeIds: string[] expected";
          }
        }

        if (message.operateIds != null && message.hasOwnProperty("operateIds")) {
          if (!Array.isArray(message.operateIds)) return "operateIds: array expected";

          for (var _i6 = 0; _i6 < message.operateIds.length; ++_i6) {
            if (!$util.isString(message.operateIds[_i6])) return "operateIds: string[] expected";
          }
        }

        if (message.credentialMetadataIds != null && message.hasOwnProperty("credentialMetadataIds")) {
          if (!Array.isArray(message.credentialMetadataIds)) return "credentialMetadataIds: array expected";

          for (var _i7 = 0; _i7 < message.credentialMetadataIds.length; ++_i7) {
            if (!$util.isString(message.credentialMetadataIds[_i7])) return "credentialMetadataIds: string[] expected";
          }
        }

        if (message.authenticationCerts != null && message.hasOwnProperty("authenticationCerts")) {
          if (!Array.isArray(message.authenticationCerts)) return "authenticationCerts: array expected";

          for (var _i8 = 0; _i8 < message.authenticationCerts.length; ++_i8) {
            var error = $root.rep.proto.Certificate.verify(message.authenticationCerts[_i8]);
            if (error) return "authenticationCerts." + error;
          }
        }

        if (message.signerInfo != null && message.hasOwnProperty("signerInfo")) if (!$util.isString(message.signerInfo)) return "signerInfo: string expected";

        if (message.createTime != null && message.hasOwnProperty("createTime")) {
          var _error = $root.google.protobuf.Timestamp.verify(message.createTime);

          if (_error) return "createTime." + _error;
        }

        if (message.disableTime != null && message.hasOwnProperty("disableTime")) {
          var _error2 = $root.google.protobuf.Timestamp.verify(message.disableTime);

          if (_error2) return "disableTime." + _error2;
        }

        if (message.signerValid != null && message.hasOwnProperty("signerValid")) if (typeof message.signerValid !== "boolean") return "signerValid: boolean expected";
        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isString(message.version)) return "version: string expected";
        return null;
      };
      /**
       * Creates a Signer message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.Signer
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.Signer} Signer
       */


      Signer.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.Signer) return object;
        var message = new $root.rep.proto.Signer();
        if (object.name != null) message.name = String(object.name);
        if (object.creditCode != null) message.creditCode = String(object.creditCode);
        if (object.mobile != null) message.mobile = String(object.mobile);

        if (object.certNames) {
          if (!Array.isArray(object.certNames)) throw TypeError(".rep.proto.Signer.certNames: array expected");
          message.certNames = [];

          for (var i = 0; i < object.certNames.length; ++i) {
            message.certNames[i] = String(object.certNames[i]);
          }
        }

        if (object.authorizeIds) {
          if (!Array.isArray(object.authorizeIds)) throw TypeError(".rep.proto.Signer.authorizeIds: array expected");
          message.authorizeIds = [];

          for (var _i9 = 0; _i9 < object.authorizeIds.length; ++_i9) {
            message.authorizeIds[_i9] = String(object.authorizeIds[_i9]);
          }
        }

        if (object.operateIds) {
          if (!Array.isArray(object.operateIds)) throw TypeError(".rep.proto.Signer.operateIds: array expected");
          message.operateIds = [];

          for (var _i10 = 0; _i10 < object.operateIds.length; ++_i10) {
            message.operateIds[_i10] = String(object.operateIds[_i10]);
          }
        }

        if (object.credentialMetadataIds) {
          if (!Array.isArray(object.credentialMetadataIds)) throw TypeError(".rep.proto.Signer.credentialMetadataIds: array expected");
          message.credentialMetadataIds = [];

          for (var _i11 = 0; _i11 < object.credentialMetadataIds.length; ++_i11) {
            message.credentialMetadataIds[_i11] = String(object.credentialMetadataIds[_i11]);
          }
        }

        if (object.authenticationCerts) {
          if (!Array.isArray(object.authenticationCerts)) throw TypeError(".rep.proto.Signer.authenticationCerts: array expected");
          message.authenticationCerts = [];

          for (var _i12 = 0; _i12 < object.authenticationCerts.length; ++_i12) {
            if ((0, _typeof2["default"])(object.authenticationCerts[_i12]) !== "object") throw TypeError(".rep.proto.Signer.authenticationCerts: object expected");
            message.authenticationCerts[_i12] = $root.rep.proto.Certificate.fromObject(object.authenticationCerts[_i12]);
          }
        }

        if (object.signerInfo != null) message.signerInfo = String(object.signerInfo);

        if (object.createTime != null) {
          if ((0, _typeof2["default"])(object.createTime) !== "object") throw TypeError(".rep.proto.Signer.createTime: object expected");
          message.createTime = $root.google.protobuf.Timestamp.fromObject(object.createTime);
        }

        if (object.disableTime != null) {
          if ((0, _typeof2["default"])(object.disableTime) !== "object") throw TypeError(".rep.proto.Signer.disableTime: object expected");
          message.disableTime = $root.google.protobuf.Timestamp.fromObject(object.disableTime);
        }

        if (object.signerValid != null) message.signerValid = Boolean(object.signerValid);
        if (object.version != null) message.version = String(object.version);
        return message;
      };
      /**
       * Creates a plain object from a Signer message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.Signer
       * @static
       * @param {rep.proto.Signer} message Signer
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      Signer.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.arrays || options.defaults) {
          object.certNames = [];
          object.authorizeIds = [];
          object.operateIds = [];
          object.credentialMetadataIds = [];
          object.authenticationCerts = [];
        }

        if (options.defaults) {
          object.name = "";
          object.creditCode = "";
          object.mobile = "";
          object.signerInfo = "";
          object.createTime = null;
          object.disableTime = null;
          object.signerValid = false;
          object.version = "";
        }

        if (message.name != null && message.hasOwnProperty("name")) object.name = message.name;
        if (message.creditCode != null && message.hasOwnProperty("creditCode")) object.creditCode = message.creditCode;
        if (message.mobile != null && message.hasOwnProperty("mobile")) object.mobile = message.mobile;

        if (message.certNames && message.certNames.length) {
          object.certNames = [];

          for (var j = 0; j < message.certNames.length; ++j) {
            object.certNames[j] = message.certNames[j];
          }
        }

        if (message.authorizeIds && message.authorizeIds.length) {
          object.authorizeIds = [];

          for (var _j = 0; _j < message.authorizeIds.length; ++_j) {
            object.authorizeIds[_j] = message.authorizeIds[_j];
          }
        }

        if (message.operateIds && message.operateIds.length) {
          object.operateIds = [];

          for (var _j2 = 0; _j2 < message.operateIds.length; ++_j2) {
            object.operateIds[_j2] = message.operateIds[_j2];
          }
        }

        if (message.credentialMetadataIds && message.credentialMetadataIds.length) {
          object.credentialMetadataIds = [];

          for (var _j3 = 0; _j3 < message.credentialMetadataIds.length; ++_j3) {
            object.credentialMetadataIds[_j3] = message.credentialMetadataIds[_j3];
          }
        }

        if (message.authenticationCerts && message.authenticationCerts.length) {
          object.authenticationCerts = [];

          for (var _j4 = 0; _j4 < message.authenticationCerts.length; ++_j4) {
            object.authenticationCerts[_j4] = $root.rep.proto.Certificate.toObject(message.authenticationCerts[_j4], options);
          }
        }

        if (message.signerInfo != null && message.hasOwnProperty("signerInfo")) object.signerInfo = message.signerInfo;
        if (message.createTime != null && message.hasOwnProperty("createTime")) object.createTime = $root.google.protobuf.Timestamp.toObject(message.createTime, options);
        if (message.disableTime != null && message.hasOwnProperty("disableTime")) object.disableTime = $root.google.protobuf.Timestamp.toObject(message.disableTime, options);
        if (message.signerValid != null && message.hasOwnProperty("signerValid")) object.signerValid = message.signerValid;
        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        return object;
      };
      /**
       * Converts this Signer to JSON.
       * @function toJSON
       * @memberof rep.proto.Signer
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      Signer.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Signer;
    }();

    proto.CertId = function () {
      /**
       * Properties of a CertId.
       * @memberof rep.proto
       * @interface ICertId
       * @property {string|null} [creditCode] CertId creditCode
       * @property {string|null} [certName] CertId certName
       * @property {string|null} [version] CertId version
       */

      /**
       * Constructs a new CertId.
       * @memberof rep.proto
       * @classdesc Represents a CertId.
       * @implements ICertId
       * @constructor
       * @param {rep.proto.ICertId=} [properties] Properties to set
       */
      function CertId(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * CertId creditCode.
       * @member {string} creditCode
       * @memberof rep.proto.CertId
       * @instance
       */


      CertId.prototype.creditCode = "";
      /**
       * CertId certName.
       * @member {string} certName
       * @memberof rep.proto.CertId
       * @instance
       */

      CertId.prototype.certName = "";
      /**
       * CertId version.
       * @member {string} version
       * @memberof rep.proto.CertId
       * @instance
       */

      CertId.prototype.version = "";
      /**
       * Creates a new CertId instance using the specified properties.
       * @function create
       * @memberof rep.proto.CertId
       * @static
       * @param {rep.proto.ICertId=} [properties] Properties to set
       * @returns {rep.proto.CertId} CertId instance
       */

      CertId.create = function create(properties) {
        return new CertId(properties);
      };
      /**
       * Encodes the specified CertId message. Does not implicitly {@link rep.proto.CertId.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.CertId
       * @static
       * @param {rep.proto.ICertId} message CertId message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      CertId.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.creditCode != null && Object.hasOwnProperty.call(message, "creditCode")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.creditCode);
        if (message.certName != null && Object.hasOwnProperty.call(message, "certName")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.certName);
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).string(message.version);
        return writer;
      };
      /**
       * Encodes the specified CertId message, length delimited. Does not implicitly {@link rep.proto.CertId.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.CertId
       * @static
       * @param {rep.proto.ICertId} message CertId message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      CertId.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a CertId message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.CertId
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.CertId} CertId
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      CertId.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.CertId();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.creditCode = reader.string();
              break;

            case 2:
              message.certName = reader.string();
              break;

            case 3:
              message.version = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a CertId message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.CertId
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.CertId} CertId
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      CertId.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a CertId message.
       * @function verify
       * @memberof rep.proto.CertId
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      CertId.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.creditCode != null && message.hasOwnProperty("creditCode")) if (!$util.isString(message.creditCode)) return "creditCode: string expected";
        if (message.certName != null && message.hasOwnProperty("certName")) if (!$util.isString(message.certName)) return "certName: string expected";
        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isString(message.version)) return "version: string expected";
        return null;
      };
      /**
       * Creates a CertId message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.CertId
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.CertId} CertId
       */


      CertId.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.CertId) return object;
        var message = new $root.rep.proto.CertId();
        if (object.creditCode != null) message.creditCode = String(object.creditCode);
        if (object.certName != null) message.certName = String(object.certName);
        if (object.version != null) message.version = String(object.version);
        return message;
      };
      /**
       * Creates a plain object from a CertId message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.CertId
       * @static
       * @param {rep.proto.CertId} message CertId
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      CertId.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.creditCode = "";
          object.certName = "";
          object.version = "";
        }

        if (message.creditCode != null && message.hasOwnProperty("creditCode")) object.creditCode = message.creditCode;
        if (message.certName != null && message.hasOwnProperty("certName")) object.certName = message.certName;
        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        return object;
      };
      /**
       * Converts this CertId to JSON.
       * @function toJSON
       * @memberof rep.proto.CertId
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      CertId.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return CertId;
    }();

    proto.Certificate = function () {
      /**
       * Properties of a Certificate.
       * @memberof rep.proto
       * @interface ICertificate
       * @property {string|null} [certificate] Certificate certificate
       * @property {string|null} [algType] Certificate algType
       * @property {boolean|null} [certValid] Certificate certValid
       * @property {google.protobuf.ITimestamp|null} [reg_Time] Certificate reg_Time
       * @property {google.protobuf.ITimestamp|null} [unreg_Time] Certificate unreg_Time
       * @property {rep.proto.Certificate.CertType|null} [certType] Certificate certType
       * @property {rep.proto.ICertId|null} [id] Certificate id
       * @property {string|null} [certHash] Certificate certHash
       * @property {string|null} [version] Certificate version
       */

      /**
       * Constructs a new Certificate.
       * @memberof rep.proto
       * @classdesc Represents a Certificate.
       * @implements ICertificate
       * @constructor
       * @param {rep.proto.ICertificate=} [properties] Properties to set
       */
      function Certificate(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * Certificate certificate.
       * @member {string} certificate
       * @memberof rep.proto.Certificate
       * @instance
       */


      Certificate.prototype.certificate = "";
      /**
       * Certificate algType.
       * @member {string} algType
       * @memberof rep.proto.Certificate
       * @instance
       */

      Certificate.prototype.algType = "";
      /**
       * Certificate certValid.
       * @member {boolean} certValid
       * @memberof rep.proto.Certificate
       * @instance
       */

      Certificate.prototype.certValid = false;
      /**
       * Certificate reg_Time.
       * @member {google.protobuf.ITimestamp|null|undefined} reg_Time
       * @memberof rep.proto.Certificate
       * @instance
       */

      Certificate.prototype.reg_Time = null;
      /**
       * Certificate unreg_Time.
       * @member {google.protobuf.ITimestamp|null|undefined} unreg_Time
       * @memberof rep.proto.Certificate
       * @instance
       */

      Certificate.prototype.unreg_Time = null;
      /**
       * Certificate certType.
       * @member {rep.proto.Certificate.CertType} certType
       * @memberof rep.proto.Certificate
       * @instance
       */

      Certificate.prototype.certType = 0;
      /**
       * Certificate id.
       * @member {rep.proto.ICertId|null|undefined} id
       * @memberof rep.proto.Certificate
       * @instance
       */

      Certificate.prototype.id = null;
      /**
       * Certificate certHash.
       * @member {string} certHash
       * @memberof rep.proto.Certificate
       * @instance
       */

      Certificate.prototype.certHash = "";
      /**
       * Certificate version.
       * @member {string} version
       * @memberof rep.proto.Certificate
       * @instance
       */

      Certificate.prototype.version = "";
      /**
       * Creates a new Certificate instance using the specified properties.
       * @function create
       * @memberof rep.proto.Certificate
       * @static
       * @param {rep.proto.ICertificate=} [properties] Properties to set
       * @returns {rep.proto.Certificate} Certificate instance
       */

      Certificate.create = function create(properties) {
        return new Certificate(properties);
      };
      /**
       * Encodes the specified Certificate message. Does not implicitly {@link rep.proto.Certificate.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.Certificate
       * @static
       * @param {rep.proto.ICertificate} message Certificate message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Certificate.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.certificate != null && Object.hasOwnProperty.call(message, "certificate")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.certificate);
        if (message.algType != null && Object.hasOwnProperty.call(message, "algType")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.algType);
        if (message.certValid != null && Object.hasOwnProperty.call(message, "certValid")) writer.uint32(
        /* id 3, wireType 0 =*/
        24).bool(message.certValid);
        if (message.reg_Time != null && Object.hasOwnProperty.call(message, "reg_Time")) $root.google.protobuf.Timestamp.encode(message.reg_Time, writer.uint32(
        /* id 4, wireType 2 =*/
        34).fork()).ldelim();
        if (message.unreg_Time != null && Object.hasOwnProperty.call(message, "unreg_Time")) $root.google.protobuf.Timestamp.encode(message.unreg_Time, writer.uint32(
        /* id 5, wireType 2 =*/
        42).fork()).ldelim();
        if (message.certType != null && Object.hasOwnProperty.call(message, "certType")) writer.uint32(
        /* id 6, wireType 0 =*/
        48).int32(message.certType);
        if (message.id != null && Object.hasOwnProperty.call(message, "id")) $root.rep.proto.CertId.encode(message.id, writer.uint32(
        /* id 7, wireType 2 =*/
        58).fork()).ldelim();
        if (message.certHash != null && Object.hasOwnProperty.call(message, "certHash")) writer.uint32(
        /* id 8, wireType 2 =*/
        66).string(message.certHash);
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 9, wireType 2 =*/
        74).string(message.version);
        return writer;
      };
      /**
       * Encodes the specified Certificate message, length delimited. Does not implicitly {@link rep.proto.Certificate.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.Certificate
       * @static
       * @param {rep.proto.ICertificate} message Certificate message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Certificate.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a Certificate message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.Certificate
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.Certificate} Certificate
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Certificate.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.Certificate();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.certificate = reader.string();
              break;

            case 2:
              message.algType = reader.string();
              break;

            case 3:
              message.certValid = reader.bool();
              break;

            case 4:
              message.reg_Time = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
              break;

            case 5:
              message.unreg_Time = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
              break;

            case 6:
              message.certType = reader.int32();
              break;

            case 7:
              message.id = $root.rep.proto.CertId.decode(reader, reader.uint32());
              break;

            case 8:
              message.certHash = reader.string();
              break;

            case 9:
              message.version = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a Certificate message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.Certificate
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.Certificate} Certificate
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Certificate.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a Certificate message.
       * @function verify
       * @memberof rep.proto.Certificate
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      Certificate.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.certificate != null && message.hasOwnProperty("certificate")) if (!$util.isString(message.certificate)) return "certificate: string expected";
        if (message.algType != null && message.hasOwnProperty("algType")) if (!$util.isString(message.algType)) return "algType: string expected";
        if (message.certValid != null && message.hasOwnProperty("certValid")) if (typeof message.certValid !== "boolean") return "certValid: boolean expected";

        if (message.reg_Time != null && message.hasOwnProperty("reg_Time")) {
          var error = $root.google.protobuf.Timestamp.verify(message.reg_Time);
          if (error) return "reg_Time." + error;
        }

        if (message.unreg_Time != null && message.hasOwnProperty("unreg_Time")) {
          var _error3 = $root.google.protobuf.Timestamp.verify(message.unreg_Time);

          if (_error3) return "unreg_Time." + _error3;
        }

        if (message.certType != null && message.hasOwnProperty("certType")) switch (message.certType) {
          default:
            return "certType: enum value expected";

          case 0:
          case 1:
          case 2:
            break;
        }

        if (message.id != null && message.hasOwnProperty("id")) {
          var _error4 = $root.rep.proto.CertId.verify(message.id);

          if (_error4) return "id." + _error4;
        }

        if (message.certHash != null && message.hasOwnProperty("certHash")) if (!$util.isString(message.certHash)) return "certHash: string expected";
        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isString(message.version)) return "version: string expected";
        return null;
      };
      /**
       * Creates a Certificate message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.Certificate
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.Certificate} Certificate
       */


      Certificate.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.Certificate) return object;
        var message = new $root.rep.proto.Certificate();
        if (object.certificate != null) message.certificate = String(object.certificate);
        if (object.algType != null) message.algType = String(object.algType);
        if (object.certValid != null) message.certValid = Boolean(object.certValid);

        if (object.reg_Time != null) {
          if ((0, _typeof2["default"])(object.reg_Time) !== "object") throw TypeError(".rep.proto.Certificate.reg_Time: object expected");
          message.reg_Time = $root.google.protobuf.Timestamp.fromObject(object.reg_Time);
        }

        if (object.unreg_Time != null) {
          if ((0, _typeof2["default"])(object.unreg_Time) !== "object") throw TypeError(".rep.proto.Certificate.unreg_Time: object expected");
          message.unreg_Time = $root.google.protobuf.Timestamp.fromObject(object.unreg_Time);
        }

        switch (object.certType) {
          case "CERT_UNDEFINED":
          case 0:
            message.certType = 0;
            break;

          case "CERT_AUTHENTICATION":
          case 1:
            message.certType = 1;
            break;

          case "CERT_CUSTOM":
          case 2:
            message.certType = 2;
            break;
        }

        if (object.id != null) {
          if ((0, _typeof2["default"])(object.id) !== "object") throw TypeError(".rep.proto.Certificate.id: object expected");
          message.id = $root.rep.proto.CertId.fromObject(object.id);
        }

        if (object.certHash != null) message.certHash = String(object.certHash);
        if (object.version != null) message.version = String(object.version);
        return message;
      };
      /**
       * Creates a plain object from a Certificate message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.Certificate
       * @static
       * @param {rep.proto.Certificate} message Certificate
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      Certificate.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.certificate = "";
          object.algType = "";
          object.certValid = false;
          object.reg_Time = null;
          object.unreg_Time = null;
          object.certType = options.enums === String ? "CERT_UNDEFINED" : 0;
          object.id = null;
          object.certHash = "";
          object.version = "";
        }

        if (message.certificate != null && message.hasOwnProperty("certificate")) object.certificate = message.certificate;
        if (message.algType != null && message.hasOwnProperty("algType")) object.algType = message.algType;
        if (message.certValid != null && message.hasOwnProperty("certValid")) object.certValid = message.certValid;
        if (message.reg_Time != null && message.hasOwnProperty("reg_Time")) object.reg_Time = $root.google.protobuf.Timestamp.toObject(message.reg_Time, options);
        if (message.unreg_Time != null && message.hasOwnProperty("unreg_Time")) object.unreg_Time = $root.google.protobuf.Timestamp.toObject(message.unreg_Time, options);
        if (message.certType != null && message.hasOwnProperty("certType")) object.certType = options.enums === String ? $root.rep.proto.Certificate.CertType[message.certType] : message.certType;
        if (message.id != null && message.hasOwnProperty("id")) object.id = $root.rep.proto.CertId.toObject(message.id, options);
        if (message.certHash != null && message.hasOwnProperty("certHash")) object.certHash = message.certHash;
        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        return object;
      };
      /**
       * Converts this Certificate to JSON.
       * @function toJSON
       * @memberof rep.proto.Certificate
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      Certificate.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };
      /**
       * CertType enum.
       * @name rep.proto.Certificate.CertType
       * @enum {number}
       * @property {number} CERT_UNDEFINED=0 CERT_UNDEFINED value
       * @property {number} CERT_AUTHENTICATION=1 CERT_AUTHENTICATION value
       * @property {number} CERT_CUSTOM=2 CERT_CUSTOM value
       */


      Certificate.CertType = function () {
        var valuesById = {},
            values = Object.create(valuesById);
        values[valuesById[0] = "CERT_UNDEFINED"] = 0;
        values[valuesById[1] = "CERT_AUTHENTICATION"] = 1;
        values[valuesById[2] = "CERT_CUSTOM"] = 2;
        return values;
      }();

      return Certificate;
    }();

    proto.Operate = function () {
      /**
       * Properties of an Operate.
       * @memberof rep.proto
       * @interface IOperate
       * @property {string|null} [opId] Operate opId
       * @property {string|null} [description] Operate description
       * @property {string|null} [register] Operate register
       * @property {boolean|null} [isPublish] Operate isPublish
       * @property {rep.proto.Operate.OperateType|null} [operateType] Operate operateType
       * @property {Array.<string>|null} [operateServiceName] Operate operateServiceName
       * @property {string|null} [operateEndpoint] Operate operateEndpoint
       * @property {string|null} [authFullName] Operate authFullName
       * @property {google.protobuf.ITimestamp|null} [createTime] Operate createTime
       * @property {google.protobuf.ITimestamp|null} [disableTime] Operate disableTime
       * @property {boolean|null} [opValid] Operate opValid
       * @property {string|null} [version] Operate version
       */

      /**
       * Constructs a new Operate.
       * @memberof rep.proto
       * @classdesc Represents an Operate.
       * @implements IOperate
       * @constructor
       * @param {rep.proto.IOperate=} [properties] Properties to set
       */
      function Operate(properties) {
        this.operateServiceName = [];
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * Operate opId.
       * @member {string} opId
       * @memberof rep.proto.Operate
       * @instance
       */


      Operate.prototype.opId = "";
      /**
       * Operate description.
       * @member {string} description
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.description = "";
      /**
       * Operate register.
       * @member {string} register
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.register = "";
      /**
       * Operate isPublish.
       * @member {boolean} isPublish
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.isPublish = false;
      /**
       * Operate operateType.
       * @member {rep.proto.Operate.OperateType} operateType
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.operateType = 0;
      /**
       * Operate operateServiceName.
       * @member {Array.<string>} operateServiceName
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.operateServiceName = $util.emptyArray;
      /**
       * Operate operateEndpoint.
       * @member {string} operateEndpoint
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.operateEndpoint = "";
      /**
       * Operate authFullName.
       * @member {string} authFullName
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.authFullName = "";
      /**
       * Operate createTime.
       * @member {google.protobuf.ITimestamp|null|undefined} createTime
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.createTime = null;
      /**
       * Operate disableTime.
       * @member {google.protobuf.ITimestamp|null|undefined} disableTime
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.disableTime = null;
      /**
       * Operate opValid.
       * @member {boolean} opValid
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.opValid = false;
      /**
       * Operate version.
       * @member {string} version
       * @memberof rep.proto.Operate
       * @instance
       */

      Operate.prototype.version = "";
      /**
       * Creates a new Operate instance using the specified properties.
       * @function create
       * @memberof rep.proto.Operate
       * @static
       * @param {rep.proto.IOperate=} [properties] Properties to set
       * @returns {rep.proto.Operate} Operate instance
       */

      Operate.create = function create(properties) {
        return new Operate(properties);
      };
      /**
       * Encodes the specified Operate message. Does not implicitly {@link rep.proto.Operate.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.Operate
       * @static
       * @param {rep.proto.IOperate} message Operate message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Operate.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.opId != null && Object.hasOwnProperty.call(message, "opId")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.opId);
        if (message.description != null && Object.hasOwnProperty.call(message, "description")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.description);
        if (message.register != null && Object.hasOwnProperty.call(message, "register")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).string(message.register);
        if (message.isPublish != null && Object.hasOwnProperty.call(message, "isPublish")) writer.uint32(
        /* id 4, wireType 0 =*/
        32).bool(message.isPublish);
        if (message.operateType != null && Object.hasOwnProperty.call(message, "operateType")) writer.uint32(
        /* id 5, wireType 0 =*/
        40).int32(message.operateType);
        if (message.operateServiceName != null && message.operateServiceName.length) for (var i = 0; i < message.operateServiceName.length; ++i) {
          writer.uint32(
          /* id 6, wireType 2 =*/
          50).string(message.operateServiceName[i]);
        }
        if (message.operateEndpoint != null && Object.hasOwnProperty.call(message, "operateEndpoint")) writer.uint32(
        /* id 7, wireType 2 =*/
        58).string(message.operateEndpoint);
        if (message.authFullName != null && Object.hasOwnProperty.call(message, "authFullName")) writer.uint32(
        /* id 8, wireType 2 =*/
        66).string(message.authFullName);
        if (message.createTime != null && Object.hasOwnProperty.call(message, "createTime")) $root.google.protobuf.Timestamp.encode(message.createTime, writer.uint32(
        /* id 9, wireType 2 =*/
        74).fork()).ldelim();
        if (message.disableTime != null && Object.hasOwnProperty.call(message, "disableTime")) $root.google.protobuf.Timestamp.encode(message.disableTime, writer.uint32(
        /* id 10, wireType 2 =*/
        82).fork()).ldelim();
        if (message.opValid != null && Object.hasOwnProperty.call(message, "opValid")) writer.uint32(
        /* id 11, wireType 0 =*/
        88).bool(message.opValid);
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 12, wireType 2 =*/
        98).string(message.version);
        return writer;
      };
      /**
       * Encodes the specified Operate message, length delimited. Does not implicitly {@link rep.proto.Operate.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.Operate
       * @static
       * @param {rep.proto.IOperate} message Operate message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Operate.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes an Operate message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.Operate
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.Operate} Operate
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Operate.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.Operate();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.opId = reader.string();
              break;

            case 2:
              message.description = reader.string();
              break;

            case 3:
              message.register = reader.string();
              break;

            case 4:
              message.isPublish = reader.bool();
              break;

            case 5:
              message.operateType = reader.int32();
              break;

            case 6:
              if (!(message.operateServiceName && message.operateServiceName.length)) message.operateServiceName = [];
              message.operateServiceName.push(reader.string());
              break;

            case 7:
              message.operateEndpoint = reader.string();
              break;

            case 8:
              message.authFullName = reader.string();
              break;

            case 9:
              message.createTime = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
              break;

            case 10:
              message.disableTime = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
              break;

            case 11:
              message.opValid = reader.bool();
              break;

            case 12:
              message.version = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes an Operate message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.Operate
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.Operate} Operate
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Operate.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies an Operate message.
       * @function verify
       * @memberof rep.proto.Operate
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      Operate.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.opId != null && message.hasOwnProperty("opId")) if (!$util.isString(message.opId)) return "opId: string expected";
        if (message.description != null && message.hasOwnProperty("description")) if (!$util.isString(message.description)) return "description: string expected";
        if (message.register != null && message.hasOwnProperty("register")) if (!$util.isString(message.register)) return "register: string expected";
        if (message.isPublish != null && message.hasOwnProperty("isPublish")) if (typeof message.isPublish !== "boolean") return "isPublish: boolean expected";
        if (message.operateType != null && message.hasOwnProperty("operateType")) switch (message.operateType) {
          default:
            return "operateType: enum value expected";

          case 0:
          case 1:
          case 2:
            break;
        }

        if (message.operateServiceName != null && message.hasOwnProperty("operateServiceName")) {
          if (!Array.isArray(message.operateServiceName)) return "operateServiceName: array expected";

          for (var i = 0; i < message.operateServiceName.length; ++i) {
            if (!$util.isString(message.operateServiceName[i])) return "operateServiceName: string[] expected";
          }
        }

        if (message.operateEndpoint != null && message.hasOwnProperty("operateEndpoint")) if (!$util.isString(message.operateEndpoint)) return "operateEndpoint: string expected";
        if (message.authFullName != null && message.hasOwnProperty("authFullName")) if (!$util.isString(message.authFullName)) return "authFullName: string expected";

        if (message.createTime != null && message.hasOwnProperty("createTime")) {
          var error = $root.google.protobuf.Timestamp.verify(message.createTime);
          if (error) return "createTime." + error;
        }

        if (message.disableTime != null && message.hasOwnProperty("disableTime")) {
          var _error5 = $root.google.protobuf.Timestamp.verify(message.disableTime);

          if (_error5) return "disableTime." + _error5;
        }

        if (message.opValid != null && message.hasOwnProperty("opValid")) if (typeof message.opValid !== "boolean") return "opValid: boolean expected";
        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isString(message.version)) return "version: string expected";
        return null;
      };
      /**
       * Creates an Operate message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.Operate
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.Operate} Operate
       */


      Operate.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.Operate) return object;
        var message = new $root.rep.proto.Operate();
        if (object.opId != null) message.opId = String(object.opId);
        if (object.description != null) message.description = String(object.description);
        if (object.register != null) message.register = String(object.register);
        if (object.isPublish != null) message.isPublish = Boolean(object.isPublish);

        switch (object.operateType) {
          case "OPERATE_UNDEFINED":
          case 0:
            message.operateType = 0;
            break;

          case "OPERATE_CONTRACT":
          case 1:
            message.operateType = 1;
            break;

          case "OPERATE_SERVICE":
          case 2:
            message.operateType = 2;
            break;
        }

        if (object.operateServiceName) {
          if (!Array.isArray(object.operateServiceName)) throw TypeError(".rep.proto.Operate.operateServiceName: array expected");
          message.operateServiceName = [];

          for (var i = 0; i < object.operateServiceName.length; ++i) {
            message.operateServiceName[i] = String(object.operateServiceName[i]);
          }
        }

        if (object.operateEndpoint != null) message.operateEndpoint = String(object.operateEndpoint);
        if (object.authFullName != null) message.authFullName = String(object.authFullName);

        if (object.createTime != null) {
          if ((0, _typeof2["default"])(object.createTime) !== "object") throw TypeError(".rep.proto.Operate.createTime: object expected");
          message.createTime = $root.google.protobuf.Timestamp.fromObject(object.createTime);
        }

        if (object.disableTime != null) {
          if ((0, _typeof2["default"])(object.disableTime) !== "object") throw TypeError(".rep.proto.Operate.disableTime: object expected");
          message.disableTime = $root.google.protobuf.Timestamp.fromObject(object.disableTime);
        }

        if (object.opValid != null) message.opValid = Boolean(object.opValid);
        if (object.version != null) message.version = String(object.version);
        return message;
      };
      /**
       * Creates a plain object from an Operate message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.Operate
       * @static
       * @param {rep.proto.Operate} message Operate
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      Operate.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.operateServiceName = [];

        if (options.defaults) {
          object.opId = "";
          object.description = "";
          object.register = "";
          object.isPublish = false;
          object.operateType = options.enums === String ? "OPERATE_UNDEFINED" : 0;
          object.operateEndpoint = "";
          object.authFullName = "";
          object.createTime = null;
          object.disableTime = null;
          object.opValid = false;
          object.version = "";
        }

        if (message.opId != null && message.hasOwnProperty("opId")) object.opId = message.opId;
        if (message.description != null && message.hasOwnProperty("description")) object.description = message.description;
        if (message.register != null && message.hasOwnProperty("register")) object.register = message.register;
        if (message.isPublish != null && message.hasOwnProperty("isPublish")) object.isPublish = message.isPublish;
        if (message.operateType != null && message.hasOwnProperty("operateType")) object.operateType = options.enums === String ? $root.rep.proto.Operate.OperateType[message.operateType] : message.operateType;

        if (message.operateServiceName && message.operateServiceName.length) {
          object.operateServiceName = [];

          for (var j = 0; j < message.operateServiceName.length; ++j) {
            object.operateServiceName[j] = message.operateServiceName[j];
          }
        }

        if (message.operateEndpoint != null && message.hasOwnProperty("operateEndpoint")) object.operateEndpoint = message.operateEndpoint;
        if (message.authFullName != null && message.hasOwnProperty("authFullName")) object.authFullName = message.authFullName;
        if (message.createTime != null && message.hasOwnProperty("createTime")) object.createTime = $root.google.protobuf.Timestamp.toObject(message.createTime, options);
        if (message.disableTime != null && message.hasOwnProperty("disableTime")) object.disableTime = $root.google.protobuf.Timestamp.toObject(message.disableTime, options);
        if (message.opValid != null && message.hasOwnProperty("opValid")) object.opValid = message.opValid;
        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        return object;
      };
      /**
       * Converts this Operate to JSON.
       * @function toJSON
       * @memberof rep.proto.Operate
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      Operate.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };
      /**
       * OperateType enum.
       * @name rep.proto.Operate.OperateType
       * @enum {number}
       * @property {number} OPERATE_UNDEFINED=0 OPERATE_UNDEFINED value
       * @property {number} OPERATE_CONTRACT=1 OPERATE_CONTRACT value
       * @property {number} OPERATE_SERVICE=2 OPERATE_SERVICE value
       */


      Operate.OperateType = function () {
        var valuesById = {},
            values = Object.create(valuesById);
        values[valuesById[0] = "OPERATE_UNDEFINED"] = 0;
        values[valuesById[1] = "OPERATE_CONTRACT"] = 1;
        values[valuesById[2] = "OPERATE_SERVICE"] = 2;
        return values;
      }();

      return Operate;
    }();

    proto.Authorize = function () {
      /**
       * Properties of an Authorize.
       * @memberof rep.proto
       * @interface IAuthorize
       * @property {string|null} [id] Authorize id
       * @property {string|null} [grant] Authorize grant
       * @property {Array.<string>|null} [granted] Authorize granted
       * @property {Array.<string>|null} [opId] Authorize opId
       * @property {rep.proto.Authorize.TransferType|null} [isTransfer] Authorize isTransfer
       * @property {google.protobuf.ITimestamp|null} [createTime] Authorize createTime
       * @property {google.protobuf.ITimestamp|null} [disableTime] Authorize disableTime
       * @property {boolean|null} [authorizeValid] Authorize authorizeValid
       * @property {string|null} [version] Authorize version
       */

      /**
       * Constructs a new Authorize.
       * @memberof rep.proto
       * @classdesc Represents an Authorize.
       * @implements IAuthorize
       * @constructor
       * @param {rep.proto.IAuthorize=} [properties] Properties to set
       */
      function Authorize(properties) {
        this.granted = [];
        this.opId = [];
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * Authorize id.
       * @member {string} id
       * @memberof rep.proto.Authorize
       * @instance
       */


      Authorize.prototype.id = "";
      /**
       * Authorize grant.
       * @member {string} grant
       * @memberof rep.proto.Authorize
       * @instance
       */

      Authorize.prototype.grant = "";
      /**
       * Authorize granted.
       * @member {Array.<string>} granted
       * @memberof rep.proto.Authorize
       * @instance
       */

      Authorize.prototype.granted = $util.emptyArray;
      /**
       * Authorize opId.
       * @member {Array.<string>} opId
       * @memberof rep.proto.Authorize
       * @instance
       */

      Authorize.prototype.opId = $util.emptyArray;
      /**
       * Authorize isTransfer.
       * @member {rep.proto.Authorize.TransferType} isTransfer
       * @memberof rep.proto.Authorize
       * @instance
       */

      Authorize.prototype.isTransfer = 0;
      /**
       * Authorize createTime.
       * @member {google.protobuf.ITimestamp|null|undefined} createTime
       * @memberof rep.proto.Authorize
       * @instance
       */

      Authorize.prototype.createTime = null;
      /**
       * Authorize disableTime.
       * @member {google.protobuf.ITimestamp|null|undefined} disableTime
       * @memberof rep.proto.Authorize
       * @instance
       */

      Authorize.prototype.disableTime = null;
      /**
       * Authorize authorizeValid.
       * @member {boolean} authorizeValid
       * @memberof rep.proto.Authorize
       * @instance
       */

      Authorize.prototype.authorizeValid = false;
      /**
       * Authorize version.
       * @member {string} version
       * @memberof rep.proto.Authorize
       * @instance
       */

      Authorize.prototype.version = "";
      /**
       * Creates a new Authorize instance using the specified properties.
       * @function create
       * @memberof rep.proto.Authorize
       * @static
       * @param {rep.proto.IAuthorize=} [properties] Properties to set
       * @returns {rep.proto.Authorize} Authorize instance
       */

      Authorize.create = function create(properties) {
        return new Authorize(properties);
      };
      /**
       * Encodes the specified Authorize message. Does not implicitly {@link rep.proto.Authorize.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.Authorize
       * @static
       * @param {rep.proto.IAuthorize} message Authorize message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Authorize.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.id != null && Object.hasOwnProperty.call(message, "id")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.id);
        if (message.grant != null && Object.hasOwnProperty.call(message, "grant")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.grant);
        if (message.granted != null && message.granted.length) for (var i = 0; i < message.granted.length; ++i) {
          writer.uint32(
          /* id 3, wireType 2 =*/
          26).string(message.granted[i]);
        }
        if (message.opId != null && message.opId.length) for (var _i13 = 0; _i13 < message.opId.length; ++_i13) {
          writer.uint32(
          /* id 4, wireType 2 =*/
          34).string(message.opId[_i13]);
        }
        if (message.isTransfer != null && Object.hasOwnProperty.call(message, "isTransfer")) writer.uint32(
        /* id 5, wireType 0 =*/
        40).int32(message.isTransfer);
        if (message.createTime != null && Object.hasOwnProperty.call(message, "createTime")) $root.google.protobuf.Timestamp.encode(message.createTime, writer.uint32(
        /* id 6, wireType 2 =*/
        50).fork()).ldelim();
        if (message.disableTime != null && Object.hasOwnProperty.call(message, "disableTime")) $root.google.protobuf.Timestamp.encode(message.disableTime, writer.uint32(
        /* id 7, wireType 2 =*/
        58).fork()).ldelim();
        if (message.authorizeValid != null && Object.hasOwnProperty.call(message, "authorizeValid")) writer.uint32(
        /* id 8, wireType 0 =*/
        64).bool(message.authorizeValid);
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 9, wireType 2 =*/
        74).string(message.version);
        return writer;
      };
      /**
       * Encodes the specified Authorize message, length delimited. Does not implicitly {@link rep.proto.Authorize.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.Authorize
       * @static
       * @param {rep.proto.IAuthorize} message Authorize message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Authorize.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes an Authorize message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.Authorize
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.Authorize} Authorize
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Authorize.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.Authorize();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.id = reader.string();
              break;

            case 2:
              message.grant = reader.string();
              break;

            case 3:
              if (!(message.granted && message.granted.length)) message.granted = [];
              message.granted.push(reader.string());
              break;

            case 4:
              if (!(message.opId && message.opId.length)) message.opId = [];
              message.opId.push(reader.string());
              break;

            case 5:
              message.isTransfer = reader.int32();
              break;

            case 6:
              message.createTime = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
              break;

            case 7:
              message.disableTime = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
              break;

            case 8:
              message.authorizeValid = reader.bool();
              break;

            case 9:
              message.version = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes an Authorize message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.Authorize
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.Authorize} Authorize
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Authorize.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies an Authorize message.
       * @function verify
       * @memberof rep.proto.Authorize
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      Authorize.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.id != null && message.hasOwnProperty("id")) if (!$util.isString(message.id)) return "id: string expected";
        if (message.grant != null && message.hasOwnProperty("grant")) if (!$util.isString(message.grant)) return "grant: string expected";

        if (message.granted != null && message.hasOwnProperty("granted")) {
          if (!Array.isArray(message.granted)) return "granted: array expected";

          for (var i = 0; i < message.granted.length; ++i) {
            if (!$util.isString(message.granted[i])) return "granted: string[] expected";
          }
        }

        if (message.opId != null && message.hasOwnProperty("opId")) {
          if (!Array.isArray(message.opId)) return "opId: array expected";

          for (var _i14 = 0; _i14 < message.opId.length; ++_i14) {
            if (!$util.isString(message.opId[_i14])) return "opId: string[] expected";
          }
        }

        if (message.isTransfer != null && message.hasOwnProperty("isTransfer")) switch (message.isTransfer) {
          default:
            return "isTransfer: enum value expected";

          case 0:
          case 1:
          case 2:
            break;
        }

        if (message.createTime != null && message.hasOwnProperty("createTime")) {
          var error = $root.google.protobuf.Timestamp.verify(message.createTime);
          if (error) return "createTime." + error;
        }

        if (message.disableTime != null && message.hasOwnProperty("disableTime")) {
          var _error6 = $root.google.protobuf.Timestamp.verify(message.disableTime);

          if (_error6) return "disableTime." + _error6;
        }

        if (message.authorizeValid != null && message.hasOwnProperty("authorizeValid")) if (typeof message.authorizeValid !== "boolean") return "authorizeValid: boolean expected";
        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isString(message.version)) return "version: string expected";
        return null;
      };
      /**
       * Creates an Authorize message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.Authorize
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.Authorize} Authorize
       */


      Authorize.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.Authorize) return object;
        var message = new $root.rep.proto.Authorize();
        if (object.id != null) message.id = String(object.id);
        if (object.grant != null) message.grant = String(object.grant);

        if (object.granted) {
          if (!Array.isArray(object.granted)) throw TypeError(".rep.proto.Authorize.granted: array expected");
          message.granted = [];

          for (var i = 0; i < object.granted.length; ++i) {
            message.granted[i] = String(object.granted[i]);
          }
        }

        if (object.opId) {
          if (!Array.isArray(object.opId)) throw TypeError(".rep.proto.Authorize.opId: array expected");
          message.opId = [];

          for (var _i15 = 0; _i15 < object.opId.length; ++_i15) {
            message.opId[_i15] = String(object.opId[_i15]);
          }
        }

        switch (object.isTransfer) {
          case "TRANSFER_DISABLE":
          case 0:
            message.isTransfer = 0;
            break;

          case "TRANSFER_ONCE":
          case 1:
            message.isTransfer = 1;
            break;

          case "TRANSFER_REPEATEDLY":
          case 2:
            message.isTransfer = 2;
            break;
        }

        if (object.createTime != null) {
          if ((0, _typeof2["default"])(object.createTime) !== "object") throw TypeError(".rep.proto.Authorize.createTime: object expected");
          message.createTime = $root.google.protobuf.Timestamp.fromObject(object.createTime);
        }

        if (object.disableTime != null) {
          if ((0, _typeof2["default"])(object.disableTime) !== "object") throw TypeError(".rep.proto.Authorize.disableTime: object expected");
          message.disableTime = $root.google.protobuf.Timestamp.fromObject(object.disableTime);
        }

        if (object.authorizeValid != null) message.authorizeValid = Boolean(object.authorizeValid);
        if (object.version != null) message.version = String(object.version);
        return message;
      };
      /**
       * Creates a plain object from an Authorize message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.Authorize
       * @static
       * @param {rep.proto.Authorize} message Authorize
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      Authorize.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.arrays || options.defaults) {
          object.granted = [];
          object.opId = [];
        }

        if (options.defaults) {
          object.id = "";
          object.grant = "";
          object.isTransfer = options.enums === String ? "TRANSFER_DISABLE" : 0;
          object.createTime = null;
          object.disableTime = null;
          object.authorizeValid = false;
          object.version = "";
        }

        if (message.id != null && message.hasOwnProperty("id")) object.id = message.id;
        if (message.grant != null && message.hasOwnProperty("grant")) object.grant = message.grant;

        if (message.granted && message.granted.length) {
          object.granted = [];

          for (var j = 0; j < message.granted.length; ++j) {
            object.granted[j] = message.granted[j];
          }
        }

        if (message.opId && message.opId.length) {
          object.opId = [];

          for (var _j5 = 0; _j5 < message.opId.length; ++_j5) {
            object.opId[_j5] = message.opId[_j5];
          }
        }

        if (message.isTransfer != null && message.hasOwnProperty("isTransfer")) object.isTransfer = options.enums === String ? $root.rep.proto.Authorize.TransferType[message.isTransfer] : message.isTransfer;
        if (message.createTime != null && message.hasOwnProperty("createTime")) object.createTime = $root.google.protobuf.Timestamp.toObject(message.createTime, options);
        if (message.disableTime != null && message.hasOwnProperty("disableTime")) object.disableTime = $root.google.protobuf.Timestamp.toObject(message.disableTime, options);
        if (message.authorizeValid != null && message.hasOwnProperty("authorizeValid")) object.authorizeValid = message.authorizeValid;
        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        return object;
      };
      /**
       * Converts this Authorize to JSON.
       * @function toJSON
       * @memberof rep.proto.Authorize
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      Authorize.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };
      /**
       * TransferType enum.
       * @name rep.proto.Authorize.TransferType
       * @enum {number}
       * @property {number} TRANSFER_DISABLE=0 TRANSFER_DISABLE value
       * @property {number} TRANSFER_ONCE=1 TRANSFER_ONCE value
       * @property {number} TRANSFER_REPEATEDLY=2 TRANSFER_REPEATEDLY value
       */


      Authorize.TransferType = function () {
        var valuesById = {},
            values = Object.create(valuesById);
        values[valuesById[0] = "TRANSFER_DISABLE"] = 0;
        values[valuesById[1] = "TRANSFER_ONCE"] = 1;
        values[valuesById[2] = "TRANSFER_REPEATEDLY"] = 2;
        return values;
      }();

      return Authorize;
    }();

    proto.BindCertToAuthorize = function () {
      /**
       * Properties of a BindCertToAuthorize.
       * @memberof rep.proto
       * @interface IBindCertToAuthorize
       * @property {string|null} [authorizeId] BindCertToAuthorize authorizeId
       * @property {rep.proto.ICertId|null} [granted] BindCertToAuthorize granted
       * @property {string|null} [version] BindCertToAuthorize version
       */

      /**
       * Constructs a new BindCertToAuthorize.
       * @memberof rep.proto
       * @classdesc Represents a BindCertToAuthorize.
       * @implements IBindCertToAuthorize
       * @constructor
       * @param {rep.proto.IBindCertToAuthorize=} [properties] Properties to set
       */
      function BindCertToAuthorize(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * BindCertToAuthorize authorizeId.
       * @member {string} authorizeId
       * @memberof rep.proto.BindCertToAuthorize
       * @instance
       */


      BindCertToAuthorize.prototype.authorizeId = "";
      /**
       * BindCertToAuthorize granted.
       * @member {rep.proto.ICertId|null|undefined} granted
       * @memberof rep.proto.BindCertToAuthorize
       * @instance
       */

      BindCertToAuthorize.prototype.granted = null;
      /**
       * BindCertToAuthorize version.
       * @member {string} version
       * @memberof rep.proto.BindCertToAuthorize
       * @instance
       */

      BindCertToAuthorize.prototype.version = "";
      /**
       * Creates a new BindCertToAuthorize instance using the specified properties.
       * @function create
       * @memberof rep.proto.BindCertToAuthorize
       * @static
       * @param {rep.proto.IBindCertToAuthorize=} [properties] Properties to set
       * @returns {rep.proto.BindCertToAuthorize} BindCertToAuthorize instance
       */

      BindCertToAuthorize.create = function create(properties) {
        return new BindCertToAuthorize(properties);
      };
      /**
       * Encodes the specified BindCertToAuthorize message. Does not implicitly {@link rep.proto.BindCertToAuthorize.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.BindCertToAuthorize
       * @static
       * @param {rep.proto.IBindCertToAuthorize} message BindCertToAuthorize message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      BindCertToAuthorize.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.authorizeId != null && Object.hasOwnProperty.call(message, "authorizeId")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.authorizeId);
        if (message.granted != null && Object.hasOwnProperty.call(message, "granted")) $root.rep.proto.CertId.encode(message.granted, writer.uint32(
        /* id 2, wireType 2 =*/
        18).fork()).ldelim();
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).string(message.version);
        return writer;
      };
      /**
       * Encodes the specified BindCertToAuthorize message, length delimited. Does not implicitly {@link rep.proto.BindCertToAuthorize.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.BindCertToAuthorize
       * @static
       * @param {rep.proto.IBindCertToAuthorize} message BindCertToAuthorize message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      BindCertToAuthorize.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a BindCertToAuthorize message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.BindCertToAuthorize
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.BindCertToAuthorize} BindCertToAuthorize
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      BindCertToAuthorize.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.BindCertToAuthorize();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.authorizeId = reader.string();
              break;

            case 2:
              message.granted = $root.rep.proto.CertId.decode(reader, reader.uint32());
              break;

            case 3:
              message.version = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a BindCertToAuthorize message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.BindCertToAuthorize
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.BindCertToAuthorize} BindCertToAuthorize
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      BindCertToAuthorize.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a BindCertToAuthorize message.
       * @function verify
       * @memberof rep.proto.BindCertToAuthorize
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      BindCertToAuthorize.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.authorizeId != null && message.hasOwnProperty("authorizeId")) if (!$util.isString(message.authorizeId)) return "authorizeId: string expected";

        if (message.granted != null && message.hasOwnProperty("granted")) {
          var error = $root.rep.proto.CertId.verify(message.granted);
          if (error) return "granted." + error;
        }

        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isString(message.version)) return "version: string expected";
        return null;
      };
      /**
       * Creates a BindCertToAuthorize message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.BindCertToAuthorize
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.BindCertToAuthorize} BindCertToAuthorize
       */


      BindCertToAuthorize.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.BindCertToAuthorize) return object;
        var message = new $root.rep.proto.BindCertToAuthorize();
        if (object.authorizeId != null) message.authorizeId = String(object.authorizeId);

        if (object.granted != null) {
          if ((0, _typeof2["default"])(object.granted) !== "object") throw TypeError(".rep.proto.BindCertToAuthorize.granted: object expected");
          message.granted = $root.rep.proto.CertId.fromObject(object.granted);
        }

        if (object.version != null) message.version = String(object.version);
        return message;
      };
      /**
       * Creates a plain object from a BindCertToAuthorize message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.BindCertToAuthorize
       * @static
       * @param {rep.proto.BindCertToAuthorize} message BindCertToAuthorize
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      BindCertToAuthorize.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.authorizeId = "";
          object.granted = null;
          object.version = "";
        }

        if (message.authorizeId != null && message.hasOwnProperty("authorizeId")) object.authorizeId = message.authorizeId;
        if (message.granted != null && message.hasOwnProperty("granted")) object.granted = $root.rep.proto.CertId.toObject(message.granted, options);
        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        return object;
      };
      /**
       * Converts this BindCertToAuthorize to JSON.
       * @function toJSON
       * @memberof rep.proto.BindCertToAuthorize
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      BindCertToAuthorize.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return BindCertToAuthorize;
    }();

    proto.CreClaStruct = function () {
      /**
       * Properties of a CreClaStruct.
       * @memberof rep.proto
       * @interface ICreClaStruct
       * @property {string|null} [version] CreClaStruct version
       * @property {string|null} [id] CreClaStruct id
       * @property {string|null} [name] CreClaStruct name
       * @property {string|null} [ccsVersion] CreClaStruct ccsVersion
       * @property {string|null} [description] CreClaStruct description
       * @property {string|null} [creator] CreClaStruct creator
       * @property {string|null} [created] CreClaStruct created
       * @property {boolean|null} [valid] CreClaStruct valid
       * @property {Array.<rep.proto.ICreAttr>|null} [attributes] CreClaStruct attributes
       */

      /**
       * Constructs a new CreClaStruct.
       * @memberof rep.proto
       * @classdesc Represents a CreClaStruct.
       * @implements ICreClaStruct
       * @constructor
       * @param {rep.proto.ICreClaStruct=} [properties] Properties to set
       */
      function CreClaStruct(properties) {
        this.attributes = [];
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * CreClaStruct version.
       * @member {string} version
       * @memberof rep.proto.CreClaStruct
       * @instance
       */


      CreClaStruct.prototype.version = "";
      /**
       * CreClaStruct id.
       * @member {string} id
       * @memberof rep.proto.CreClaStruct
       * @instance
       */

      CreClaStruct.prototype.id = "";
      /**
       * CreClaStruct name.
       * @member {string} name
       * @memberof rep.proto.CreClaStruct
       * @instance
       */

      CreClaStruct.prototype.name = "";
      /**
       * CreClaStruct ccsVersion.
       * @member {string} ccsVersion
       * @memberof rep.proto.CreClaStruct
       * @instance
       */

      CreClaStruct.prototype.ccsVersion = "";
      /**
       * CreClaStruct description.
       * @member {string} description
       * @memberof rep.proto.CreClaStruct
       * @instance
       */

      CreClaStruct.prototype.description = "";
      /**
       * CreClaStruct creator.
       * @member {string} creator
       * @memberof rep.proto.CreClaStruct
       * @instance
       */

      CreClaStruct.prototype.creator = "";
      /**
       * CreClaStruct created.
       * @member {string} created
       * @memberof rep.proto.CreClaStruct
       * @instance
       */

      CreClaStruct.prototype.created = "";
      /**
       * CreClaStruct valid.
       * @member {boolean} valid
       * @memberof rep.proto.CreClaStruct
       * @instance
       */

      CreClaStruct.prototype.valid = false;
      /**
       * CreClaStruct attributes.
       * @member {Array.<rep.proto.ICreAttr>} attributes
       * @memberof rep.proto.CreClaStruct
       * @instance
       */

      CreClaStruct.prototype.attributes = $util.emptyArray;
      /**
       * Creates a new CreClaStruct instance using the specified properties.
       * @function create
       * @memberof rep.proto.CreClaStruct
       * @static
       * @param {rep.proto.ICreClaStruct=} [properties] Properties to set
       * @returns {rep.proto.CreClaStruct} CreClaStruct instance
       */

      CreClaStruct.create = function create(properties) {
        return new CreClaStruct(properties);
      };
      /**
       * Encodes the specified CreClaStruct message. Does not implicitly {@link rep.proto.CreClaStruct.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.CreClaStruct
       * @static
       * @param {rep.proto.ICreClaStruct} message CreClaStruct message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      CreClaStruct.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.version);
        if (message.id != null && Object.hasOwnProperty.call(message, "id")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.id);
        if (message.name != null && Object.hasOwnProperty.call(message, "name")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).string(message.name);
        if (message.ccsVersion != null && Object.hasOwnProperty.call(message, "ccsVersion")) writer.uint32(
        /* id 4, wireType 2 =*/
        34).string(message.ccsVersion);
        if (message.description != null && Object.hasOwnProperty.call(message, "description")) writer.uint32(
        /* id 5, wireType 2 =*/
        42).string(message.description);
        if (message.creator != null && Object.hasOwnProperty.call(message, "creator")) writer.uint32(
        /* id 6, wireType 2 =*/
        50).string(message.creator);
        if (message.created != null && Object.hasOwnProperty.call(message, "created")) writer.uint32(
        /* id 7, wireType 2 =*/
        58).string(message.created);
        if (message.valid != null && Object.hasOwnProperty.call(message, "valid")) writer.uint32(
        /* id 8, wireType 0 =*/
        64).bool(message.valid);
        if (message.attributes != null && message.attributes.length) for (var i = 0; i < message.attributes.length; ++i) {
          $root.rep.proto.CreAttr.encode(message.attributes[i], writer.uint32(
          /* id 9, wireType 2 =*/
          74).fork()).ldelim();
        }
        return writer;
      };
      /**
       * Encodes the specified CreClaStruct message, length delimited. Does not implicitly {@link rep.proto.CreClaStruct.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.CreClaStruct
       * @static
       * @param {rep.proto.ICreClaStruct} message CreClaStruct message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      CreClaStruct.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a CreClaStruct message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.CreClaStruct
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.CreClaStruct} CreClaStruct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      CreClaStruct.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.CreClaStruct();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.version = reader.string();
              break;

            case 2:
              message.id = reader.string();
              break;

            case 3:
              message.name = reader.string();
              break;

            case 4:
              message.ccsVersion = reader.string();
              break;

            case 5:
              message.description = reader.string();
              break;

            case 6:
              message.creator = reader.string();
              break;

            case 7:
              message.created = reader.string();
              break;

            case 8:
              message.valid = reader.bool();
              break;

            case 9:
              if (!(message.attributes && message.attributes.length)) message.attributes = [];
              message.attributes.push($root.rep.proto.CreAttr.decode(reader, reader.uint32()));
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a CreClaStruct message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.CreClaStruct
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.CreClaStruct} CreClaStruct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      CreClaStruct.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a CreClaStruct message.
       * @function verify
       * @memberof rep.proto.CreClaStruct
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      CreClaStruct.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isString(message.version)) return "version: string expected";
        if (message.id != null && message.hasOwnProperty("id")) if (!$util.isString(message.id)) return "id: string expected";
        if (message.name != null && message.hasOwnProperty("name")) if (!$util.isString(message.name)) return "name: string expected";
        if (message.ccsVersion != null && message.hasOwnProperty("ccsVersion")) if (!$util.isString(message.ccsVersion)) return "ccsVersion: string expected";
        if (message.description != null && message.hasOwnProperty("description")) if (!$util.isString(message.description)) return "description: string expected";
        if (message.creator != null && message.hasOwnProperty("creator")) if (!$util.isString(message.creator)) return "creator: string expected";
        if (message.created != null && message.hasOwnProperty("created")) if (!$util.isString(message.created)) return "created: string expected";
        if (message.valid != null && message.hasOwnProperty("valid")) if (typeof message.valid !== "boolean") return "valid: boolean expected";

        if (message.attributes != null && message.hasOwnProperty("attributes")) {
          if (!Array.isArray(message.attributes)) return "attributes: array expected";

          for (var i = 0; i < message.attributes.length; ++i) {
            var error = $root.rep.proto.CreAttr.verify(message.attributes[i]);
            if (error) return "attributes." + error;
          }
        }

        return null;
      };
      /**
       * Creates a CreClaStruct message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.CreClaStruct
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.CreClaStruct} CreClaStruct
       */


      CreClaStruct.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.CreClaStruct) return object;
        var message = new $root.rep.proto.CreClaStruct();
        if (object.version != null) message.version = String(object.version);
        if (object.id != null) message.id = String(object.id);
        if (object.name != null) message.name = String(object.name);
        if (object.ccsVersion != null) message.ccsVersion = String(object.ccsVersion);
        if (object.description != null) message.description = String(object.description);
        if (object.creator != null) message.creator = String(object.creator);
        if (object.created != null) message.created = String(object.created);
        if (object.valid != null) message.valid = Boolean(object.valid);

        if (object.attributes) {
          if (!Array.isArray(object.attributes)) throw TypeError(".rep.proto.CreClaStruct.attributes: array expected");
          message.attributes = [];

          for (var i = 0; i < object.attributes.length; ++i) {
            if ((0, _typeof2["default"])(object.attributes[i]) !== "object") throw TypeError(".rep.proto.CreClaStruct.attributes: object expected");
            message.attributes[i] = $root.rep.proto.CreAttr.fromObject(object.attributes[i]);
          }
        }

        return message;
      };
      /**
       * Creates a plain object from a CreClaStruct message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.CreClaStruct
       * @static
       * @param {rep.proto.CreClaStruct} message CreClaStruct
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      CreClaStruct.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.attributes = [];

        if (options.defaults) {
          object.version = "";
          object.id = "";
          object.name = "";
          object.ccsVersion = "";
          object.description = "";
          object.creator = "";
          object.created = "";
          object.valid = false;
        }

        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        if (message.id != null && message.hasOwnProperty("id")) object.id = message.id;
        if (message.name != null && message.hasOwnProperty("name")) object.name = message.name;
        if (message.ccsVersion != null && message.hasOwnProperty("ccsVersion")) object.ccsVersion = message.ccsVersion;
        if (message.description != null && message.hasOwnProperty("description")) object.description = message.description;
        if (message.creator != null && message.hasOwnProperty("creator")) object.creator = message.creator;
        if (message.created != null && message.hasOwnProperty("created")) object.created = message.created;
        if (message.valid != null && message.hasOwnProperty("valid")) object.valid = message.valid;

        if (message.attributes && message.attributes.length) {
          object.attributes = [];

          for (var j = 0; j < message.attributes.length; ++j) {
            object.attributes[j] = $root.rep.proto.CreAttr.toObject(message.attributes[j], options);
          }
        }

        return object;
      };
      /**
       * Converts this CreClaStruct to JSON.
       * @function toJSON
       * @memberof rep.proto.CreClaStruct
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      CreClaStruct.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return CreClaStruct;
    }();

    proto.CreAttr = function () {
      /**
       * Properties of a CreAttr.
       * @memberof rep.proto
       * @interface ICreAttr
       * @property {string|null} [name] CreAttr name
       * @property {string|null} [type] CreAttr type
       * @property {boolean|null} [required] CreAttr required
       * @property {string|null} [description] CreAttr description
       */

      /**
       * Constructs a new CreAttr.
       * @memberof rep.proto
       * @classdesc Represents a CreAttr.
       * @implements ICreAttr
       * @constructor
       * @param {rep.proto.ICreAttr=} [properties] Properties to set
       */
      function CreAttr(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * CreAttr name.
       * @member {string} name
       * @memberof rep.proto.CreAttr
       * @instance
       */


      CreAttr.prototype.name = "";
      /**
       * CreAttr type.
       * @member {string} type
       * @memberof rep.proto.CreAttr
       * @instance
       */

      CreAttr.prototype.type = "";
      /**
       * CreAttr required.
       * @member {boolean} required
       * @memberof rep.proto.CreAttr
       * @instance
       */

      CreAttr.prototype.required = false;
      /**
       * CreAttr description.
       * @member {string} description
       * @memberof rep.proto.CreAttr
       * @instance
       */

      CreAttr.prototype.description = "";
      /**
       * Creates a new CreAttr instance using the specified properties.
       * @function create
       * @memberof rep.proto.CreAttr
       * @static
       * @param {rep.proto.ICreAttr=} [properties] Properties to set
       * @returns {rep.proto.CreAttr} CreAttr instance
       */

      CreAttr.create = function create(properties) {
        return new CreAttr(properties);
      };
      /**
       * Encodes the specified CreAttr message. Does not implicitly {@link rep.proto.CreAttr.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.CreAttr
       * @static
       * @param {rep.proto.ICreAttr} message CreAttr message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      CreAttr.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.name != null && Object.hasOwnProperty.call(message, "name")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.name);
        if (message.type != null && Object.hasOwnProperty.call(message, "type")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.type);
        if (message.required != null && Object.hasOwnProperty.call(message, "required")) writer.uint32(
        /* id 3, wireType 0 =*/
        24).bool(message.required);
        if (message.description != null && Object.hasOwnProperty.call(message, "description")) writer.uint32(
        /* id 4, wireType 2 =*/
        34).string(message.description);
        return writer;
      };
      /**
       * Encodes the specified CreAttr message, length delimited. Does not implicitly {@link rep.proto.CreAttr.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.CreAttr
       * @static
       * @param {rep.proto.ICreAttr} message CreAttr message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      CreAttr.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a CreAttr message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.CreAttr
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.CreAttr} CreAttr
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      CreAttr.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.CreAttr();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.name = reader.string();
              break;

            case 2:
              message.type = reader.string();
              break;

            case 3:
              message.required = reader.bool();
              break;

            case 4:
              message.description = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a CreAttr message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.CreAttr
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.CreAttr} CreAttr
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      CreAttr.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a CreAttr message.
       * @function verify
       * @memberof rep.proto.CreAttr
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      CreAttr.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.name != null && message.hasOwnProperty("name")) if (!$util.isString(message.name)) return "name: string expected";
        if (message.type != null && message.hasOwnProperty("type")) if (!$util.isString(message.type)) return "type: string expected";
        if (message.required != null && message.hasOwnProperty("required")) if (typeof message.required !== "boolean") return "required: boolean expected";
        if (message.description != null && message.hasOwnProperty("description")) if (!$util.isString(message.description)) return "description: string expected";
        return null;
      };
      /**
       * Creates a CreAttr message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.CreAttr
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.CreAttr} CreAttr
       */


      CreAttr.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.CreAttr) return object;
        var message = new $root.rep.proto.CreAttr();
        if (object.name != null) message.name = String(object.name);
        if (object.type != null) message.type = String(object.type);
        if (object.required != null) message.required = Boolean(object.required);
        if (object.description != null) message.description = String(object.description);
        return message;
      };
      /**
       * Creates a plain object from a CreAttr message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.CreAttr
       * @static
       * @param {rep.proto.CreAttr} message CreAttr
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      CreAttr.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.name = "";
          object.type = "";
          object.required = false;
          object.description = "";
        }

        if (message.name != null && message.hasOwnProperty("name")) object.name = message.name;
        if (message.type != null && message.hasOwnProperty("type")) object.type = message.type;
        if (message.required != null && message.hasOwnProperty("required")) object.required = message.required;
        if (message.description != null && message.hasOwnProperty("description")) object.description = message.description;
        return object;
      };
      /**
       * Converts this CreAttr to JSON.
       * @function toJSON
       * @memberof rep.proto.CreAttr
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      CreAttr.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return CreAttr;
    }();

    proto.VerCreStatus = function () {
      /**
       * Properties of a VerCreStatus.
       * @memberof rep.proto
       * @interface IVerCreStatus
       * @property {string|null} [version] VerCreStatus version
       * @property {string|null} [id] VerCreStatus id
       * @property {string|null} [status] VerCreStatus status
       * @property {Array.<string>|null} [revokedClaimIndex] VerCreStatus revokedClaimIndex
       * @property {string|null} [creator] VerCreStatus creator
       */

      /**
       * Constructs a new VerCreStatus.
       * @memberof rep.proto
       * @classdesc Represents a VerCreStatus.
       * @implements IVerCreStatus
       * @constructor
       * @param {rep.proto.IVerCreStatus=} [properties] Properties to set
       */
      function VerCreStatus(properties) {
        this.revokedClaimIndex = [];
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * VerCreStatus version.
       * @member {string} version
       * @memberof rep.proto.VerCreStatus
       * @instance
       */


      VerCreStatus.prototype.version = "";
      /**
       * VerCreStatus id.
       * @member {string} id
       * @memberof rep.proto.VerCreStatus
       * @instance
       */

      VerCreStatus.prototype.id = "";
      /**
       * VerCreStatus status.
       * @member {string} status
       * @memberof rep.proto.VerCreStatus
       * @instance
       */

      VerCreStatus.prototype.status = "";
      /**
       * VerCreStatus revokedClaimIndex.
       * @member {Array.<string>} revokedClaimIndex
       * @memberof rep.proto.VerCreStatus
       * @instance
       */

      VerCreStatus.prototype.revokedClaimIndex = $util.emptyArray;
      /**
       * VerCreStatus creator.
       * @member {string} creator
       * @memberof rep.proto.VerCreStatus
       * @instance
       */

      VerCreStatus.prototype.creator = "";
      /**
       * Creates a new VerCreStatus instance using the specified properties.
       * @function create
       * @memberof rep.proto.VerCreStatus
       * @static
       * @param {rep.proto.IVerCreStatus=} [properties] Properties to set
       * @returns {rep.proto.VerCreStatus} VerCreStatus instance
       */

      VerCreStatus.create = function create(properties) {
        return new VerCreStatus(properties);
      };
      /**
       * Encodes the specified VerCreStatus message. Does not implicitly {@link rep.proto.VerCreStatus.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.VerCreStatus
       * @static
       * @param {rep.proto.IVerCreStatus} message VerCreStatus message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      VerCreStatus.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.version);
        if (message.id != null && Object.hasOwnProperty.call(message, "id")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.id);
        if (message.status != null && Object.hasOwnProperty.call(message, "status")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).string(message.status);
        if (message.revokedClaimIndex != null && message.revokedClaimIndex.length) for (var i = 0; i < message.revokedClaimIndex.length; ++i) {
          writer.uint32(
          /* id 4, wireType 2 =*/
          34).string(message.revokedClaimIndex[i]);
        }
        if (message.creator != null && Object.hasOwnProperty.call(message, "creator")) writer.uint32(
        /* id 5, wireType 2 =*/
        42).string(message.creator);
        return writer;
      };
      /**
       * Encodes the specified VerCreStatus message, length delimited. Does not implicitly {@link rep.proto.VerCreStatus.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.VerCreStatus
       * @static
       * @param {rep.proto.IVerCreStatus} message VerCreStatus message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      VerCreStatus.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a VerCreStatus message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.VerCreStatus
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.VerCreStatus} VerCreStatus
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      VerCreStatus.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.VerCreStatus();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.version = reader.string();
              break;

            case 2:
              message.id = reader.string();
              break;

            case 3:
              message.status = reader.string();
              break;

            case 4:
              if (!(message.revokedClaimIndex && message.revokedClaimIndex.length)) message.revokedClaimIndex = [];
              message.revokedClaimIndex.push(reader.string());
              break;

            case 5:
              message.creator = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a VerCreStatus message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.VerCreStatus
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.VerCreStatus} VerCreStatus
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      VerCreStatus.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a VerCreStatus message.
       * @function verify
       * @memberof rep.proto.VerCreStatus
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      VerCreStatus.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isString(message.version)) return "version: string expected";
        if (message.id != null && message.hasOwnProperty("id")) if (!$util.isString(message.id)) return "id: string expected";
        if (message.status != null && message.hasOwnProperty("status")) if (!$util.isString(message.status)) return "status: string expected";

        if (message.revokedClaimIndex != null && message.hasOwnProperty("revokedClaimIndex")) {
          if (!Array.isArray(message.revokedClaimIndex)) return "revokedClaimIndex: array expected";

          for (var i = 0; i < message.revokedClaimIndex.length; ++i) {
            if (!$util.isString(message.revokedClaimIndex[i])) return "revokedClaimIndex: string[] expected";
          }
        }

        if (message.creator != null && message.hasOwnProperty("creator")) if (!$util.isString(message.creator)) return "creator: string expected";
        return null;
      };
      /**
       * Creates a VerCreStatus message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.VerCreStatus
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.VerCreStatus} VerCreStatus
       */


      VerCreStatus.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.VerCreStatus) return object;
        var message = new $root.rep.proto.VerCreStatus();
        if (object.version != null) message.version = String(object.version);
        if (object.id != null) message.id = String(object.id);
        if (object.status != null) message.status = String(object.status);

        if (object.revokedClaimIndex) {
          if (!Array.isArray(object.revokedClaimIndex)) throw TypeError(".rep.proto.VerCreStatus.revokedClaimIndex: array expected");
          message.revokedClaimIndex = [];

          for (var i = 0; i < object.revokedClaimIndex.length; ++i) {
            message.revokedClaimIndex[i] = String(object.revokedClaimIndex[i]);
          }
        }

        if (object.creator != null) message.creator = String(object.creator);
        return message;
      };
      /**
       * Creates a plain object from a VerCreStatus message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.VerCreStatus
       * @static
       * @param {rep.proto.VerCreStatus} message VerCreStatus
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      VerCreStatus.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.revokedClaimIndex = [];

        if (options.defaults) {
          object.version = "";
          object.id = "";
          object.status = "";
          object.creator = "";
        }

        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        if (message.id != null && message.hasOwnProperty("id")) object.id = message.id;
        if (message.status != null && message.hasOwnProperty("status")) object.status = message.status;

        if (message.revokedClaimIndex && message.revokedClaimIndex.length) {
          object.revokedClaimIndex = [];

          for (var j = 0; j < message.revokedClaimIndex.length; ++j) {
            object.revokedClaimIndex[j] = message.revokedClaimIndex[j];
          }
        }

        if (message.creator != null && message.hasOwnProperty("creator")) object.creator = message.creator;
        return object;
      };
      /**
       * Converts this VerCreStatus to JSON.
       * @function toJSON
       * @memberof rep.proto.VerCreStatus
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      VerCreStatus.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return VerCreStatus;
    }();

    proto.Signature = function () {
      /**
       * Properties of a Signature.
       * @memberof rep.proto
       * @interface ISignature
       * @property {rep.proto.ICertId|null} [certId] Signature certId
       * @property {google.protobuf.ITimestamp|null} [tmLocal] Signature tmLocal
       * @property {Uint8Array|null} [signature] Signature signature
       */

      /**
       * Constructs a new Signature.
       * @memberof rep.proto
       * @classdesc Represents a Signature.
       * @implements ISignature
       * @constructor
       * @param {rep.proto.ISignature=} [properties] Properties to set
       */
      function Signature(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * Signature certId.
       * @member {rep.proto.ICertId|null|undefined} certId
       * @memberof rep.proto.Signature
       * @instance
       */


      Signature.prototype.certId = null;
      /**
       * Signature tmLocal.
       * @member {google.protobuf.ITimestamp|null|undefined} tmLocal
       * @memberof rep.proto.Signature
       * @instance
       */

      Signature.prototype.tmLocal = null;
      /**
       * Signature signature.
       * @member {Uint8Array} signature
       * @memberof rep.proto.Signature
       * @instance
       */

      Signature.prototype.signature = $util.newBuffer([]);
      /**
       * Creates a new Signature instance using the specified properties.
       * @function create
       * @memberof rep.proto.Signature
       * @static
       * @param {rep.proto.ISignature=} [properties] Properties to set
       * @returns {rep.proto.Signature} Signature instance
       */

      Signature.create = function create(properties) {
        return new Signature(properties);
      };
      /**
       * Encodes the specified Signature message. Does not implicitly {@link rep.proto.Signature.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.Signature
       * @static
       * @param {rep.proto.ISignature} message Signature message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Signature.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.certId != null && Object.hasOwnProperty.call(message, "certId")) $root.rep.proto.CertId.encode(message.certId, writer.uint32(
        /* id 1, wireType 2 =*/
        10).fork()).ldelim();
        if (message.tmLocal != null && Object.hasOwnProperty.call(message, "tmLocal")) $root.google.protobuf.Timestamp.encode(message.tmLocal, writer.uint32(
        /* id 2, wireType 2 =*/
        18).fork()).ldelim();
        if (message.signature != null && Object.hasOwnProperty.call(message, "signature")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).bytes(message.signature);
        return writer;
      };
      /**
       * Encodes the specified Signature message, length delimited. Does not implicitly {@link rep.proto.Signature.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.Signature
       * @static
       * @param {rep.proto.ISignature} message Signature message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Signature.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a Signature message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.Signature
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.Signature} Signature
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Signature.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.Signature();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.certId = $root.rep.proto.CertId.decode(reader, reader.uint32());
              break;

            case 2:
              message.tmLocal = $root.google.protobuf.Timestamp.decode(reader, reader.uint32());
              break;

            case 3:
              message.signature = reader.bytes();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a Signature message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.Signature
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.Signature} Signature
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Signature.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a Signature message.
       * @function verify
       * @memberof rep.proto.Signature
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      Signature.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";

        if (message.certId != null && message.hasOwnProperty("certId")) {
          var error = $root.rep.proto.CertId.verify(message.certId);
          if (error) return "certId." + error;
        }

        if (message.tmLocal != null && message.hasOwnProperty("tmLocal")) {
          var _error7 = $root.google.protobuf.Timestamp.verify(message.tmLocal);

          if (_error7) return "tmLocal." + _error7;
        }

        if (message.signature != null && message.hasOwnProperty("signature")) if (!(message.signature && typeof message.signature.length === "number" || $util.isString(message.signature))) return "signature: buffer expected";
        return null;
      };
      /**
       * Creates a Signature message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.Signature
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.Signature} Signature
       */


      Signature.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.Signature) return object;
        var message = new $root.rep.proto.Signature();

        if (object.certId != null) {
          if ((0, _typeof2["default"])(object.certId) !== "object") throw TypeError(".rep.proto.Signature.certId: object expected");
          message.certId = $root.rep.proto.CertId.fromObject(object.certId);
        }

        if (object.tmLocal != null) {
          if ((0, _typeof2["default"])(object.tmLocal) !== "object") throw TypeError(".rep.proto.Signature.tmLocal: object expected");
          message.tmLocal = $root.google.protobuf.Timestamp.fromObject(object.tmLocal);
        }

        if (object.signature != null) if (typeof object.signature === "string") $util.base64.decode(object.signature, message.signature = $util.newBuffer($util.base64.length(object.signature)), 0);else if (object.signature.length) message.signature = object.signature;
        return message;
      };
      /**
       * Creates a plain object from a Signature message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.Signature
       * @static
       * @param {rep.proto.Signature} message Signature
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      Signature.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.certId = null;
          object.tmLocal = null;
          if (options.bytes === String) object.signature = "";else {
            object.signature = [];
            if (options.bytes !== Array) object.signature = $util.newBuffer(object.signature);
          }
        }

        if (message.certId != null && message.hasOwnProperty("certId")) object.certId = $root.rep.proto.CertId.toObject(message.certId, options);
        if (message.tmLocal != null && message.hasOwnProperty("tmLocal")) object.tmLocal = $root.google.protobuf.Timestamp.toObject(message.tmLocal, options);
        if (message.signature != null && message.hasOwnProperty("signature")) object.signature = options.bytes === String ? $util.base64.encode(message.signature, 0, message.signature.length) : options.bytes === Array ? Array.prototype.slice.call(message.signature) : message.signature;
        return object;
      };
      /**
       * Converts this Signature to JSON.
       * @function toJSON
       * @memberof rep.proto.Signature
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      Signature.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Signature;
    }();

    proto.ChaincodeInput = function () {
      /**
       * Properties of a ChaincodeInput.
       * @memberof rep.proto
       * @interface IChaincodeInput
       * @property {string|null} ["function"] ChaincodeInput function
       * @property {Array.<string>|null} [args] ChaincodeInput args
       */

      /**
       * Constructs a new ChaincodeInput.
       * @memberof rep.proto
       * @classdesc Represents a ChaincodeInput.
       * @implements IChaincodeInput
       * @constructor
       * @param {rep.proto.IChaincodeInput=} [properties] Properties to set
       */
      function ChaincodeInput(properties) {
        this.args = [];
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * ChaincodeInput function.
       * @member {string} function
       * @memberof rep.proto.ChaincodeInput
       * @instance
       */


      ChaincodeInput.prototype["function"] = "";
      /**
       * ChaincodeInput args.
       * @member {Array.<string>} args
       * @memberof rep.proto.ChaincodeInput
       * @instance
       */

      ChaincodeInput.prototype.args = $util.emptyArray;
      /**
       * Creates a new ChaincodeInput instance using the specified properties.
       * @function create
       * @memberof rep.proto.ChaincodeInput
       * @static
       * @param {rep.proto.IChaincodeInput=} [properties] Properties to set
       * @returns {rep.proto.ChaincodeInput} ChaincodeInput instance
       */

      ChaincodeInput.create = function create(properties) {
        return new ChaincodeInput(properties);
      };
      /**
       * Encodes the specified ChaincodeInput message. Does not implicitly {@link rep.proto.ChaincodeInput.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.ChaincodeInput
       * @static
       * @param {rep.proto.IChaincodeInput} message ChaincodeInput message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      ChaincodeInput.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message["function"] != null && Object.hasOwnProperty.call(message, "function")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message["function"]);
        if (message.args != null && message.args.length) for (var i = 0; i < message.args.length; ++i) {
          writer.uint32(
          /* id 2, wireType 2 =*/
          18).string(message.args[i]);
        }
        return writer;
      };
      /**
       * Encodes the specified ChaincodeInput message, length delimited. Does not implicitly {@link rep.proto.ChaincodeInput.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.ChaincodeInput
       * @static
       * @param {rep.proto.IChaincodeInput} message ChaincodeInput message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      ChaincodeInput.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a ChaincodeInput message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.ChaincodeInput
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.ChaincodeInput} ChaincodeInput
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      ChaincodeInput.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.ChaincodeInput();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message["function"] = reader.string();
              break;

            case 2:
              if (!(message.args && message.args.length)) message.args = [];
              message.args.push(reader.string());
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a ChaincodeInput message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.ChaincodeInput
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.ChaincodeInput} ChaincodeInput
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      ChaincodeInput.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a ChaincodeInput message.
       * @function verify
       * @memberof rep.proto.ChaincodeInput
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      ChaincodeInput.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message["function"] != null && message.hasOwnProperty("function")) if (!$util.isString(message["function"])) return "function: string expected";

        if (message.args != null && message.hasOwnProperty("args")) {
          if (!Array.isArray(message.args)) return "args: array expected";

          for (var i = 0; i < message.args.length; ++i) {
            if (!$util.isString(message.args[i])) return "args: string[] expected";
          }
        }

        return null;
      };
      /**
       * Creates a ChaincodeInput message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.ChaincodeInput
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.ChaincodeInput} ChaincodeInput
       */


      ChaincodeInput.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.ChaincodeInput) return object;
        var message = new $root.rep.proto.ChaincodeInput();
        if (object["function"] != null) message["function"] = String(object["function"]);

        if (object.args) {
          if (!Array.isArray(object.args)) throw TypeError(".rep.proto.ChaincodeInput.args: array expected");
          message.args = [];

          for (var i = 0; i < object.args.length; ++i) {
            message.args[i] = String(object.args[i]);
          }
        }

        return message;
      };
      /**
       * Creates a plain object from a ChaincodeInput message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.ChaincodeInput
       * @static
       * @param {rep.proto.ChaincodeInput} message ChaincodeInput
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      ChaincodeInput.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.args = [];
        if (options.defaults) object["function"] = "";
        if (message["function"] != null && message.hasOwnProperty("function")) object["function"] = message["function"];

        if (message.args && message.args.length) {
          object.args = [];

          for (var j = 0; j < message.args.length; ++j) {
            object.args[j] = message.args[j];
          }
        }

        return object;
      };
      /**
       * Converts this ChaincodeInput to JSON.
       * @function toJSON
       * @memberof rep.proto.ChaincodeInput
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      ChaincodeInput.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return ChaincodeInput;
    }();

    proto.ChaincodeId = function () {
      /**
       * Properties of a ChaincodeId.
       * @memberof rep.proto
       * @interface IChaincodeId
       * @property {string|null} [chaincodeName] ChaincodeId chaincodeName
       * @property {number|null} [version] ChaincodeId version
       */

      /**
       * Constructs a new ChaincodeId.
       * @memberof rep.proto
       * @classdesc Represents a ChaincodeId.
       * @implements IChaincodeId
       * @constructor
       * @param {rep.proto.IChaincodeId=} [properties] Properties to set
       */
      function ChaincodeId(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * ChaincodeId chaincodeName.
       * @member {string} chaincodeName
       * @memberof rep.proto.ChaincodeId
       * @instance
       */


      ChaincodeId.prototype.chaincodeName = "";
      /**
       * ChaincodeId version.
       * @member {number} version
       * @memberof rep.proto.ChaincodeId
       * @instance
       */

      ChaincodeId.prototype.version = 0;
      /**
       * Creates a new ChaincodeId instance using the specified properties.
       * @function create
       * @memberof rep.proto.ChaincodeId
       * @static
       * @param {rep.proto.IChaincodeId=} [properties] Properties to set
       * @returns {rep.proto.ChaincodeId} ChaincodeId instance
       */

      ChaincodeId.create = function create(properties) {
        return new ChaincodeId(properties);
      };
      /**
       * Encodes the specified ChaincodeId message. Does not implicitly {@link rep.proto.ChaincodeId.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.ChaincodeId
       * @static
       * @param {rep.proto.IChaincodeId} message ChaincodeId message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      ChaincodeId.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.chaincodeName != null && Object.hasOwnProperty.call(message, "chaincodeName")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.chaincodeName);
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 2, wireType 0 =*/
        16).int32(message.version);
        return writer;
      };
      /**
       * Encodes the specified ChaincodeId message, length delimited. Does not implicitly {@link rep.proto.ChaincodeId.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.ChaincodeId
       * @static
       * @param {rep.proto.IChaincodeId} message ChaincodeId message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      ChaincodeId.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a ChaincodeId message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.ChaincodeId
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.ChaincodeId} ChaincodeId
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      ChaincodeId.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.ChaincodeId();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.chaincodeName = reader.string();
              break;

            case 2:
              message.version = reader.int32();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a ChaincodeId message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.ChaincodeId
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.ChaincodeId} ChaincodeId
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      ChaincodeId.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a ChaincodeId message.
       * @function verify
       * @memberof rep.proto.ChaincodeId
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      ChaincodeId.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.chaincodeName != null && message.hasOwnProperty("chaincodeName")) if (!$util.isString(message.chaincodeName)) return "chaincodeName: string expected";
        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isInteger(message.version)) return "version: integer expected";
        return null;
      };
      /**
       * Creates a ChaincodeId message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.ChaincodeId
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.ChaincodeId} ChaincodeId
       */


      ChaincodeId.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.ChaincodeId) return object;
        var message = new $root.rep.proto.ChaincodeId();
        if (object.chaincodeName != null) message.chaincodeName = String(object.chaincodeName);
        if (object.version != null) message.version = object.version | 0;
        return message;
      };
      /**
       * Creates a plain object from a ChaincodeId message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.ChaincodeId
       * @static
       * @param {rep.proto.ChaincodeId} message ChaincodeId
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      ChaincodeId.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.chaincodeName = "";
          object.version = 0;
        }

        if (message.chaincodeName != null && message.hasOwnProperty("chaincodeName")) object.chaincodeName = message.chaincodeName;
        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        return object;
      };
      /**
       * Converts this ChaincodeId to JSON.
       * @function toJSON
       * @memberof rep.proto.ChaincodeId
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      ChaincodeId.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return ChaincodeId;
    }();

    proto.Block = function () {
      /**
       * Properties of a Block.
       * @memberof rep.proto
       * @interface IBlock
       * @property {rep.proto.IBlockHeader|null} [header] Block header
       * @property {Array.<rep.proto.ITransaction>|null} [transactions] Block transactions
       * @property {Array.<rep.proto.ITransactionResult>|null} [transactionResults] Block transactionResults
       * @property {rep.proto.ITransaction|null} [regTx] Block regTx
       */

      /**
       * Constructs a new Block.
       * @memberof rep.proto
       * @classdesc Represents a Block.
       * @implements IBlock
       * @constructor
       * @param {rep.proto.IBlock=} [properties] Properties to set
       */
      function Block(properties) {
        this.transactions = [];
        this.transactionResults = [];
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * Block header.
       * @member {rep.proto.IBlockHeader|null|undefined} header
       * @memberof rep.proto.Block
       * @instance
       */


      Block.prototype.header = null;
      /**
       * Block transactions.
       * @member {Array.<rep.proto.ITransaction>} transactions
       * @memberof rep.proto.Block
       * @instance
       */

      Block.prototype.transactions = $util.emptyArray;
      /**
       * Block transactionResults.
       * @member {Array.<rep.proto.ITransactionResult>} transactionResults
       * @memberof rep.proto.Block
       * @instance
       */

      Block.prototype.transactionResults = $util.emptyArray;
      /**
       * Block regTx.
       * @member {rep.proto.ITransaction|null|undefined} regTx
       * @memberof rep.proto.Block
       * @instance
       */

      Block.prototype.regTx = null;
      /**
       * Creates a new Block instance using the specified properties.
       * @function create
       * @memberof rep.proto.Block
       * @static
       * @param {rep.proto.IBlock=} [properties] Properties to set
       * @returns {rep.proto.Block} Block instance
       */

      Block.create = function create(properties) {
        return new Block(properties);
      };
      /**
       * Encodes the specified Block message. Does not implicitly {@link rep.proto.Block.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.Block
       * @static
       * @param {rep.proto.IBlock} message Block message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Block.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.header != null && Object.hasOwnProperty.call(message, "header")) $root.rep.proto.BlockHeader.encode(message.header, writer.uint32(
        /* id 1, wireType 2 =*/
        10).fork()).ldelim();
        if (message.transactions != null && message.transactions.length) for (var i = 0; i < message.transactions.length; ++i) {
          $root.rep.proto.Transaction.encode(message.transactions[i], writer.uint32(
          /* id 3, wireType 2 =*/
          26).fork()).ldelim();
        }
        if (message.transactionResults != null && message.transactionResults.length) for (var _i16 = 0; _i16 < message.transactionResults.length; ++_i16) {
          $root.rep.proto.TransactionResult.encode(message.transactionResults[_i16], writer.uint32(
          /* id 4, wireType 2 =*/
          34).fork()).ldelim();
        }
        if (message.regTx != null && Object.hasOwnProperty.call(message, "regTx")) $root.rep.proto.Transaction.encode(message.regTx, writer.uint32(
        /* id 15, wireType 2 =*/
        122).fork()).ldelim();
        return writer;
      };
      /**
       * Encodes the specified Block message, length delimited. Does not implicitly {@link rep.proto.Block.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.Block
       * @static
       * @param {rep.proto.IBlock} message Block message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Block.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a Block message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.Block
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.Block} Block
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Block.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.Block();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.header = $root.rep.proto.BlockHeader.decode(reader, reader.uint32());
              break;

            case 3:
              if (!(message.transactions && message.transactions.length)) message.transactions = [];
              message.transactions.push($root.rep.proto.Transaction.decode(reader, reader.uint32()));
              break;

            case 4:
              if (!(message.transactionResults && message.transactionResults.length)) message.transactionResults = [];
              message.transactionResults.push($root.rep.proto.TransactionResult.decode(reader, reader.uint32()));
              break;

            case 15:
              message.regTx = $root.rep.proto.Transaction.decode(reader, reader.uint32());
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a Block message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.Block
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.Block} Block
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Block.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a Block message.
       * @function verify
       * @memberof rep.proto.Block
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      Block.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";

        if (message.header != null && message.hasOwnProperty("header")) {
          var error = $root.rep.proto.BlockHeader.verify(message.header);
          if (error) return "header." + error;
        }

        if (message.transactions != null && message.hasOwnProperty("transactions")) {
          if (!Array.isArray(message.transactions)) return "transactions: array expected";

          for (var i = 0; i < message.transactions.length; ++i) {
            var _error8 = $root.rep.proto.Transaction.verify(message.transactions[i]);

            if (_error8) return "transactions." + _error8;
          }
        }

        if (message.transactionResults != null && message.hasOwnProperty("transactionResults")) {
          if (!Array.isArray(message.transactionResults)) return "transactionResults: array expected";

          for (var _i17 = 0; _i17 < message.transactionResults.length; ++_i17) {
            var _error9 = $root.rep.proto.TransactionResult.verify(message.transactionResults[_i17]);

            if (_error9) return "transactionResults." + _error9;
          }
        }

        if (message.regTx != null && message.hasOwnProperty("regTx")) {
          var _error10 = $root.rep.proto.Transaction.verify(message.regTx);

          if (_error10) return "regTx." + _error10;
        }

        return null;
      };
      /**
       * Creates a Block message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.Block
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.Block} Block
       */


      Block.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.Block) return object;
        var message = new $root.rep.proto.Block();

        if (object.header != null) {
          if ((0, _typeof2["default"])(object.header) !== "object") throw TypeError(".rep.proto.Block.header: object expected");
          message.header = $root.rep.proto.BlockHeader.fromObject(object.header);
        }

        if (object.transactions) {
          if (!Array.isArray(object.transactions)) throw TypeError(".rep.proto.Block.transactions: array expected");
          message.transactions = [];

          for (var i = 0; i < object.transactions.length; ++i) {
            if ((0, _typeof2["default"])(object.transactions[i]) !== "object") throw TypeError(".rep.proto.Block.transactions: object expected");
            message.transactions[i] = $root.rep.proto.Transaction.fromObject(object.transactions[i]);
          }
        }

        if (object.transactionResults) {
          if (!Array.isArray(object.transactionResults)) throw TypeError(".rep.proto.Block.transactionResults: array expected");
          message.transactionResults = [];

          for (var _i18 = 0; _i18 < object.transactionResults.length; ++_i18) {
            if ((0, _typeof2["default"])(object.transactionResults[_i18]) !== "object") throw TypeError(".rep.proto.Block.transactionResults: object expected");
            message.transactionResults[_i18] = $root.rep.proto.TransactionResult.fromObject(object.transactionResults[_i18]);
          }
        }

        if (object.regTx != null) {
          if ((0, _typeof2["default"])(object.regTx) !== "object") throw TypeError(".rep.proto.Block.regTx: object expected");
          message.regTx = $root.rep.proto.Transaction.fromObject(object.regTx);
        }

        return message;
      };
      /**
       * Creates a plain object from a Block message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.Block
       * @static
       * @param {rep.proto.Block} message Block
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      Block.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.arrays || options.defaults) {
          object.transactions = [];
          object.transactionResults = [];
        }

        if (options.defaults) {
          object.header = null;
          object.regTx = null;
        }

        if (message.header != null && message.hasOwnProperty("header")) object.header = $root.rep.proto.BlockHeader.toObject(message.header, options);

        if (message.transactions && message.transactions.length) {
          object.transactions = [];

          for (var j = 0; j < message.transactions.length; ++j) {
            object.transactions[j] = $root.rep.proto.Transaction.toObject(message.transactions[j], options);
          }
        }

        if (message.transactionResults && message.transactionResults.length) {
          object.transactionResults = [];

          for (var _j6 = 0; _j6 < message.transactionResults.length; ++_j6) {
            object.transactionResults[_j6] = $root.rep.proto.TransactionResult.toObject(message.transactionResults[_j6], options);
          }
        }

        if (message.regTx != null && message.hasOwnProperty("regTx")) object.regTx = $root.rep.proto.Transaction.toObject(message.regTx, options);
        return object;
      };
      /**
       * Converts this Block to JSON.
       * @function toJSON
       * @memberof rep.proto.Block
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      Block.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Block;
    }();

    proto.BlockHeader = function () {
      /**
       * Properties of a BlockHeader.
       * @memberof rep.proto
       * @interface IBlockHeader
       * @property {number|null} [version] BlockHeader version
       * @property {number|Long|null} [height] BlockHeader height
       * @property {Uint8Array|null} [commitTx] BlockHeader commitTx
       * @property {Uint8Array|null} [commitTxResult] BlockHeader commitTxResult
       * @property {Uint8Array|null} [hashPresent] BlockHeader hashPresent
       * @property {Uint8Array|null} [hashPrevious] BlockHeader hashPrevious
       * @property {Uint8Array|null} [commitState] BlockHeader commitState
       * @property {Uint8Array|null} [commitStateGlobal] BlockHeader commitStateGlobal
       * @property {number|Long|null} [heightExpired] BlockHeader heightExpired
       * @property {Array.<rep.proto.ISignature>|null} [endorsements] BlockHeader endorsements
       */

      /**
       * Constructs a new BlockHeader.
       * @memberof rep.proto
       * @classdesc Represents a BlockHeader.
       * @implements IBlockHeader
       * @constructor
       * @param {rep.proto.IBlockHeader=} [properties] Properties to set
       */
      function BlockHeader(properties) {
        this.endorsements = [];
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * BlockHeader version.
       * @member {number} version
       * @memberof rep.proto.BlockHeader
       * @instance
       */


      BlockHeader.prototype.version = 0;
      /**
       * BlockHeader height.
       * @member {number|Long} height
       * @memberof rep.proto.BlockHeader
       * @instance
       */

      BlockHeader.prototype.height = $util.Long ? $util.Long.fromBits(0, 0, true) : 0;
      /**
       * BlockHeader commitTx.
       * @member {Uint8Array} commitTx
       * @memberof rep.proto.BlockHeader
       * @instance
       */

      BlockHeader.prototype.commitTx = $util.newBuffer([]);
      /**
       * BlockHeader commitTxResult.
       * @member {Uint8Array} commitTxResult
       * @memberof rep.proto.BlockHeader
       * @instance
       */

      BlockHeader.prototype.commitTxResult = $util.newBuffer([]);
      /**
       * BlockHeader hashPresent.
       * @member {Uint8Array} hashPresent
       * @memberof rep.proto.BlockHeader
       * @instance
       */

      BlockHeader.prototype.hashPresent = $util.newBuffer([]);
      /**
       * BlockHeader hashPrevious.
       * @member {Uint8Array} hashPrevious
       * @memberof rep.proto.BlockHeader
       * @instance
       */

      BlockHeader.prototype.hashPrevious = $util.newBuffer([]);
      /**
       * BlockHeader commitState.
       * @member {Uint8Array} commitState
       * @memberof rep.proto.BlockHeader
       * @instance
       */

      BlockHeader.prototype.commitState = $util.newBuffer([]);
      /**
       * BlockHeader commitStateGlobal.
       * @member {Uint8Array} commitStateGlobal
       * @memberof rep.proto.BlockHeader
       * @instance
       */

      BlockHeader.prototype.commitStateGlobal = $util.newBuffer([]);
      /**
       * BlockHeader heightExpired.
       * @member {number|Long} heightExpired
       * @memberof rep.proto.BlockHeader
       * @instance
       */

      BlockHeader.prototype.heightExpired = $util.Long ? $util.Long.fromBits(0, 0, true) : 0;
      /**
       * BlockHeader endorsements.
       * @member {Array.<rep.proto.ISignature>} endorsements
       * @memberof rep.proto.BlockHeader
       * @instance
       */

      BlockHeader.prototype.endorsements = $util.emptyArray;
      /**
       * Creates a new BlockHeader instance using the specified properties.
       * @function create
       * @memberof rep.proto.BlockHeader
       * @static
       * @param {rep.proto.IBlockHeader=} [properties] Properties to set
       * @returns {rep.proto.BlockHeader} BlockHeader instance
       */

      BlockHeader.create = function create(properties) {
        return new BlockHeader(properties);
      };
      /**
       * Encodes the specified BlockHeader message. Does not implicitly {@link rep.proto.BlockHeader.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.BlockHeader
       * @static
       * @param {rep.proto.IBlockHeader} message BlockHeader message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      BlockHeader.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.version != null && Object.hasOwnProperty.call(message, "version")) writer.uint32(
        /* id 1, wireType 0 =*/
        8).uint32(message.version);
        if (message.height != null && Object.hasOwnProperty.call(message, "height")) writer.uint32(
        /* id 2, wireType 0 =*/
        16).uint64(message.height);
        if (message.commitTx != null && Object.hasOwnProperty.call(message, "commitTx")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).bytes(message.commitTx);
        if (message.commitTxResult != null && Object.hasOwnProperty.call(message, "commitTxResult")) writer.uint32(
        /* id 4, wireType 2 =*/
        34).bytes(message.commitTxResult);
        if (message.hashPresent != null && Object.hasOwnProperty.call(message, "hashPresent")) writer.uint32(
        /* id 5, wireType 2 =*/
        42).bytes(message.hashPresent);
        if (message.hashPrevious != null && Object.hasOwnProperty.call(message, "hashPrevious")) writer.uint32(
        /* id 6, wireType 2 =*/
        50).bytes(message.hashPrevious);
        if (message.commitState != null && Object.hasOwnProperty.call(message, "commitState")) writer.uint32(
        /* id 7, wireType 2 =*/
        58).bytes(message.commitState);
        if (message.commitStateGlobal != null && Object.hasOwnProperty.call(message, "commitStateGlobal")) writer.uint32(
        /* id 8, wireType 2 =*/
        66).bytes(message.commitStateGlobal);
        if (message.heightExpired != null && Object.hasOwnProperty.call(message, "heightExpired")) writer.uint32(
        /* id 9, wireType 0 =*/
        72).uint64(message.heightExpired);
        if (message.endorsements != null && message.endorsements.length) for (var i = 0; i < message.endorsements.length; ++i) {
          $root.rep.proto.Signature.encode(message.endorsements[i], writer.uint32(
          /* id 10, wireType 2 =*/
          82).fork()).ldelim();
        }
        return writer;
      };
      /**
       * Encodes the specified BlockHeader message, length delimited. Does not implicitly {@link rep.proto.BlockHeader.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.BlockHeader
       * @static
       * @param {rep.proto.IBlockHeader} message BlockHeader message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      BlockHeader.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a BlockHeader message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.BlockHeader
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.BlockHeader} BlockHeader
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      BlockHeader.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.BlockHeader();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.version = reader.uint32();
              break;

            case 2:
              message.height = reader.uint64();
              break;

            case 3:
              message.commitTx = reader.bytes();
              break;

            case 4:
              message.commitTxResult = reader.bytes();
              break;

            case 5:
              message.hashPresent = reader.bytes();
              break;

            case 6:
              message.hashPrevious = reader.bytes();
              break;

            case 7:
              message.commitState = reader.bytes();
              break;

            case 8:
              message.commitStateGlobal = reader.bytes();
              break;

            case 9:
              message.heightExpired = reader.uint64();
              break;

            case 10:
              if (!(message.endorsements && message.endorsements.length)) message.endorsements = [];
              message.endorsements.push($root.rep.proto.Signature.decode(reader, reader.uint32()));
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a BlockHeader message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.BlockHeader
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.BlockHeader} BlockHeader
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      BlockHeader.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a BlockHeader message.
       * @function verify
       * @memberof rep.proto.BlockHeader
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      BlockHeader.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.version != null && message.hasOwnProperty("version")) if (!$util.isInteger(message.version)) return "version: integer expected";
        if (message.height != null && message.hasOwnProperty("height")) if (!$util.isInteger(message.height) && !(message.height && $util.isInteger(message.height.low) && $util.isInteger(message.height.high))) return "height: integer|Long expected";
        if (message.commitTx != null && message.hasOwnProperty("commitTx")) if (!(message.commitTx && typeof message.commitTx.length === "number" || $util.isString(message.commitTx))) return "commitTx: buffer expected";
        if (message.commitTxResult != null && message.hasOwnProperty("commitTxResult")) if (!(message.commitTxResult && typeof message.commitTxResult.length === "number" || $util.isString(message.commitTxResult))) return "commitTxResult: buffer expected";
        if (message.hashPresent != null && message.hasOwnProperty("hashPresent")) if (!(message.hashPresent && typeof message.hashPresent.length === "number" || $util.isString(message.hashPresent))) return "hashPresent: buffer expected";
        if (message.hashPrevious != null && message.hasOwnProperty("hashPrevious")) if (!(message.hashPrevious && typeof message.hashPrevious.length === "number" || $util.isString(message.hashPrevious))) return "hashPrevious: buffer expected";
        if (message.commitState != null && message.hasOwnProperty("commitState")) if (!(message.commitState && typeof message.commitState.length === "number" || $util.isString(message.commitState))) return "commitState: buffer expected";
        if (message.commitStateGlobal != null && message.hasOwnProperty("commitStateGlobal")) if (!(message.commitStateGlobal && typeof message.commitStateGlobal.length === "number" || $util.isString(message.commitStateGlobal))) return "commitStateGlobal: buffer expected";
        if (message.heightExpired != null && message.hasOwnProperty("heightExpired")) if (!$util.isInteger(message.heightExpired) && !(message.heightExpired && $util.isInteger(message.heightExpired.low) && $util.isInteger(message.heightExpired.high))) return "heightExpired: integer|Long expected";

        if (message.endorsements != null && message.hasOwnProperty("endorsements")) {
          if (!Array.isArray(message.endorsements)) return "endorsements: array expected";

          for (var i = 0; i < message.endorsements.length; ++i) {
            var error = $root.rep.proto.Signature.verify(message.endorsements[i]);
            if (error) return "endorsements." + error;
          }
        }

        return null;
      };
      /**
       * Creates a BlockHeader message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.BlockHeader
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.BlockHeader} BlockHeader
       */


      BlockHeader.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.BlockHeader) return object;
        var message = new $root.rep.proto.BlockHeader();
        if (object.version != null) message.version = object.version >>> 0;
        if (object.height != null) if ($util.Long) (message.height = $util.Long.fromValue(object.height)).unsigned = true;else if (typeof object.height === "string") message.height = parseInt(object.height, 10);else if (typeof object.height === "number") message.height = object.height;else if ((0, _typeof2["default"])(object.height) === "object") message.height = new $util.LongBits(object.height.low >>> 0, object.height.high >>> 0).toNumber(true);
        if (object.commitTx != null) if (typeof object.commitTx === "string") $util.base64.decode(object.commitTx, message.commitTx = $util.newBuffer($util.base64.length(object.commitTx)), 0);else if (object.commitTx.length) message.commitTx = object.commitTx;
        if (object.commitTxResult != null) if (typeof object.commitTxResult === "string") $util.base64.decode(object.commitTxResult, message.commitTxResult = $util.newBuffer($util.base64.length(object.commitTxResult)), 0);else if (object.commitTxResult.length) message.commitTxResult = object.commitTxResult;
        if (object.hashPresent != null) if (typeof object.hashPresent === "string") $util.base64.decode(object.hashPresent, message.hashPresent = $util.newBuffer($util.base64.length(object.hashPresent)), 0);else if (object.hashPresent.length) message.hashPresent = object.hashPresent;
        if (object.hashPrevious != null) if (typeof object.hashPrevious === "string") $util.base64.decode(object.hashPrevious, message.hashPrevious = $util.newBuffer($util.base64.length(object.hashPrevious)), 0);else if (object.hashPrevious.length) message.hashPrevious = object.hashPrevious;
        if (object.commitState != null) if (typeof object.commitState === "string") $util.base64.decode(object.commitState, message.commitState = $util.newBuffer($util.base64.length(object.commitState)), 0);else if (object.commitState.length) message.commitState = object.commitState;
        if (object.commitStateGlobal != null) if (typeof object.commitStateGlobal === "string") $util.base64.decode(object.commitStateGlobal, message.commitStateGlobal = $util.newBuffer($util.base64.length(object.commitStateGlobal)), 0);else if (object.commitStateGlobal.length) message.commitStateGlobal = object.commitStateGlobal;
        if (object.heightExpired != null) if ($util.Long) (message.heightExpired = $util.Long.fromValue(object.heightExpired)).unsigned = true;else if (typeof object.heightExpired === "string") message.heightExpired = parseInt(object.heightExpired, 10);else if (typeof object.heightExpired === "number") message.heightExpired = object.heightExpired;else if ((0, _typeof2["default"])(object.heightExpired) === "object") message.heightExpired = new $util.LongBits(object.heightExpired.low >>> 0, object.heightExpired.high >>> 0).toNumber(true);

        if (object.endorsements) {
          if (!Array.isArray(object.endorsements)) throw TypeError(".rep.proto.BlockHeader.endorsements: array expected");
          message.endorsements = [];

          for (var i = 0; i < object.endorsements.length; ++i) {
            if ((0, _typeof2["default"])(object.endorsements[i]) !== "object") throw TypeError(".rep.proto.BlockHeader.endorsements: object expected");
            message.endorsements[i] = $root.rep.proto.Signature.fromObject(object.endorsements[i]);
          }
        }

        return message;
      };
      /**
       * Creates a plain object from a BlockHeader message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.BlockHeader
       * @static
       * @param {rep.proto.BlockHeader} message BlockHeader
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      BlockHeader.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.endorsements = [];

        if (options.defaults) {
          object.version = 0;

          if ($util.Long) {
            var _long = new $util.Long(0, 0, true);

            object.height = options.longs === String ? _long.toString() : options.longs === Number ? _long.toNumber() : _long;
          } else object.height = options.longs === String ? "0" : 0;

          if (options.bytes === String) object.commitTx = "";else {
            object.commitTx = [];
            if (options.bytes !== Array) object.commitTx = $util.newBuffer(object.commitTx);
          }
          if (options.bytes === String) object.commitTxResult = "";else {
            object.commitTxResult = [];
            if (options.bytes !== Array) object.commitTxResult = $util.newBuffer(object.commitTxResult);
          }
          if (options.bytes === String) object.hashPresent = "";else {
            object.hashPresent = [];
            if (options.bytes !== Array) object.hashPresent = $util.newBuffer(object.hashPresent);
          }
          if (options.bytes === String) object.hashPrevious = "";else {
            object.hashPrevious = [];
            if (options.bytes !== Array) object.hashPrevious = $util.newBuffer(object.hashPrevious);
          }
          if (options.bytes === String) object.commitState = "";else {
            object.commitState = [];
            if (options.bytes !== Array) object.commitState = $util.newBuffer(object.commitState);
          }
          if (options.bytes === String) object.commitStateGlobal = "";else {
            object.commitStateGlobal = [];
            if (options.bytes !== Array) object.commitStateGlobal = $util.newBuffer(object.commitStateGlobal);
          }

          if ($util.Long) {
            var _long2 = new $util.Long(0, 0, true);

            object.heightExpired = options.longs === String ? _long2.toString() : options.longs === Number ? _long2.toNumber() : _long2;
          } else object.heightExpired = options.longs === String ? "0" : 0;
        }

        if (message.version != null && message.hasOwnProperty("version")) object.version = message.version;
        if (message.height != null && message.hasOwnProperty("height")) if (typeof message.height === "number") object.height = options.longs === String ? String(message.height) : message.height;else object.height = options.longs === String ? $util.Long.prototype.toString.call(message.height) : options.longs === Number ? new $util.LongBits(message.height.low >>> 0, message.height.high >>> 0).toNumber(true) : message.height;
        if (message.commitTx != null && message.hasOwnProperty("commitTx")) object.commitTx = options.bytes === String ? $util.base64.encode(message.commitTx, 0, message.commitTx.length) : options.bytes === Array ? Array.prototype.slice.call(message.commitTx) : message.commitTx;
        if (message.commitTxResult != null && message.hasOwnProperty("commitTxResult")) object.commitTxResult = options.bytes === String ? $util.base64.encode(message.commitTxResult, 0, message.commitTxResult.length) : options.bytes === Array ? Array.prototype.slice.call(message.commitTxResult) : message.commitTxResult;
        if (message.hashPresent != null && message.hasOwnProperty("hashPresent")) object.hashPresent = options.bytes === String ? $util.base64.encode(message.hashPresent, 0, message.hashPresent.length) : options.bytes === Array ? Array.prototype.slice.call(message.hashPresent) : message.hashPresent;
        if (message.hashPrevious != null && message.hasOwnProperty("hashPrevious")) object.hashPrevious = options.bytes === String ? $util.base64.encode(message.hashPrevious, 0, message.hashPrevious.length) : options.bytes === Array ? Array.prototype.slice.call(message.hashPrevious) : message.hashPrevious;
        if (message.commitState != null && message.hasOwnProperty("commitState")) object.commitState = options.bytes === String ? $util.base64.encode(message.commitState, 0, message.commitState.length) : options.bytes === Array ? Array.prototype.slice.call(message.commitState) : message.commitState;
        if (message.commitStateGlobal != null && message.hasOwnProperty("commitStateGlobal")) object.commitStateGlobal = options.bytes === String ? $util.base64.encode(message.commitStateGlobal, 0, message.commitStateGlobal.length) : options.bytes === Array ? Array.prototype.slice.call(message.commitStateGlobal) : message.commitStateGlobal;
        if (message.heightExpired != null && message.hasOwnProperty("heightExpired")) if (typeof message.heightExpired === "number") object.heightExpired = options.longs === String ? String(message.heightExpired) : message.heightExpired;else object.heightExpired = options.longs === String ? $util.Long.prototype.toString.call(message.heightExpired) : options.longs === Number ? new $util.LongBits(message.heightExpired.low >>> 0, message.heightExpired.high >>> 0).toNumber(true) : message.heightExpired;

        if (message.endorsements && message.endorsements.length) {
          object.endorsements = [];

          for (var j = 0; j < message.endorsements.length; ++j) {
            object.endorsements[j] = $root.rep.proto.Signature.toObject(message.endorsements[j], options);
          }
        }

        return object;
      };
      /**
       * Converts this BlockHeader to JSON.
       * @function toJSON
       * @memberof rep.proto.BlockHeader
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      BlockHeader.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return BlockHeader;
    }();

    proto.TransactionError = function () {
      /**
       * Properties of a TransactionError.
       * @memberof rep.proto
       * @interface ITransactionError
       * @property {string|null} [txId] TransactionError txId
       * @property {number|null} [code] TransactionError code
       * @property {string|null} [reason] TransactionError reason
       */

      /**
       * Constructs a new TransactionError.
       * @memberof rep.proto
       * @classdesc Represents a TransactionError.
       * @implements ITransactionError
       * @constructor
       * @param {rep.proto.ITransactionError=} [properties] Properties to set
       */
      function TransactionError(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * TransactionError txId.
       * @member {string} txId
       * @memberof rep.proto.TransactionError
       * @instance
       */


      TransactionError.prototype.txId = "";
      /**
       * TransactionError code.
       * @member {number} code
       * @memberof rep.proto.TransactionError
       * @instance
       */

      TransactionError.prototype.code = 0;
      /**
       * TransactionError reason.
       * @member {string} reason
       * @memberof rep.proto.TransactionError
       * @instance
       */

      TransactionError.prototype.reason = "";
      /**
       * Creates a new TransactionError instance using the specified properties.
       * @function create
       * @memberof rep.proto.TransactionError
       * @static
       * @param {rep.proto.ITransactionError=} [properties] Properties to set
       * @returns {rep.proto.TransactionError} TransactionError instance
       */

      TransactionError.create = function create(properties) {
        return new TransactionError(properties);
      };
      /**
       * Encodes the specified TransactionError message. Does not implicitly {@link rep.proto.TransactionError.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.TransactionError
       * @static
       * @param {rep.proto.ITransactionError} message TransactionError message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      TransactionError.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.txId != null && Object.hasOwnProperty.call(message, "txId")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.txId);
        if (message.code != null && Object.hasOwnProperty.call(message, "code")) writer.uint32(
        /* id 2, wireType 0 =*/
        16).int32(message.code);
        if (message.reason != null && Object.hasOwnProperty.call(message, "reason")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).string(message.reason);
        return writer;
      };
      /**
       * Encodes the specified TransactionError message, length delimited. Does not implicitly {@link rep.proto.TransactionError.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.TransactionError
       * @static
       * @param {rep.proto.ITransactionError} message TransactionError message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      TransactionError.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a TransactionError message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.TransactionError
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.TransactionError} TransactionError
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      TransactionError.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.TransactionError();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.txId = reader.string();
              break;

            case 2:
              message.code = reader.int32();
              break;

            case 3:
              message.reason = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a TransactionError message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.TransactionError
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.TransactionError} TransactionError
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      TransactionError.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a TransactionError message.
       * @function verify
       * @memberof rep.proto.TransactionError
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      TransactionError.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.txId != null && message.hasOwnProperty("txId")) if (!$util.isString(message.txId)) return "txId: string expected";
        if (message.code != null && message.hasOwnProperty("code")) if (!$util.isInteger(message.code)) return "code: integer expected";
        if (message.reason != null && message.hasOwnProperty("reason")) if (!$util.isString(message.reason)) return "reason: string expected";
        return null;
      };
      /**
       * Creates a TransactionError message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.TransactionError
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.TransactionError} TransactionError
       */


      TransactionError.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.TransactionError) return object;
        var message = new $root.rep.proto.TransactionError();
        if (object.txId != null) message.txId = String(object.txId);
        if (object.code != null) message.code = object.code | 0;
        if (object.reason != null) message.reason = String(object.reason);
        return message;
      };
      /**
       * Creates a plain object from a TransactionError message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.TransactionError
       * @static
       * @param {rep.proto.TransactionError} message TransactionError
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      TransactionError.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.txId = "";
          object.code = 0;
          object.reason = "";
        }

        if (message.txId != null && message.hasOwnProperty("txId")) object.txId = message.txId;
        if (message.code != null && message.hasOwnProperty("code")) object.code = message.code;
        if (message.reason != null && message.hasOwnProperty("reason")) object.reason = message.reason;
        return object;
      };
      /**
       * Converts this TransactionError to JSON.
       * @function toJSON
       * @memberof rep.proto.TransactionError
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      TransactionError.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return TransactionError;
    }();

    proto.TransactionResult = function () {
      /**
       * Properties of a TransactionResult.
       * @memberof rep.proto
       * @interface ITransactionResult
       * @property {string|null} [txId] TransactionResult txId
       * @property {Object.<string,Uint8Array>|null} [statesGet] TransactionResult statesGet
       * @property {Object.<string,Uint8Array>|null} [statesSet] TransactionResult statesSet
       * @property {Object.<string,Uint8Array>|null} [statesDel] TransactionResult statesDel
       * @property {rep.proto.IActionResult|null} [err] TransactionResult err
       */

      /**
       * Constructs a new TransactionResult.
       * @memberof rep.proto
       * @classdesc Represents a TransactionResult.
       * @implements ITransactionResult
       * @constructor
       * @param {rep.proto.ITransactionResult=} [properties] Properties to set
       */
      function TransactionResult(properties) {
        this.statesGet = {};
        this.statesSet = {};
        this.statesDel = {};
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * TransactionResult txId.
       * @member {string} txId
       * @memberof rep.proto.TransactionResult
       * @instance
       */


      TransactionResult.prototype.txId = "";
      /**
       * TransactionResult statesGet.
       * @member {Object.<string,Uint8Array>} statesGet
       * @memberof rep.proto.TransactionResult
       * @instance
       */

      TransactionResult.prototype.statesGet = $util.emptyObject;
      /**
       * TransactionResult statesSet.
       * @member {Object.<string,Uint8Array>} statesSet
       * @memberof rep.proto.TransactionResult
       * @instance
       */

      TransactionResult.prototype.statesSet = $util.emptyObject;
      /**
       * TransactionResult statesDel.
       * @member {Object.<string,Uint8Array>} statesDel
       * @memberof rep.proto.TransactionResult
       * @instance
       */

      TransactionResult.prototype.statesDel = $util.emptyObject;
      /**
       * TransactionResult err.
       * @member {rep.proto.IActionResult|null|undefined} err
       * @memberof rep.proto.TransactionResult
       * @instance
       */

      TransactionResult.prototype.err = null;
      /**
       * Creates a new TransactionResult instance using the specified properties.
       * @function create
       * @memberof rep.proto.TransactionResult
       * @static
       * @param {rep.proto.ITransactionResult=} [properties] Properties to set
       * @returns {rep.proto.TransactionResult} TransactionResult instance
       */

      TransactionResult.create = function create(properties) {
        return new TransactionResult(properties);
      };
      /**
       * Encodes the specified TransactionResult message. Does not implicitly {@link rep.proto.TransactionResult.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.TransactionResult
       * @static
       * @param {rep.proto.ITransactionResult} message TransactionResult message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      TransactionResult.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.txId != null && Object.hasOwnProperty.call(message, "txId")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.txId);
        if (message.statesGet != null && Object.hasOwnProperty.call(message, "statesGet")) for (var keys = Object.keys(message.statesGet), i = 0; i < keys.length; ++i) {
          writer.uint32(
          /* id 2, wireType 2 =*/
          18).fork().uint32(
          /* id 1, wireType 2 =*/
          10).string(keys[i]).uint32(
          /* id 2, wireType 2 =*/
          18).bytes(message.statesGet[keys[i]]).ldelim();
        }
        if (message.statesSet != null && Object.hasOwnProperty.call(message, "statesSet")) for (var _keys = Object.keys(message.statesSet), _i19 = 0; _i19 < _keys.length; ++_i19) {
          writer.uint32(
          /* id 3, wireType 2 =*/
          26).fork().uint32(
          /* id 1, wireType 2 =*/
          10).string(_keys[_i19]).uint32(
          /* id 2, wireType 2 =*/
          18).bytes(message.statesSet[_keys[_i19]]).ldelim();
        }
        if (message.statesDel != null && Object.hasOwnProperty.call(message, "statesDel")) for (var _keys2 = Object.keys(message.statesDel), _i20 = 0; _i20 < _keys2.length; ++_i20) {
          writer.uint32(
          /* id 4, wireType 2 =*/
          34).fork().uint32(
          /* id 1, wireType 2 =*/
          10).string(_keys2[_i20]).uint32(
          /* id 2, wireType 2 =*/
          18).bytes(message.statesDel[_keys2[_i20]]).ldelim();
        }
        if (message.err != null && Object.hasOwnProperty.call(message, "err")) $root.rep.proto.ActionResult.encode(message.err, writer.uint32(
        /* id 5, wireType 2 =*/
        42).fork()).ldelim();
        return writer;
      };
      /**
       * Encodes the specified TransactionResult message, length delimited. Does not implicitly {@link rep.proto.TransactionResult.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.TransactionResult
       * @static
       * @param {rep.proto.ITransactionResult} message TransactionResult message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      TransactionResult.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a TransactionResult message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.TransactionResult
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.TransactionResult} TransactionResult
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      TransactionResult.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.TransactionResult(),
            key,
            value;

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.txId = reader.string();
              break;

            case 2:
              {
                if (message.statesGet === $util.emptyObject) message.statesGet = {};
                var end2 = reader.uint32() + reader.pos;
                key = "";
                value = [];

                while (reader.pos < end2) {
                  var tag2 = reader.uint32();

                  switch (tag2 >>> 3) {
                    case 1:
                      key = reader.string();
                      break;

                    case 2:
                      value = reader.bytes();
                      break;

                    default:
                      reader.skipType(tag2 & 7);
                      break;
                  }
                }

                message.statesGet[key] = value;
                break;
              }

            case 3:
              {
                if (message.statesSet === $util.emptyObject) message.statesSet = {};

                var _end = reader.uint32() + reader.pos;

                key = "";
                value = [];

                while (reader.pos < _end) {
                  var _tag = reader.uint32();

                  switch (_tag >>> 3) {
                    case 1:
                      key = reader.string();
                      break;

                    case 2:
                      value = reader.bytes();
                      break;

                    default:
                      reader.skipType(_tag & 7);
                      break;
                  }
                }

                message.statesSet[key] = value;
                break;
              }

            case 4:
              {
                if (message.statesDel === $util.emptyObject) message.statesDel = {};

                var _end2 = reader.uint32() + reader.pos;

                key = "";
                value = [];

                while (reader.pos < _end2) {
                  var _tag2 = reader.uint32();

                  switch (_tag2 >>> 3) {
                    case 1:
                      key = reader.string();
                      break;

                    case 2:
                      value = reader.bytes();
                      break;

                    default:
                      reader.skipType(_tag2 & 7);
                      break;
                  }
                }

                message.statesDel[key] = value;
                break;
              }

            case 5:
              message.err = $root.rep.proto.ActionResult.decode(reader, reader.uint32());
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a TransactionResult message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.TransactionResult
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.TransactionResult} TransactionResult
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      TransactionResult.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a TransactionResult message.
       * @function verify
       * @memberof rep.proto.TransactionResult
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      TransactionResult.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.txId != null && message.hasOwnProperty("txId")) if (!$util.isString(message.txId)) return "txId: string expected";

        if (message.statesGet != null && message.hasOwnProperty("statesGet")) {
          if (!$util.isObject(message.statesGet)) return "statesGet: object expected";
          var key = Object.keys(message.statesGet);

          for (var i = 0; i < key.length; ++i) {
            if (!(message.statesGet[key[i]] && typeof message.statesGet[key[i]].length === "number" || $util.isString(message.statesGet[key[i]]))) return "statesGet: buffer{k:string} expected";
          }
        }

        if (message.statesSet != null && message.hasOwnProperty("statesSet")) {
          if (!$util.isObject(message.statesSet)) return "statesSet: object expected";

          var _key = Object.keys(message.statesSet);

          for (var _i21 = 0; _i21 < _key.length; ++_i21) {
            if (!(message.statesSet[_key[_i21]] && typeof message.statesSet[_key[_i21]].length === "number" || $util.isString(message.statesSet[_key[_i21]]))) return "statesSet: buffer{k:string} expected";
          }
        }

        if (message.statesDel != null && message.hasOwnProperty("statesDel")) {
          if (!$util.isObject(message.statesDel)) return "statesDel: object expected";

          var _key2 = Object.keys(message.statesDel);

          for (var _i22 = 0; _i22 < _key2.length; ++_i22) {
            if (!(message.statesDel[_key2[_i22]] && typeof message.statesDel[_key2[_i22]].length === "number" || $util.isString(message.statesDel[_key2[_i22]]))) return "statesDel: buffer{k:string} expected";
          }
        }

        if (message.err != null && message.hasOwnProperty("err")) {
          var error = $root.rep.proto.ActionResult.verify(message.err);
          if (error) return "err." + error;
        }

        return null;
      };
      /**
       * Creates a TransactionResult message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.TransactionResult
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.TransactionResult} TransactionResult
       */


      TransactionResult.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.TransactionResult) return object;
        var message = new $root.rep.proto.TransactionResult();
        if (object.txId != null) message.txId = String(object.txId);

        if (object.statesGet) {
          if ((0, _typeof2["default"])(object.statesGet) !== "object") throw TypeError(".rep.proto.TransactionResult.statesGet: object expected");
          message.statesGet = {};

          for (var keys = Object.keys(object.statesGet), i = 0; i < keys.length; ++i) {
            if (typeof object.statesGet[keys[i]] === "string") $util.base64.decode(object.statesGet[keys[i]], message.statesGet[keys[i]] = $util.newBuffer($util.base64.length(object.statesGet[keys[i]])), 0);else if (object.statesGet[keys[i]].length) message.statesGet[keys[i]] = object.statesGet[keys[i]];
          }
        }

        if (object.statesSet) {
          if ((0, _typeof2["default"])(object.statesSet) !== "object") throw TypeError(".rep.proto.TransactionResult.statesSet: object expected");
          message.statesSet = {};

          for (var _keys3 = Object.keys(object.statesSet), _i23 = 0; _i23 < _keys3.length; ++_i23) {
            if (typeof object.statesSet[_keys3[_i23]] === "string") $util.base64.decode(object.statesSet[_keys3[_i23]], message.statesSet[_keys3[_i23]] = $util.newBuffer($util.base64.length(object.statesSet[_keys3[_i23]])), 0);else if (object.statesSet[_keys3[_i23]].length) message.statesSet[_keys3[_i23]] = object.statesSet[_keys3[_i23]];
          }
        }

        if (object.statesDel) {
          if ((0, _typeof2["default"])(object.statesDel) !== "object") throw TypeError(".rep.proto.TransactionResult.statesDel: object expected");
          message.statesDel = {};

          for (var _keys4 = Object.keys(object.statesDel), _i24 = 0; _i24 < _keys4.length; ++_i24) {
            if (typeof object.statesDel[_keys4[_i24]] === "string") $util.base64.decode(object.statesDel[_keys4[_i24]], message.statesDel[_keys4[_i24]] = $util.newBuffer($util.base64.length(object.statesDel[_keys4[_i24]])), 0);else if (object.statesDel[_keys4[_i24]].length) message.statesDel[_keys4[_i24]] = object.statesDel[_keys4[_i24]];
          }
        }

        if (object.err != null) {
          if ((0, _typeof2["default"])(object.err) !== "object") throw TypeError(".rep.proto.TransactionResult.err: object expected");
          message.err = $root.rep.proto.ActionResult.fromObject(object.err);
        }

        return message;
      };
      /**
       * Creates a plain object from a TransactionResult message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.TransactionResult
       * @static
       * @param {rep.proto.TransactionResult} message TransactionResult
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      TransactionResult.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.objects || options.defaults) {
          object.statesGet = {};
          object.statesSet = {};
          object.statesDel = {};
        }

        if (options.defaults) {
          object.txId = "";
          object.err = null;
        }

        if (message.txId != null && message.hasOwnProperty("txId")) object.txId = message.txId;
        var keys2;

        if (message.statesGet && (keys2 = Object.keys(message.statesGet)).length) {
          object.statesGet = {};

          for (var j = 0; j < keys2.length; ++j) {
            object.statesGet[keys2[j]] = options.bytes === String ? $util.base64.encode(message.statesGet[keys2[j]], 0, message.statesGet[keys2[j]].length) : options.bytes === Array ? Array.prototype.slice.call(message.statesGet[keys2[j]]) : message.statesGet[keys2[j]];
          }
        }

        if (message.statesSet && (keys2 = Object.keys(message.statesSet)).length) {
          object.statesSet = {};

          for (var _j7 = 0; _j7 < keys2.length; ++_j7) {
            object.statesSet[keys2[_j7]] = options.bytes === String ? $util.base64.encode(message.statesSet[keys2[_j7]], 0, message.statesSet[keys2[_j7]].length) : options.bytes === Array ? Array.prototype.slice.call(message.statesSet[keys2[_j7]]) : message.statesSet[keys2[_j7]];
          }
        }

        if (message.statesDel && (keys2 = Object.keys(message.statesDel)).length) {
          object.statesDel = {};

          for (var _j8 = 0; _j8 < keys2.length; ++_j8) {
            object.statesDel[keys2[_j8]] = options.bytes === String ? $util.base64.encode(message.statesDel[keys2[_j8]], 0, message.statesDel[keys2[_j8]].length) : options.bytes === Array ? Array.prototype.slice.call(message.statesDel[keys2[_j8]]) : message.statesDel[keys2[_j8]];
          }
        }

        if (message.err != null && message.hasOwnProperty("err")) object.err = $root.rep.proto.ActionResult.toObject(message.err, options);
        return object;
      };
      /**
       * Converts this TransactionResult to JSON.
       * @function toJSON
       * @memberof rep.proto.TransactionResult
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      TransactionResult.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return TransactionResult;
    }();

    proto.Transaction = function () {
      /**
       * Properties of a Transaction.
       * @memberof rep.proto
       * @interface ITransaction
       * @property {string|null} [id] Transaction id
       * @property {rep.proto.Transaction.Type|null} [type] Transaction type
       * @property {rep.proto.IChaincodeId|null} [cid] Transaction cid
       * @property {rep.proto.IChaincodeDeploy|null} [spec] Transaction spec
       * @property {rep.proto.IChaincodeInput|null} [ipt] Transaction ipt
       * @property {boolean|null} [state] Transaction state
       * @property {number|null} [gasLimit] Transaction gasLimit
       * @property {string|null} [oid] Transaction oid
       * @property {rep.proto.ISignature|null} [signature] Transaction signature
       */

      /**
       * Constructs a new Transaction.
       * @memberof rep.proto
       * @classdesc Represents a Transaction.
       * @implements ITransaction
       * @constructor
       * @param {rep.proto.ITransaction=} [properties] Properties to set
       */
      function Transaction(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * Transaction id.
       * @member {string} id
       * @memberof rep.proto.Transaction
       * @instance
       */


      Transaction.prototype.id = "";
      /**
       * Transaction type.
       * @member {rep.proto.Transaction.Type} type
       * @memberof rep.proto.Transaction
       * @instance
       */

      Transaction.prototype.type = 0;
      /**
       * Transaction cid.
       * @member {rep.proto.IChaincodeId|null|undefined} cid
       * @memberof rep.proto.Transaction
       * @instance
       */

      Transaction.prototype.cid = null;
      /**
       * Transaction spec.
       * @member {rep.proto.IChaincodeDeploy|null|undefined} spec
       * @memberof rep.proto.Transaction
       * @instance
       */

      Transaction.prototype.spec = null;
      /**
       * Transaction ipt.
       * @member {rep.proto.IChaincodeInput|null|undefined} ipt
       * @memberof rep.proto.Transaction
       * @instance
       */

      Transaction.prototype.ipt = null;
      /**
       * Transaction state.
       * @member {boolean|null|undefined} state
       * @memberof rep.proto.Transaction
       * @instance
       */

      Transaction.prototype.state = null;
      /**
       * Transaction gasLimit.
       * @member {number} gasLimit
       * @memberof rep.proto.Transaction
       * @instance
       */

      Transaction.prototype.gasLimit = 0;
      /**
       * Transaction oid.
       * @member {string} oid
       * @memberof rep.proto.Transaction
       * @instance
       */

      Transaction.prototype.oid = "";
      /**
       * Transaction signature.
       * @member {rep.proto.ISignature|null|undefined} signature
       * @memberof rep.proto.Transaction
       * @instance
       */

      Transaction.prototype.signature = null; // OneOf field names bound to virtual getters and setters

      var $oneOfFields;
      /**
       * Transaction para.
       * @member {"spec"|"ipt"|"state"|undefined} para
       * @memberof rep.proto.Transaction
       * @instance
       */

      Object.defineProperty(Transaction.prototype, "para", {
        get: $util.oneOfGetter($oneOfFields = ["spec", "ipt", "state"]),
        set: $util.oneOfSetter($oneOfFields)
      });
      /**
       * Creates a new Transaction instance using the specified properties.
       * @function create
       * @memberof rep.proto.Transaction
       * @static
       * @param {rep.proto.ITransaction=} [properties] Properties to set
       * @returns {rep.proto.Transaction} Transaction instance
       */

      Transaction.create = function create(properties) {
        return new Transaction(properties);
      };
      /**
       * Encodes the specified Transaction message. Does not implicitly {@link rep.proto.Transaction.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.Transaction
       * @static
       * @param {rep.proto.ITransaction} message Transaction message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Transaction.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.id != null && Object.hasOwnProperty.call(message, "id")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.id);
        if (message.type != null && Object.hasOwnProperty.call(message, "type")) writer.uint32(
        /* id 2, wireType 0 =*/
        16).int32(message.type);
        if (message.cid != null && Object.hasOwnProperty.call(message, "cid")) $root.rep.proto.ChaincodeId.encode(message.cid, writer.uint32(
        /* id 3, wireType 2 =*/
        26).fork()).ldelim();
        if (message.spec != null && Object.hasOwnProperty.call(message, "spec")) $root.rep.proto.ChaincodeDeploy.encode(message.spec, writer.uint32(
        /* id 4, wireType 2 =*/
        34).fork()).ldelim();
        if (message.ipt != null && Object.hasOwnProperty.call(message, "ipt")) $root.rep.proto.ChaincodeInput.encode(message.ipt, writer.uint32(
        /* id 5, wireType 2 =*/
        42).fork()).ldelim();
        if (message.state != null && Object.hasOwnProperty.call(message, "state")) writer.uint32(
        /* id 6, wireType 0 =*/
        48).bool(message.state);
        if (message.gasLimit != null && Object.hasOwnProperty.call(message, "gasLimit")) writer.uint32(
        /* id 7, wireType 0 =*/
        56).uint32(message.gasLimit);
        if (message.oid != null && Object.hasOwnProperty.call(message, "oid")) writer.uint32(
        /* id 8, wireType 2 =*/
        66).string(message.oid);
        if (message.signature != null && Object.hasOwnProperty.call(message, "signature")) $root.rep.proto.Signature.encode(message.signature, writer.uint32(
        /* id 9, wireType 2 =*/
        74).fork()).ldelim();
        return writer;
      };
      /**
       * Encodes the specified Transaction message, length delimited. Does not implicitly {@link rep.proto.Transaction.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.Transaction
       * @static
       * @param {rep.proto.ITransaction} message Transaction message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Transaction.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a Transaction message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.Transaction
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.Transaction} Transaction
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Transaction.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.Transaction();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.id = reader.string();
              break;

            case 2:
              message.type = reader.int32();
              break;

            case 3:
              message.cid = $root.rep.proto.ChaincodeId.decode(reader, reader.uint32());
              break;

            case 4:
              message.spec = $root.rep.proto.ChaincodeDeploy.decode(reader, reader.uint32());
              break;

            case 5:
              message.ipt = $root.rep.proto.ChaincodeInput.decode(reader, reader.uint32());
              break;

            case 6:
              message.state = reader.bool();
              break;

            case 7:
              message.gasLimit = reader.uint32();
              break;

            case 8:
              message.oid = reader.string();
              break;

            case 9:
              message.signature = $root.rep.proto.Signature.decode(reader, reader.uint32());
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a Transaction message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.Transaction
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.Transaction} Transaction
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Transaction.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a Transaction message.
       * @function verify
       * @memberof rep.proto.Transaction
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      Transaction.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        var properties = {};
        if (message.id != null && message.hasOwnProperty("id")) if (!$util.isString(message.id)) return "id: string expected";
        if (message.type != null && message.hasOwnProperty("type")) switch (message.type) {
          default:
            return "type: enum value expected";

          case 0:
          case 1:
          case 2:
          case 3:
            break;
        }

        if (message.cid != null && message.hasOwnProperty("cid")) {
          var error = $root.rep.proto.ChaincodeId.verify(message.cid);
          if (error) return "cid." + error;
        }

        if (message.spec != null && message.hasOwnProperty("spec")) {
          properties.para = 1;
          {
            var _error11 = $root.rep.proto.ChaincodeDeploy.verify(message.spec);

            if (_error11) return "spec." + _error11;
          }
        }

        if (message.ipt != null && message.hasOwnProperty("ipt")) {
          if (properties.para === 1) return "para: multiple values";
          properties.para = 1;
          {
            var _error12 = $root.rep.proto.ChaincodeInput.verify(message.ipt);

            if (_error12) return "ipt." + _error12;
          }
        }

        if (message.state != null && message.hasOwnProperty("state")) {
          if (properties.para === 1) return "para: multiple values";
          properties.para = 1;
          if (typeof message.state !== "boolean") return "state: boolean expected";
        }

        if (message.gasLimit != null && message.hasOwnProperty("gasLimit")) if (!$util.isInteger(message.gasLimit)) return "gasLimit: integer expected";
        if (message.oid != null && message.hasOwnProperty("oid")) if (!$util.isString(message.oid)) return "oid: string expected";

        if (message.signature != null && message.hasOwnProperty("signature")) {
          var _error13 = $root.rep.proto.Signature.verify(message.signature);

          if (_error13) return "signature." + _error13;
        }

        return null;
      };
      /**
       * Creates a Transaction message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.Transaction
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.Transaction} Transaction
       */


      Transaction.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.Transaction) return object;
        var message = new $root.rep.proto.Transaction();
        if (object.id != null) message.id = String(object.id);

        switch (object.type) {
          case "UNDEFINED":
          case 0:
            message.type = 0;
            break;

          case "CHAINCODE_DEPLOY":
          case 1:
            message.type = 1;
            break;

          case "CHAINCODE_INVOKE":
          case 2:
            message.type = 2;
            break;

          case "CHAINCODE_SET_STATE":
          case 3:
            message.type = 3;
            break;
        }

        if (object.cid != null) {
          if ((0, _typeof2["default"])(object.cid) !== "object") throw TypeError(".rep.proto.Transaction.cid: object expected");
          message.cid = $root.rep.proto.ChaincodeId.fromObject(object.cid);
        }

        if (object.spec != null) {
          if ((0, _typeof2["default"])(object.spec) !== "object") throw TypeError(".rep.proto.Transaction.spec: object expected");
          message.spec = $root.rep.proto.ChaincodeDeploy.fromObject(object.spec);
        }

        if (object.ipt != null) {
          if ((0, _typeof2["default"])(object.ipt) !== "object") throw TypeError(".rep.proto.Transaction.ipt: object expected");
          message.ipt = $root.rep.proto.ChaincodeInput.fromObject(object.ipt);
        }

        if (object.state != null) message.state = Boolean(object.state);
        if (object.gasLimit != null) message.gasLimit = object.gasLimit >>> 0;
        if (object.oid != null) message.oid = String(object.oid);

        if (object.signature != null) {
          if ((0, _typeof2["default"])(object.signature) !== "object") throw TypeError(".rep.proto.Transaction.signature: object expected");
          message.signature = $root.rep.proto.Signature.fromObject(object.signature);
        }

        return message;
      };
      /**
       * Creates a plain object from a Transaction message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.Transaction
       * @static
       * @param {rep.proto.Transaction} message Transaction
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      Transaction.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.id = "";
          object.type = options.enums === String ? "UNDEFINED" : 0;
          object.cid = null;
          object.gasLimit = 0;
          object.oid = "";
          object.signature = null;
        }

        if (message.id != null && message.hasOwnProperty("id")) object.id = message.id;
        if (message.type != null && message.hasOwnProperty("type")) object.type = options.enums === String ? $root.rep.proto.Transaction.Type[message.type] : message.type;
        if (message.cid != null && message.hasOwnProperty("cid")) object.cid = $root.rep.proto.ChaincodeId.toObject(message.cid, options);

        if (message.spec != null && message.hasOwnProperty("spec")) {
          object.spec = $root.rep.proto.ChaincodeDeploy.toObject(message.spec, options);
          if (options.oneofs) object.para = "spec";
        }

        if (message.ipt != null && message.hasOwnProperty("ipt")) {
          object.ipt = $root.rep.proto.ChaincodeInput.toObject(message.ipt, options);
          if (options.oneofs) object.para = "ipt";
        }

        if (message.state != null && message.hasOwnProperty("state")) {
          object.state = message.state;
          if (options.oneofs) object.para = "state";
        }

        if (message.gasLimit != null && message.hasOwnProperty("gasLimit")) object.gasLimit = message.gasLimit;
        if (message.oid != null && message.hasOwnProperty("oid")) object.oid = message.oid;
        if (message.signature != null && message.hasOwnProperty("signature")) object.signature = $root.rep.proto.Signature.toObject(message.signature, options);
        return object;
      };
      /**
       * Converts this Transaction to JSON.
       * @function toJSON
       * @memberof rep.proto.Transaction
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      Transaction.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };
      /**
       * Type enum.
       * @name rep.proto.Transaction.Type
       * @enum {number}
       * @property {number} UNDEFINED=0 UNDEFINED value
       * @property {number} CHAINCODE_DEPLOY=1 CHAINCODE_DEPLOY value
       * @property {number} CHAINCODE_INVOKE=2 CHAINCODE_INVOKE value
       * @property {number} CHAINCODE_SET_STATE=3 CHAINCODE_SET_STATE value
       */


      Transaction.Type = function () {
        var valuesById = {},
            values = Object.create(valuesById);
        values[valuesById[0] = "UNDEFINED"] = 0;
        values[valuesById[1] = "CHAINCODE_DEPLOY"] = 1;
        values[valuesById[2] = "CHAINCODE_INVOKE"] = 2;
        values[valuesById[3] = "CHAINCODE_SET_STATE"] = 3;
        return values;
      }();

      return Transaction;
    }();

    proto.ChaincodeDeploy = function () {
      /**
       * Properties of a ChaincodeDeploy.
       * @memberof rep.proto
       * @interface IChaincodeDeploy
       * @property {number|null} [timeout] ChaincodeDeploy timeout
       * @property {string|null} [codePackage] ChaincodeDeploy codePackage
       * @property {string|null} [legalProse] ChaincodeDeploy legalProse
       * @property {rep.proto.ChaincodeDeploy.CodeType|null} [cType] ChaincodeDeploy cType
       * @property {rep.proto.ChaincodeDeploy.RunType|null} [rType] ChaincodeDeploy rType
       * @property {rep.proto.ChaincodeDeploy.StateType|null} [sType] ChaincodeDeploy sType
       * @property {string|null} [initParameter] ChaincodeDeploy initParameter
       * @property {rep.proto.ChaincodeDeploy.ContractClassification|null} [cclassification] ChaincodeDeploy cclassification
       */

      /**
       * Constructs a new ChaincodeDeploy.
       * @memberof rep.proto
       * @classdesc Represents a ChaincodeDeploy.
       * @implements IChaincodeDeploy
       * @constructor
       * @param {rep.proto.IChaincodeDeploy=} [properties] Properties to set
       */
      function ChaincodeDeploy(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * ChaincodeDeploy timeout.
       * @member {number} timeout
       * @memberof rep.proto.ChaincodeDeploy
       * @instance
       */


      ChaincodeDeploy.prototype.timeout = 0;
      /**
       * ChaincodeDeploy codePackage.
       * @member {string} codePackage
       * @memberof rep.proto.ChaincodeDeploy
       * @instance
       */

      ChaincodeDeploy.prototype.codePackage = "";
      /**
       * ChaincodeDeploy legalProse.
       * @member {string} legalProse
       * @memberof rep.proto.ChaincodeDeploy
       * @instance
       */

      ChaincodeDeploy.prototype.legalProse = "";
      /**
       * ChaincodeDeploy cType.
       * @member {rep.proto.ChaincodeDeploy.CodeType} cType
       * @memberof rep.proto.ChaincodeDeploy
       * @instance
       */

      ChaincodeDeploy.prototype.cType = 0;
      /**
       * ChaincodeDeploy rType.
       * @member {rep.proto.ChaincodeDeploy.RunType} rType
       * @memberof rep.proto.ChaincodeDeploy
       * @instance
       */

      ChaincodeDeploy.prototype.rType = 0;
      /**
       * ChaincodeDeploy sType.
       * @member {rep.proto.ChaincodeDeploy.StateType} sType
       * @memberof rep.proto.ChaincodeDeploy
       * @instance
       */

      ChaincodeDeploy.prototype.sType = 0;
      /**
       * ChaincodeDeploy initParameter.
       * @member {string} initParameter
       * @memberof rep.proto.ChaincodeDeploy
       * @instance
       */

      ChaincodeDeploy.prototype.initParameter = "";
      /**
       * ChaincodeDeploy cclassification.
       * @member {rep.proto.ChaincodeDeploy.ContractClassification} cclassification
       * @memberof rep.proto.ChaincodeDeploy
       * @instance
       */

      ChaincodeDeploy.prototype.cclassification = 0;
      /**
       * Creates a new ChaincodeDeploy instance using the specified properties.
       * @function create
       * @memberof rep.proto.ChaincodeDeploy
       * @static
       * @param {rep.proto.IChaincodeDeploy=} [properties] Properties to set
       * @returns {rep.proto.ChaincodeDeploy} ChaincodeDeploy instance
       */

      ChaincodeDeploy.create = function create(properties) {
        return new ChaincodeDeploy(properties);
      };
      /**
       * Encodes the specified ChaincodeDeploy message. Does not implicitly {@link rep.proto.ChaincodeDeploy.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.ChaincodeDeploy
       * @static
       * @param {rep.proto.IChaincodeDeploy} message ChaincodeDeploy message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      ChaincodeDeploy.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.timeout != null && Object.hasOwnProperty.call(message, "timeout")) writer.uint32(
        /* id 1, wireType 0 =*/
        8).int32(message.timeout);
        if (message.codePackage != null && Object.hasOwnProperty.call(message, "codePackage")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.codePackage);
        if (message.legalProse != null && Object.hasOwnProperty.call(message, "legalProse")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).string(message.legalProse);
        if (message.cType != null && Object.hasOwnProperty.call(message, "cType")) writer.uint32(
        /* id 4, wireType 0 =*/
        32).int32(message.cType);
        if (message.rType != null && Object.hasOwnProperty.call(message, "rType")) writer.uint32(
        /* id 5, wireType 0 =*/
        40).int32(message.rType);
        if (message.sType != null && Object.hasOwnProperty.call(message, "sType")) writer.uint32(
        /* id 6, wireType 0 =*/
        48).int32(message.sType);
        if (message.initParameter != null && Object.hasOwnProperty.call(message, "initParameter")) writer.uint32(
        /* id 7, wireType 2 =*/
        58).string(message.initParameter);
        if (message.cclassification != null && Object.hasOwnProperty.call(message, "cclassification")) writer.uint32(
        /* id 8, wireType 0 =*/
        64).int32(message.cclassification);
        return writer;
      };
      /**
       * Encodes the specified ChaincodeDeploy message, length delimited. Does not implicitly {@link rep.proto.ChaincodeDeploy.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.ChaincodeDeploy
       * @static
       * @param {rep.proto.IChaincodeDeploy} message ChaincodeDeploy message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      ChaincodeDeploy.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a ChaincodeDeploy message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.ChaincodeDeploy
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.ChaincodeDeploy} ChaincodeDeploy
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      ChaincodeDeploy.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.ChaincodeDeploy();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.timeout = reader.int32();
              break;

            case 2:
              message.codePackage = reader.string();
              break;

            case 3:
              message.legalProse = reader.string();
              break;

            case 4:
              message.cType = reader.int32();
              break;

            case 5:
              message.rType = reader.int32();
              break;

            case 6:
              message.sType = reader.int32();
              break;

            case 7:
              message.initParameter = reader.string();
              break;

            case 8:
              message.cclassification = reader.int32();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a ChaincodeDeploy message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.ChaincodeDeploy
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.ChaincodeDeploy} ChaincodeDeploy
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      ChaincodeDeploy.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a ChaincodeDeploy message.
       * @function verify
       * @memberof rep.proto.ChaincodeDeploy
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      ChaincodeDeploy.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.timeout != null && message.hasOwnProperty("timeout")) if (!$util.isInteger(message.timeout)) return "timeout: integer expected";
        if (message.codePackage != null && message.hasOwnProperty("codePackage")) if (!$util.isString(message.codePackage)) return "codePackage: string expected";
        if (message.legalProse != null && message.hasOwnProperty("legalProse")) if (!$util.isString(message.legalProse)) return "legalProse: string expected";
        if (message.cType != null && message.hasOwnProperty("cType")) switch (message.cType) {
          default:
            return "cType: enum value expected";

          case 0:
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
            break;
        }
        if (message.rType != null && message.hasOwnProperty("rType")) switch (message.rType) {
          default:
            return "rType: enum value expected";

          case 0:
          case 1:
          case 2:
          case 3:
            break;
        }
        if (message.sType != null && message.hasOwnProperty("sType")) switch (message.sType) {
          default:
            return "sType: enum value expected";

          case 0:
          case 1:
          case 2:
            break;
        }
        if (message.initParameter != null && message.hasOwnProperty("initParameter")) if (!$util.isString(message.initParameter)) return "initParameter: string expected";
        if (message.cclassification != null && message.hasOwnProperty("cclassification")) switch (message.cclassification) {
          default:
            return "cclassification: enum value expected";

          case 0:
          case 1:
          case 2:
            break;
        }
        return null;
      };
      /**
       * Creates a ChaincodeDeploy message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.ChaincodeDeploy
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.ChaincodeDeploy} ChaincodeDeploy
       */


      ChaincodeDeploy.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.ChaincodeDeploy) return object;
        var message = new $root.rep.proto.ChaincodeDeploy();
        if (object.timeout != null) message.timeout = object.timeout | 0;
        if (object.codePackage != null) message.codePackage = String(object.codePackage);
        if (object.legalProse != null) message.legalProse = String(object.legalProse);

        switch (object.cType) {
          case "CODE_UNDEFINED":
          case 0:
            message.cType = 0;
            break;

          case "CODE_JAVASCRIPT":
          case 1:
            message.cType = 1;
            break;

          case "CODE_SCALA":
          case 2:
            message.cType = 2;
            break;

          case "CODE_VCL_DLL":
          case 3:
            message.cType = 3;
            break;

          case "CODE_VCL_EXE":
          case 4:
            message.cType = 4;
            break;

          case "CODE_VCL_WASM":
          case 5:
            message.cType = 5;
            break;

          case "CODE_WASM":
          case 6:
            message.cType = 6;
            break;
        }

        switch (object.rType) {
          case "RUN_UNDEFINED":
          case 0:
            message.rType = 0;
            break;

          case "RUN_SERIAL":
          case 1:
            message.rType = 1;
            break;

          case "RUN_PARALLEL":
          case 2:
            message.rType = 2;
            break;

          case "RUN_OPTIONAL":
          case 3:
            message.rType = 3;
            break;
        }

        switch (object.sType) {
          case "STATE_UNDEFINED":
          case 0:
            message.sType = 0;
            break;

          case "STATE_BLOCK":
          case 1:
            message.sType = 1;
            break;

          case "STATE_GLOBAL":
          case 2:
            message.sType = 2;
            break;
        }

        if (object.initParameter != null) message.initParameter = String(object.initParameter);

        switch (object.cclassification) {
          case "CONTRACT_UNDEFINED":
          case 0:
            message.cclassification = 0;
            break;

          case "CONTRACT_SYSTEM":
          case 1:
            message.cclassification = 1;
            break;

          case "CONTRACT_CUSTOM":
          case 2:
            message.cclassification = 2;
            break;
        }

        return message;
      };
      /**
       * Creates a plain object from a ChaincodeDeploy message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.ChaincodeDeploy
       * @static
       * @param {rep.proto.ChaincodeDeploy} message ChaincodeDeploy
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      ChaincodeDeploy.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.timeout = 0;
          object.codePackage = "";
          object.legalProse = "";
          object.cType = options.enums === String ? "CODE_UNDEFINED" : 0;
          object.rType = options.enums === String ? "RUN_UNDEFINED" : 0;
          object.sType = options.enums === String ? "STATE_UNDEFINED" : 0;
          object.initParameter = "";
          object.cclassification = options.enums === String ? "CONTRACT_UNDEFINED" : 0;
        }

        if (message.timeout != null && message.hasOwnProperty("timeout")) object.timeout = message.timeout;
        if (message.codePackage != null && message.hasOwnProperty("codePackage")) object.codePackage = message.codePackage;
        if (message.legalProse != null && message.hasOwnProperty("legalProse")) object.legalProse = message.legalProse;
        if (message.cType != null && message.hasOwnProperty("cType")) object.cType = options.enums === String ? $root.rep.proto.ChaincodeDeploy.CodeType[message.cType] : message.cType;
        if (message.rType != null && message.hasOwnProperty("rType")) object.rType = options.enums === String ? $root.rep.proto.ChaincodeDeploy.RunType[message.rType] : message.rType;
        if (message.sType != null && message.hasOwnProperty("sType")) object.sType = options.enums === String ? $root.rep.proto.ChaincodeDeploy.StateType[message.sType] : message.sType;
        if (message.initParameter != null && message.hasOwnProperty("initParameter")) object.initParameter = message.initParameter;
        if (message.cclassification != null && message.hasOwnProperty("cclassification")) object.cclassification = options.enums === String ? $root.rep.proto.ChaincodeDeploy.ContractClassification[message.cclassification] : message.cclassification;
        return object;
      };
      /**
       * Converts this ChaincodeDeploy to JSON.
       * @function toJSON
       * @memberof rep.proto.ChaincodeDeploy
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      ChaincodeDeploy.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };
      /**
       * CodeType enum.
       * @name rep.proto.ChaincodeDeploy.CodeType
       * @enum {number}
       * @property {number} CODE_UNDEFINED=0 CODE_UNDEFINED value
       * @property {number} CODE_JAVASCRIPT=1 CODE_JAVASCRIPT value
       * @property {number} CODE_SCALA=2 CODE_SCALA value
       * @property {number} CODE_VCL_DLL=3 CODE_VCL_DLL value
       * @property {number} CODE_VCL_EXE=4 CODE_VCL_EXE value
       * @property {number} CODE_VCL_WASM=5 CODE_VCL_WASM value
       * @property {number} CODE_WASM=6 CODE_WASM value
       */


      ChaincodeDeploy.CodeType = function () {
        var valuesById = {},
            values = Object.create(valuesById);
        values[valuesById[0] = "CODE_UNDEFINED"] = 0;
        values[valuesById[1] = "CODE_JAVASCRIPT"] = 1;
        values[valuesById[2] = "CODE_SCALA"] = 2;
        values[valuesById[3] = "CODE_VCL_DLL"] = 3;
        values[valuesById[4] = "CODE_VCL_EXE"] = 4;
        values[valuesById[5] = "CODE_VCL_WASM"] = 5;
        values[valuesById[6] = "CODE_WASM"] = 6;
        return values;
      }();
      /**
       * RunType enum.
       * @name rep.proto.ChaincodeDeploy.RunType
       * @enum {number}
       * @property {number} RUN_UNDEFINED=0 RUN_UNDEFINED value
       * @property {number} RUN_SERIAL=1 RUN_SERIAL value
       * @property {number} RUN_PARALLEL=2 RUN_PARALLEL value
       * @property {number} RUN_OPTIONAL=3 RUN_OPTIONAL value
       */


      ChaincodeDeploy.RunType = function () {
        var valuesById = {},
            values = Object.create(valuesById);
        values[valuesById[0] = "RUN_UNDEFINED"] = 0;
        values[valuesById[1] = "RUN_SERIAL"] = 1;
        values[valuesById[2] = "RUN_PARALLEL"] = 2;
        values[valuesById[3] = "RUN_OPTIONAL"] = 3;
        return values;
      }();
      /**
       * StateType enum.
       * @name rep.proto.ChaincodeDeploy.StateType
       * @enum {number}
       * @property {number} STATE_UNDEFINED=0 STATE_UNDEFINED value
       * @property {number} STATE_BLOCK=1 STATE_BLOCK value
       * @property {number} STATE_GLOBAL=2 STATE_GLOBAL value
       */


      ChaincodeDeploy.StateType = function () {
        var valuesById = {},
            values = Object.create(valuesById);
        values[valuesById[0] = "STATE_UNDEFINED"] = 0;
        values[valuesById[1] = "STATE_BLOCK"] = 1;
        values[valuesById[2] = "STATE_GLOBAL"] = 2;
        return values;
      }();
      /**
       * ContractClassification enum.
       * @name rep.proto.ChaincodeDeploy.ContractClassification
       * @enum {number}
       * @property {number} CONTRACT_UNDEFINED=0 CONTRACT_UNDEFINED value
       * @property {number} CONTRACT_SYSTEM=1 CONTRACT_SYSTEM value
       * @property {number} CONTRACT_CUSTOM=2 CONTRACT_CUSTOM value
       */


      ChaincodeDeploy.ContractClassification = function () {
        var valuesById = {},
            values = Object.create(valuesById);
        values[valuesById[0] = "CONTRACT_UNDEFINED"] = 0;
        values[valuesById[1] = "CONTRACT_SYSTEM"] = 1;
        values[valuesById[2] = "CONTRACT_CUSTOM"] = 2;
        return values;
      }();

      return ChaincodeDeploy;
    }();

    proto.ActionResult = function () {
      /**
       * Properties of an ActionResult.
       * @memberof rep.proto
       * @interface IActionResult
       * @property {number|null} [code] ActionResult code
       * @property {string|null} [reason] ActionResult reason
       */

      /**
       * Constructs a new ActionResult.
       * @memberof rep.proto
       * @classdesc Represents an ActionResult.
       * @implements IActionResult
       * @constructor
       * @param {rep.proto.IActionResult=} [properties] Properties to set
       */
      function ActionResult(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * ActionResult code.
       * @member {number} code
       * @memberof rep.proto.ActionResult
       * @instance
       */


      ActionResult.prototype.code = 0;
      /**
       * ActionResult reason.
       * @member {string} reason
       * @memberof rep.proto.ActionResult
       * @instance
       */

      ActionResult.prototype.reason = "";
      /**
       * Creates a new ActionResult instance using the specified properties.
       * @function create
       * @memberof rep.proto.ActionResult
       * @static
       * @param {rep.proto.IActionResult=} [properties] Properties to set
       * @returns {rep.proto.ActionResult} ActionResult instance
       */

      ActionResult.create = function create(properties) {
        return new ActionResult(properties);
      };
      /**
       * Encodes the specified ActionResult message. Does not implicitly {@link rep.proto.ActionResult.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.ActionResult
       * @static
       * @param {rep.proto.IActionResult} message ActionResult message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      ActionResult.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.code != null && Object.hasOwnProperty.call(message, "code")) writer.uint32(
        /* id 1, wireType 0 =*/
        8).int32(message.code);
        if (message.reason != null && Object.hasOwnProperty.call(message, "reason")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).string(message.reason);
        return writer;
      };
      /**
       * Encodes the specified ActionResult message, length delimited. Does not implicitly {@link rep.proto.ActionResult.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.ActionResult
       * @static
       * @param {rep.proto.IActionResult} message ActionResult message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      ActionResult.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes an ActionResult message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.ActionResult
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.ActionResult} ActionResult
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      ActionResult.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.ActionResult();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.code = reader.int32();
              break;

            case 2:
              message.reason = reader.string();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes an ActionResult message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.ActionResult
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.ActionResult} ActionResult
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      ActionResult.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies an ActionResult message.
       * @function verify
       * @memberof rep.proto.ActionResult
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      ActionResult.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.code != null && message.hasOwnProperty("code")) if (!$util.isInteger(message.code)) return "code: integer expected";
        if (message.reason != null && message.hasOwnProperty("reason")) if (!$util.isString(message.reason)) return "reason: string expected";
        return null;
      };
      /**
       * Creates an ActionResult message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.ActionResult
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.ActionResult} ActionResult
       */


      ActionResult.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.ActionResult) return object;
        var message = new $root.rep.proto.ActionResult();
        if (object.code != null) message.code = object.code | 0;
        if (object.reason != null) message.reason = String(object.reason);
        return message;
      };
      /**
       * Creates a plain object from an ActionResult message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.ActionResult
       * @static
       * @param {rep.proto.ActionResult} message ActionResult
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      ActionResult.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.code = 0;
          object.reason = "";
        }

        if (message.code != null && message.hasOwnProperty("code")) object.code = message.code;
        if (message.reason != null && message.hasOwnProperty("reason")) object.reason = message.reason;
        return object;
      };
      /**
       * Converts this ActionResult to JSON.
       * @function toJSON
       * @memberof rep.proto.ActionResult
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      ActionResult.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return ActionResult;
    }();

    proto.StateProof = function () {
      /**
       * Properties of a StateProof.
       * @memberof rep.proto
       * @interface IStateProof
       * @property {string|null} [key] StateProof key
       * @property {Uint8Array|null} [value] StateProof value
       * @property {Uint8Array|null} [proof] StateProof proof
       */

      /**
       * Constructs a new StateProof.
       * @memberof rep.proto
       * @classdesc Represents a StateProof.
       * @implements IStateProof
       * @constructor
       * @param {rep.proto.IStateProof=} [properties] Properties to set
       */
      function StateProof(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * StateProof key.
       * @member {string} key
       * @memberof rep.proto.StateProof
       * @instance
       */


      StateProof.prototype.key = "";
      /**
       * StateProof value.
       * @member {Uint8Array} value
       * @memberof rep.proto.StateProof
       * @instance
       */

      StateProof.prototype.value = $util.newBuffer([]);
      /**
       * StateProof proof.
       * @member {Uint8Array} proof
       * @memberof rep.proto.StateProof
       * @instance
       */

      StateProof.prototype.proof = $util.newBuffer([]);
      /**
       * Creates a new StateProof instance using the specified properties.
       * @function create
       * @memberof rep.proto.StateProof
       * @static
       * @param {rep.proto.IStateProof=} [properties] Properties to set
       * @returns {rep.proto.StateProof} StateProof instance
       */

      StateProof.create = function create(properties) {
        return new StateProof(properties);
      };
      /**
       * Encodes the specified StateProof message. Does not implicitly {@link rep.proto.StateProof.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.StateProof
       * @static
       * @param {rep.proto.IStateProof} message StateProof message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      StateProof.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.key != null && Object.hasOwnProperty.call(message, "key")) writer.uint32(
        /* id 1, wireType 2 =*/
        10).string(message.key);
        if (message.value != null && Object.hasOwnProperty.call(message, "value")) writer.uint32(
        /* id 2, wireType 2 =*/
        18).bytes(message.value);
        if (message.proof != null && Object.hasOwnProperty.call(message, "proof")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).bytes(message.proof);
        return writer;
      };
      /**
       * Encodes the specified StateProof message, length delimited. Does not implicitly {@link rep.proto.StateProof.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.StateProof
       * @static
       * @param {rep.proto.IStateProof} message StateProof message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      StateProof.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a StateProof message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.StateProof
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.StateProof} StateProof
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      StateProof.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.StateProof();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.key = reader.string();
              break;

            case 2:
              message.value = reader.bytes();
              break;

            case 3:
              message.proof = reader.bytes();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a StateProof message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.StateProof
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.StateProof} StateProof
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      StateProof.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a StateProof message.
       * @function verify
       * @memberof rep.proto.StateProof
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      StateProof.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.key != null && message.hasOwnProperty("key")) if (!$util.isString(message.key)) return "key: string expected";
        if (message.value != null && message.hasOwnProperty("value")) if (!(message.value && typeof message.value.length === "number" || $util.isString(message.value))) return "value: buffer expected";
        if (message.proof != null && message.hasOwnProperty("proof")) if (!(message.proof && typeof message.proof.length === "number" || $util.isString(message.proof))) return "proof: buffer expected";
        return null;
      };
      /**
       * Creates a StateProof message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.StateProof
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.StateProof} StateProof
       */


      StateProof.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.StateProof) return object;
        var message = new $root.rep.proto.StateProof();
        if (object.key != null) message.key = String(object.key);
        if (object.value != null) if (typeof object.value === "string") $util.base64.decode(object.value, message.value = $util.newBuffer($util.base64.length(object.value)), 0);else if (object.value.length) message.value = object.value;
        if (object.proof != null) if (typeof object.proof === "string") $util.base64.decode(object.proof, message.proof = $util.newBuffer($util.base64.length(object.proof)), 0);else if (object.proof.length) message.proof = object.proof;
        return message;
      };
      /**
       * Creates a plain object from a StateProof message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.StateProof
       * @static
       * @param {rep.proto.StateProof} message StateProof
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      StateProof.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          object.key = "";
          if (options.bytes === String) object.value = "";else {
            object.value = [];
            if (options.bytes !== Array) object.value = $util.newBuffer(object.value);
          }
          if (options.bytes === String) object.proof = "";else {
            object.proof = [];
            if (options.bytes !== Array) object.proof = $util.newBuffer(object.proof);
          }
        }

        if (message.key != null && message.hasOwnProperty("key")) object.key = message.key;
        if (message.value != null && message.hasOwnProperty("value")) object.value = options.bytes === String ? $util.base64.encode(message.value, 0, message.value.length) : options.bytes === Array ? Array.prototype.slice.call(message.value) : message.value;
        if (message.proof != null && message.hasOwnProperty("proof")) object.proof = options.bytes === String ? $util.base64.encode(message.proof, 0, message.proof.length) : options.bytes === Array ? Array.prototype.slice.call(message.proof) : message.proof;
        return object;
      };
      /**
       * Converts this StateProof to JSON.
       * @function toJSON
       * @memberof rep.proto.StateProof
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      StateProof.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return StateProof;
    }();

    proto.BlockchainInfo = function () {
      /**
       * Properties of a BlockchainInfo.
       * @memberof rep.proto
       * @interface IBlockchainInfo
       * @property {number|Long|null} [height] BlockchainInfo height
       * @property {number|Long|null} [totalTransactions] BlockchainInfo totalTransactions
       * @property {Uint8Array|null} [currentBlockHash] BlockchainInfo currentBlockHash
       * @property {Uint8Array|null} [previousBlockHash] BlockchainInfo previousBlockHash
       * @property {Uint8Array|null} [currentStateHash] BlockchainInfo currentStateHash
       */

      /**
       * Constructs a new BlockchainInfo.
       * @memberof rep.proto
       * @classdesc Represents a BlockchainInfo.
       * @implements IBlockchainInfo
       * @constructor
       * @param {rep.proto.IBlockchainInfo=} [properties] Properties to set
       */
      function BlockchainInfo(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * BlockchainInfo height.
       * @member {number|Long} height
       * @memberof rep.proto.BlockchainInfo
       * @instance
       */


      BlockchainInfo.prototype.height = $util.Long ? $util.Long.fromBits(0, 0, true) : 0;
      /**
       * BlockchainInfo totalTransactions.
       * @member {number|Long} totalTransactions
       * @memberof rep.proto.BlockchainInfo
       * @instance
       */

      BlockchainInfo.prototype.totalTransactions = $util.Long ? $util.Long.fromBits(0, 0, true) : 0;
      /**
       * BlockchainInfo currentBlockHash.
       * @member {Uint8Array} currentBlockHash
       * @memberof rep.proto.BlockchainInfo
       * @instance
       */

      BlockchainInfo.prototype.currentBlockHash = $util.newBuffer([]);
      /**
       * BlockchainInfo previousBlockHash.
       * @member {Uint8Array} previousBlockHash
       * @memberof rep.proto.BlockchainInfo
       * @instance
       */

      BlockchainInfo.prototype.previousBlockHash = $util.newBuffer([]);
      /**
       * BlockchainInfo currentStateHash.
       * @member {Uint8Array} currentStateHash
       * @memberof rep.proto.BlockchainInfo
       * @instance
       */

      BlockchainInfo.prototype.currentStateHash = $util.newBuffer([]);
      /**
       * Creates a new BlockchainInfo instance using the specified properties.
       * @function create
       * @memberof rep.proto.BlockchainInfo
       * @static
       * @param {rep.proto.IBlockchainInfo=} [properties] Properties to set
       * @returns {rep.proto.BlockchainInfo} BlockchainInfo instance
       */

      BlockchainInfo.create = function create(properties) {
        return new BlockchainInfo(properties);
      };
      /**
       * Encodes the specified BlockchainInfo message. Does not implicitly {@link rep.proto.BlockchainInfo.verify|verify} messages.
       * @function encode
       * @memberof rep.proto.BlockchainInfo
       * @static
       * @param {rep.proto.IBlockchainInfo} message BlockchainInfo message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      BlockchainInfo.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.height != null && Object.hasOwnProperty.call(message, "height")) writer.uint32(
        /* id 1, wireType 0 =*/
        8).uint64(message.height);
        if (message.totalTransactions != null && Object.hasOwnProperty.call(message, "totalTransactions")) writer.uint32(
        /* id 2, wireType 0 =*/
        16).uint64(message.totalTransactions);
        if (message.currentBlockHash != null && Object.hasOwnProperty.call(message, "currentBlockHash")) writer.uint32(
        /* id 3, wireType 2 =*/
        26).bytes(message.currentBlockHash);
        if (message.previousBlockHash != null && Object.hasOwnProperty.call(message, "previousBlockHash")) writer.uint32(
        /* id 4, wireType 2 =*/
        34).bytes(message.previousBlockHash);
        if (message.currentStateHash != null && Object.hasOwnProperty.call(message, "currentStateHash")) writer.uint32(
        /* id 5, wireType 2 =*/
        42).bytes(message.currentStateHash);
        return writer;
      };
      /**
       * Encodes the specified BlockchainInfo message, length delimited. Does not implicitly {@link rep.proto.BlockchainInfo.verify|verify} messages.
       * @function encodeDelimited
       * @memberof rep.proto.BlockchainInfo
       * @static
       * @param {rep.proto.IBlockchainInfo} message BlockchainInfo message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      BlockchainInfo.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a BlockchainInfo message from the specified reader or buffer.
       * @function decode
       * @memberof rep.proto.BlockchainInfo
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {rep.proto.BlockchainInfo} BlockchainInfo
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      BlockchainInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.rep.proto.BlockchainInfo();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.height = reader.uint64();
              break;

            case 2:
              message.totalTransactions = reader.uint64();
              break;

            case 3:
              message.currentBlockHash = reader.bytes();
              break;

            case 4:
              message.previousBlockHash = reader.bytes();
              break;

            case 5:
              message.currentStateHash = reader.bytes();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a BlockchainInfo message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof rep.proto.BlockchainInfo
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {rep.proto.BlockchainInfo} BlockchainInfo
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      BlockchainInfo.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a BlockchainInfo message.
       * @function verify
       * @memberof rep.proto.BlockchainInfo
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      BlockchainInfo.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.height != null && message.hasOwnProperty("height")) if (!$util.isInteger(message.height) && !(message.height && $util.isInteger(message.height.low) && $util.isInteger(message.height.high))) return "height: integer|Long expected";
        if (message.totalTransactions != null && message.hasOwnProperty("totalTransactions")) if (!$util.isInteger(message.totalTransactions) && !(message.totalTransactions && $util.isInteger(message.totalTransactions.low) && $util.isInteger(message.totalTransactions.high))) return "totalTransactions: integer|Long expected";
        if (message.currentBlockHash != null && message.hasOwnProperty("currentBlockHash")) if (!(message.currentBlockHash && typeof message.currentBlockHash.length === "number" || $util.isString(message.currentBlockHash))) return "currentBlockHash: buffer expected";
        if (message.previousBlockHash != null && message.hasOwnProperty("previousBlockHash")) if (!(message.previousBlockHash && typeof message.previousBlockHash.length === "number" || $util.isString(message.previousBlockHash))) return "previousBlockHash: buffer expected";
        if (message.currentStateHash != null && message.hasOwnProperty("currentStateHash")) if (!(message.currentStateHash && typeof message.currentStateHash.length === "number" || $util.isString(message.currentStateHash))) return "currentStateHash: buffer expected";
        return null;
      };
      /**
       * Creates a BlockchainInfo message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof rep.proto.BlockchainInfo
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {rep.proto.BlockchainInfo} BlockchainInfo
       */


      BlockchainInfo.fromObject = function fromObject(object) {
        if (object instanceof $root.rep.proto.BlockchainInfo) return object;
        var message = new $root.rep.proto.BlockchainInfo();
        if (object.height != null) if ($util.Long) (message.height = $util.Long.fromValue(object.height)).unsigned = true;else if (typeof object.height === "string") message.height = parseInt(object.height, 10);else if (typeof object.height === "number") message.height = object.height;else if ((0, _typeof2["default"])(object.height) === "object") message.height = new $util.LongBits(object.height.low >>> 0, object.height.high >>> 0).toNumber(true);
        if (object.totalTransactions != null) if ($util.Long) (message.totalTransactions = $util.Long.fromValue(object.totalTransactions)).unsigned = true;else if (typeof object.totalTransactions === "string") message.totalTransactions = parseInt(object.totalTransactions, 10);else if (typeof object.totalTransactions === "number") message.totalTransactions = object.totalTransactions;else if ((0, _typeof2["default"])(object.totalTransactions) === "object") message.totalTransactions = new $util.LongBits(object.totalTransactions.low >>> 0, object.totalTransactions.high >>> 0).toNumber(true);
        if (object.currentBlockHash != null) if (typeof object.currentBlockHash === "string") $util.base64.decode(object.currentBlockHash, message.currentBlockHash = $util.newBuffer($util.base64.length(object.currentBlockHash)), 0);else if (object.currentBlockHash.length) message.currentBlockHash = object.currentBlockHash;
        if (object.previousBlockHash != null) if (typeof object.previousBlockHash === "string") $util.base64.decode(object.previousBlockHash, message.previousBlockHash = $util.newBuffer($util.base64.length(object.previousBlockHash)), 0);else if (object.previousBlockHash.length) message.previousBlockHash = object.previousBlockHash;
        if (object.currentStateHash != null) if (typeof object.currentStateHash === "string") $util.base64.decode(object.currentStateHash, message.currentStateHash = $util.newBuffer($util.base64.length(object.currentStateHash)), 0);else if (object.currentStateHash.length) message.currentStateHash = object.currentStateHash;
        return message;
      };
      /**
       * Creates a plain object from a BlockchainInfo message. Also converts values to other types if specified.
       * @function toObject
       * @memberof rep.proto.BlockchainInfo
       * @static
       * @param {rep.proto.BlockchainInfo} message BlockchainInfo
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      BlockchainInfo.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          if ($util.Long) {
            var _long3 = new $util.Long(0, 0, true);

            object.height = options.longs === String ? _long3.toString() : options.longs === Number ? _long3.toNumber() : _long3;
          } else object.height = options.longs === String ? "0" : 0;

          if ($util.Long) {
            var _long4 = new $util.Long(0, 0, true);

            object.totalTransactions = options.longs === String ? _long4.toString() : options.longs === Number ? _long4.toNumber() : _long4;
          } else object.totalTransactions = options.longs === String ? "0" : 0;

          if (options.bytes === String) object.currentBlockHash = "";else {
            object.currentBlockHash = [];
            if (options.bytes !== Array) object.currentBlockHash = $util.newBuffer(object.currentBlockHash);
          }
          if (options.bytes === String) object.previousBlockHash = "";else {
            object.previousBlockHash = [];
            if (options.bytes !== Array) object.previousBlockHash = $util.newBuffer(object.previousBlockHash);
          }
          if (options.bytes === String) object.currentStateHash = "";else {
            object.currentStateHash = [];
            if (options.bytes !== Array) object.currentStateHash = $util.newBuffer(object.currentStateHash);
          }
        }

        if (message.height != null && message.hasOwnProperty("height")) if (typeof message.height === "number") object.height = options.longs === String ? String(message.height) : message.height;else object.height = options.longs === String ? $util.Long.prototype.toString.call(message.height) : options.longs === Number ? new $util.LongBits(message.height.low >>> 0, message.height.high >>> 0).toNumber(true) : message.height;
        if (message.totalTransactions != null && message.hasOwnProperty("totalTransactions")) if (typeof message.totalTransactions === "number") object.totalTransactions = options.longs === String ? String(message.totalTransactions) : message.totalTransactions;else object.totalTransactions = options.longs === String ? $util.Long.prototype.toString.call(message.totalTransactions) : options.longs === Number ? new $util.LongBits(message.totalTransactions.low >>> 0, message.totalTransactions.high >>> 0).toNumber(true) : message.totalTransactions;
        if (message.currentBlockHash != null && message.hasOwnProperty("currentBlockHash")) object.currentBlockHash = options.bytes === String ? $util.base64.encode(message.currentBlockHash, 0, message.currentBlockHash.length) : options.bytes === Array ? Array.prototype.slice.call(message.currentBlockHash) : message.currentBlockHash;
        if (message.previousBlockHash != null && message.hasOwnProperty("previousBlockHash")) object.previousBlockHash = options.bytes === String ? $util.base64.encode(message.previousBlockHash, 0, message.previousBlockHash.length) : options.bytes === Array ? Array.prototype.slice.call(message.previousBlockHash) : message.previousBlockHash;
        if (message.currentStateHash != null && message.hasOwnProperty("currentStateHash")) object.currentStateHash = options.bytes === String ? $util.base64.encode(message.currentStateHash, 0, message.currentStateHash.length) : options.bytes === Array ? Array.prototype.slice.call(message.currentStateHash) : message.currentStateHash;
        return object;
      };
      /**
       * Converts this BlockchainInfo to JSON.
       * @function toJSON
       * @memberof rep.proto.BlockchainInfo
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      BlockchainInfo.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return BlockchainInfo;
    }();

    return proto;
  }();

  return rep;
}();

exports.rep = rep;

var google = $root.google = function () {
  /**
   * Namespace google.
   * @exports google
   * @namespace
   */
  var google = {};

  google.protobuf = function () {
    /**
     * Namespace protobuf.
     * @memberof google
     * @namespace
     */
    var protobuf = {};

    protobuf.Timestamp = function () {
      /**
       * Properties of a Timestamp.
       * @memberof google.protobuf
       * @interface ITimestamp
       * @property {number|Long|null} [seconds] Timestamp seconds
       * @property {number|null} [nanos] Timestamp nanos
       */

      /**
       * Constructs a new Timestamp.
       * @memberof google.protobuf
       * @classdesc Represents a Timestamp.
       * @implements ITimestamp
       * @constructor
       * @param {google.protobuf.ITimestamp=} [properties] Properties to set
       */
      function Timestamp(properties) {
        if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }
      }
      /**
       * Timestamp seconds.
       * @member {number|Long} seconds
       * @memberof google.protobuf.Timestamp
       * @instance
       */


      Timestamp.prototype.seconds = $util.Long ? $util.Long.fromBits(0, 0, false) : 0;
      /**
       * Timestamp nanos.
       * @member {number} nanos
       * @memberof google.protobuf.Timestamp
       * @instance
       */

      Timestamp.prototype.nanos = 0;
      /**
       * Creates a new Timestamp instance using the specified properties.
       * @function create
       * @memberof google.protobuf.Timestamp
       * @static
       * @param {google.protobuf.ITimestamp=} [properties] Properties to set
       * @returns {google.protobuf.Timestamp} Timestamp instance
       */

      Timestamp.create = function create(properties) {
        return new Timestamp(properties);
      };
      /**
       * Encodes the specified Timestamp message. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
       * @function encode
       * @memberof google.protobuf.Timestamp
       * @static
       * @param {google.protobuf.ITimestamp} message Timestamp message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Timestamp.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.seconds != null && Object.hasOwnProperty.call(message, "seconds")) writer.uint32(
        /* id 1, wireType 0 =*/
        8).int64(message.seconds);
        if (message.nanos != null && Object.hasOwnProperty.call(message, "nanos")) writer.uint32(
        /* id 2, wireType 0 =*/
        16).int32(message.nanos);
        return writer;
      };
      /**
       * Encodes the specified Timestamp message, length delimited. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
       * @function encodeDelimited
       * @memberof google.protobuf.Timestamp
       * @static
       * @param {google.protobuf.ITimestamp} message Timestamp message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */


      Timestamp.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };
      /**
       * Decodes a Timestamp message from the specified reader or buffer.
       * @function decode
       * @memberof google.protobuf.Timestamp
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {google.protobuf.Timestamp} Timestamp
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Timestamp.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.google.protobuf.Timestamp();

        while (reader.pos < end) {
          var tag = reader.uint32();

          switch (tag >>> 3) {
            case 1:
              message.seconds = reader.int64();
              break;

            case 2:
              message.nanos = reader.int32();
              break;

            default:
              reader.skipType(tag & 7);
              break;
          }
        }

        return message;
      };
      /**
       * Decodes a Timestamp message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof google.protobuf.Timestamp
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {google.protobuf.Timestamp} Timestamp
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */


      Timestamp.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };
      /**
       * Verifies a Timestamp message.
       * @function verify
       * @memberof google.protobuf.Timestamp
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */


      Timestamp.verify = function verify(message) {
        if ((0, _typeof2["default"])(message) !== "object" || message === null) return "object expected";
        if (message.seconds != null && message.hasOwnProperty("seconds")) if (!$util.isInteger(message.seconds) && !(message.seconds && $util.isInteger(message.seconds.low) && $util.isInteger(message.seconds.high))) return "seconds: integer|Long expected";
        if (message.nanos != null && message.hasOwnProperty("nanos")) if (!$util.isInteger(message.nanos)) return "nanos: integer expected";
        return null;
      };
      /**
       * Creates a Timestamp message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof google.protobuf.Timestamp
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {google.protobuf.Timestamp} Timestamp
       */


      Timestamp.fromObject = function fromObject(object) {
        if (object instanceof $root.google.protobuf.Timestamp) return object;
        var message = new $root.google.protobuf.Timestamp();
        if (object.seconds != null) if ($util.Long) (message.seconds = $util.Long.fromValue(object.seconds)).unsigned = false;else if (typeof object.seconds === "string") message.seconds = parseInt(object.seconds, 10);else if (typeof object.seconds === "number") message.seconds = object.seconds;else if ((0, _typeof2["default"])(object.seconds) === "object") message.seconds = new $util.LongBits(object.seconds.low >>> 0, object.seconds.high >>> 0).toNumber();
        if (object.nanos != null) message.nanos = object.nanos | 0;
        return message;
      };
      /**
       * Creates a plain object from a Timestamp message. Also converts values to other types if specified.
       * @function toObject
       * @memberof google.protobuf.Timestamp
       * @static
       * @param {google.protobuf.Timestamp} message Timestamp
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */


      Timestamp.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};

        if (options.defaults) {
          if ($util.Long) {
            var _long5 = new $util.Long(0, 0, false);

            object.seconds = options.longs === String ? _long5.toString() : options.longs === Number ? _long5.toNumber() : _long5;
          } else object.seconds = options.longs === String ? "0" : 0;

          object.nanos = 0;
        }

        if (message.seconds != null && message.hasOwnProperty("seconds")) if (typeof message.seconds === "number") object.seconds = options.longs === String ? String(message.seconds) : message.seconds;else object.seconds = options.longs === String ? $util.Long.prototype.toString.call(message.seconds) : options.longs === Number ? new $util.LongBits(message.seconds.low >>> 0, message.seconds.high >>> 0).toNumber() : message.seconds;
        if (message.nanos != null && message.hasOwnProperty("nanos")) object.nanos = message.nanos;
        return object;
      };
      /**
       * Converts this Timestamp to JSON.
       * @function toJSON
       * @memberof google.protobuf.Timestamp
       * @instance
       * @returns {Object.<string,*>} JSON object
       */


      Timestamp.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Timestamp;
    }();

    return protobuf;
  }();

  return google;
}();

exports.google = google;
//# sourceMappingURL=rc2.js.map