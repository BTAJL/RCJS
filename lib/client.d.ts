/// <reference types="node" />
/** 区块链的概要信息 */
export interface ChainInfo {
    /** 最新区块高度 */
    height: number;
    /** 最新区块哈希值(base64编码字符串) */
    currentBlockHash: string;
    /** 最新区块的父区块哈希值(base64编码字符串) */
    previousBlockHash: string;
    /** 世界状态哈希值(base64编码字符串) */
    currentStateHash: string;
    /** 交易总量 */
    totalTransactions: number;
}
export declare enum RESPONSE_FORMAT {
    JSON = 0,
    STREAM = 1
}
export declare class Client {
    private _address;
    /**
     * Creates an instance of RestAPI.
     * <br />
     * 与RepChain区块链节点交互的客户端.
     *
     * @param address RepChain节点的Restful API服务地址
     * @returns Client对象实例
     */
    constructor(address: string);
    _sanitizeUrl(address: string): string;
    /**
     * 获取客户端欲连接的节点Url
     * @returns 客户端欲连接的节点Url
     */
    getAddress(): string;
    /**
     * 修改客户端欲连接的节点Url
     * @param address 欲连接的节点Url
     */
    setAddress(address: string): void;
    /**
     * 获取区块链的当前概要信息
     *
     * @returns Json格式信息
     */
    chainInfo(): Promise<ChainInfo>;
    /**
     * 获取区块链最新区块高度
     *
     * @returns 区块高度
     */
    chainHeight(): Promise<number>;
    /**
     * 获取最新区块哈希值
     *
     * @returns 区块哈希值(base64编码字符串)
     */
    chainCurrentBlockHash(): Promise<string>;
    /**
     * 获取最新区块的父区块哈希值
     *
     * @returns 最新区块的父区块哈希值(base64编码字符串)
     */
    chainPreviousBlockHash(): Promise<string>;
    /**
     * 获取当前的世界状态哈希值
     *
     * @returns 世界状态哈希值(base64编码字符串)
     */
    chainCurrentStateHash(): Promise<string>;
    /**
     * 获取区块链中的交易总数量
     *
     * @returns 交易总量
     * @memberof RestAPI
     */
    chainTotalTransactions(): Promise<number>;
    /**
     * 获取区块数据
     *
     * @param id 区块唯一标识，可为区块高度(number)或区块哈希值(base64编码字符串或二进制数据)
     * @param [blockFormat=RESPONSE_FORMAT.JSON] 期望返回的区块数据的格式，可为RESPONSE_FORMAT.JSON(json对象，默认)或RESPONSE_FORMAT.STREAM(二进制数据)
     * @returns 区块数据
     */
    block(id: number | string | Buffer, blockFormat?: RESPONSE_FORMAT): Promise<{
        [key: string]: any;
    }> | Promise<Buffer>;
    /**
     * 获取交易数据
     *
     * @param id 交易唯一标识，即txid
     * @param [txFormat=REPONSE_FORMAT.JSON] 期望返回的交易数据的格式JSON或STREAM
     * @param [withBlockHeight=false] 返回交易数据中是否包含区块高度，当txFormat=时才有效
     * @returns 交易数据
     */
    transaction(id: string, txFormat?: RESPONSE_FORMAT, withBlockHeight?: boolean): Promise<{
        [key: string]: any;
    }> | Promise<Buffer>;
    /**
     * 发送签名交易
     *
     * @param tx 待发送的已签名交易数据，支持使用Buffer类型数据或其hex编码字符串数据
     * @returns 接收交易后RepChain节点的返回信息
     */
    sendTransaction(tx: Buffer | string): Promise<{
        txid: string;
    }> | Promise<{
        err: string;
    }>;
}
export declare type RestAPI = Client;
