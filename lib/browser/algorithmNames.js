"use strict";
exports.__esModule = true;
exports.SHA256withECDSAForJsrsasign = void 0;
// To use for nodejs crypto
var SHA256withECDSA = "sha256";
exports["default"] = SHA256withECDSA;
// To use for jsrsasign 
exports.SHA256withECDSAForJsrsasign = "SHA256withECDSA";
