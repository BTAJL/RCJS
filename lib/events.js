"use strict";
exports.__esModule = true;
exports.EventTube = void 0;
// 使用w3cwebsocket对象，兼容Browser和Node环境
var websocket_1 = require("websocket");
var defaultLogger = {
    info: function (info) { return console.log(info); },
    error: function (error) { return console.error(error); }
};
var EventTube = /** @class */ (function () {
    /**
     * 构建事件订阅对象实例
     *
     * @param address websocket服务地址
     * @param cb 处理返回信息的回调函数
     * @param timeout 重连的时间间隔
     */
    function EventTube(address, cb, timeout, logger) {
        this.address = address;
        this.cb = cb;
        this.timeout = timeout || 5000;
        this.timer = null;
        this.ws = null;
        this.shouldReconnect = false;
        this.logger = logger || defaultLogger;
        this.connect();
    }
    /**
     * 获取Websocket连接的目标节点Url
     * @returns 目标节点Url
     */
    EventTube.prototype.getAddress = function () {
        return this.address;
    };
    EventTube.prototype.setCallback = function (cb) {
        this.cb = cb;
    };
    EventTube.prototype.reconnect = function () {
        var _this = this;
        var me = this;
        this.logger.info("To rebuild the websocket connection(with peer: ".concat(me.address, ")"));
        var timeout = me.timeout;
        if (!me.timer) {
            me.timer = setTimeout(function () {
                if (me.shouldReconnect) {
                    me.connect();
                }
                else {
                    _this.logger.info("Intentionally to revoke the attempting to rebuild the websocket connection(with peer: ".concat(me.address, ")"));
                }
            }, timeout);
        }
    };
    EventTube.prototype.connect = function () {
        var _this = this;
        var me = this;
        me.timer = null;
        me.shouldReconnect = true;
        this.logger.info("Connecting peer with websocket url: ".concat(me.address));
        var ws = new websocket_1.w3cwebsocket(me.address);
        me.ws = ws;
        ws.onerror = function (e) {
            _this.logger.error("Websocket connection(with peer: ".concat(me.address, ") error: ").concat(e.message));
            me.reconnect();
        };
        ws.onmessage = function (m) {
            if (me.cb) {
                me.cb(m);
            }
        };
        ws.onopen = function () {
            _this.logger.info("Built websocket connection(with peer: ".concat(me.address, ")"));
        };
        ws.onclose = function (e) {
            if (e.code !== 4000) {
                _this.logger.error("Webscoket connection(with peer: ".concat(me.address, ") closed with the code: ").concat(e.code, " for the reason: ").concat(e.reason));
                me.reconnect();
            }
            else {
                _this.logger.info("Disconnected websocket connection(with peer: ".concat(me.address, ") intentionally, with the code ").concat(e.code, " for the reason: ").concat(e.reason));
            }
        };
    };
    /**
     * 主动关闭websocket连接
     *
     * @param reason 解释主动关闭连接的原因，不超过123字节
     * @param code 主动关闭连接的状态码
     */
    EventTube.prototype.close = function (reason, code) {
        if (code === void 0) { code = 4000; }
        if (!this.ws) {
            throw new Error("No websocket connection yet");
        }
        if (code === 4000) {
            // Not to reconnect when meet error 
            this.shouldReconnect = false;
        }
        this.ws.close(code, reason);
    };
    return EventTube;
}());
exports.EventTube = EventTube;
