export type gmGetHashCallback = (sm3HashVal: string) => any;
export type gmSignCallback = (signature: string) => any;
export type gmCertificateCallback = (certPEM: string) => any;
/**
 * @callback gmGetHashCallback
 * @param {string} sm3HashVal hex格式的国密算法哈希值
 */
/**
 * 根据指定的密码学哈希算法为给定数据计算哈希值
 *
 * @param {Object} getHashParams 计算哈希值所需参数
 * @param {Buffer | string} getHashParams.data 待对其计算哈希值的原数据
 * @param {string} [getHashParams.alg] 待使用的密码学哈希算法，默认为sha256
 * @param {string} [getHashParams.provider] 密码学哈希算法的提供者，支持nodecrypto、jsrsasign以及gm
 * <li>nodecrypto，NodeJS内建的crypto工具，默认使用该provider，支持openssl提供的hash算法，
 *   可在终端使用命令`openssl list-message-digest-algorithms`(1.0.2版)
 *   或`openssl list -digest-algorithms`(1.1.0版)查看支持的哈希算法
 * </li>
 * <li>jsrsasign，开源js加密工具(https://kjur.github.io/jsrsasign),
 *   支持的哈希算法如<a href=https://kjur.github.io/jsrsasign/api/symbols/KJUR.crypto.MessageDigest.html>
 * https://kjur.github.io/jsrsasign/api/symbols/KJUR.crypto.MessageDigest.html<a/>所示
 * </li>
 * <li>gm，国密算法加密工具，支持sm3哈希算法
 * </li>
 * @param {gmGetHashCallback} [getHashParams.cb] sm3计算服务为异步实现，当使用sm3时需提供回调方法
 * @returns {Buffer} digest 结果哈希值，若使用sm3将返回undefined
 */
export function GetHashVal({ data, alg, provider, cb, }: {
    data: Buffer | string;
    alg?: string | undefined;
    provider?: string | undefined;
    cb?: gmGetHashCallback | undefined;
}): Buffer;
/**
 * 创建非对称密钥对，支持RSA与EC密钥对生成
 *
 * @param {string} [alg] 密钥对生成算法，RSA或EC，默认使用EC
 * @param {string | number} [keyLenOrCurve] 指定密钥长度（针对RSA）或曲线名（针对EC），
 * 默认使用EC secp256k1曲线
 * @returns {Object} keypair 含有jsrsasign提供的prvKeyObj与pubKeyObj对象
 */
export function CreateKeypair(alg?: string | undefined, keyLenOrCurve?: string | number | undefined): Object;
/**
 * 导入已有非对称密钥
 *
 * @param {string} keyPEM 使用符合PKCS#8标准的pem格式私钥信息获取私钥对象，支持导入jsrsasign对PKCS#8标准私钥使用加密算法加密后的pem格式私钥信息
 * 使用符合PKCS#8标准的pem格式公钥信息或符合X.509标准的pem格式证书信息获取公钥对象
 * @param {string} [passWord] 私钥保护密码, 当导入已加密私钥时，需要提供该参数
 * @returns {Object} keyObj密钥对象，prvkeyObj或pubKeyObj
 */
export function ImportKey(keyPEM: string, passWord?: string | undefined): Object;
/**
 * 获得PEM格式的私钥或公钥信息
 *
 * @param {Object} keyObj prvKeyObj或pubKeyObj
 * @param {string} [passWord] 私钥保护密码，当需要生成加密私钥信息时需提供该参数,
 * 目前是由jsrsasign使用对私钥进行加密（reference: https://kjur.github.io/jsrsasign/api/symbols/KEYUTIL.html#.getPEM）
 * @returns {string} keyPEM 符合PKCS#8标准的密钥信息
 */
export function GetKeyPEM(keyObj: Object, passWord?: string | undefined): string;
/**
 *
 * @callback gmSignCallback
 * @param {string} signature hex格式签名信息
 */
/**
 * 根据指定私钥和签名算法，对给定数据进行签名
 *
 * @param {Object} signParams 签名所需参数
 * @param {Object | string} signParams.prvKey 私钥信息，支持使用jsrsasign提供的私钥对象，
 * 或直接使用符合PKCS#5的未加密pem格式DSA/RSA私钥，符合PKCS#8的未加密pem格式RSA/ECDSA私钥，当使用gm签名算法时该参数应为null
 * @param {string | Buffer} signParams.data 待被签名的数据
 * @param {string} [signParams.alg] 签名算法，默认使用"sha256"，国密签名算法为sm2-with-SM3
 * @param {string} [signParams.provider] 签名算法的提供者，支持使用"jsrsasign"或"nodecrypto"(node内建的crypto，默认使用)，以及"gm"国密签名算法工具
 * @param {string} [signParams.codeType=hex] 当签名算法提供者provider为"jsrsasign"时，可指定jsrsasign签名时使用updateHex或updateString方法，默认使用"hex"调用updateHex方法以防止签名结果被其他库验签时失败
 * @param {string} [signParams.gmUserID] 国密签名算法需要的用户标识，该标识是到gm websocket server查找到其对应国密私钥的唯一标识
 * @param {gmSignCallback} [signParams.cb] 国密签名算法支持为异步实现，当使用国密签名算法时，需要使用该回调方法
 * @returns {Buffer} signature 签名结果值，当使用非国密签名时，会返回该结果
 */
export function Sign({ prvKey, data, alg, provider, gmUserID, cb, codeType, }: {
    prvKey: Object | string;
    data: string | Buffer;
    alg?: string | undefined;
    provider?: string | undefined;
    codeType?: string | undefined;
    gmUserID?: string | undefined;
    cb?: gmSignCallback | undefined;
}): Buffer;
/**
 * 验证签名
 *
 * @param {Object} verifySignatureParams 验证签名所需参数
 * @param {Object | string} verifySignatureParams.pubKey 公钥信息，支持使用jsrsasign提供的公钥对象，
 * 或直接使用符合PKCS#8的pem格式DSA/RSA/ECDSA公钥，符合X.509的PEM格式包含公钥信息的证书
 * @param {Buffer} verifySignatureParams.sigValue 签名结果
 * @param {string | Buffer} verifySignatureParams.data 被签名的原数据
 * @param {string} [verifySignatureParams.alg] 签名算法，默认使用"sha256"(即"SHA256withECDSA")
 * @param {string} [verifySignatureParams.provider] 签名算法的提供者，
 * 支持使用"jsrsasign"或"nodecrypto"(node内建的crypto,默认使用)，待支持使用国密算法提供者
 * @returns {boolean} isValid 签名真实性鉴定结果
 */
export function VerifySign({ pubKey, sigValue, data, alg, provider, }: {
    pubKey: Object | string;
    sigValue: Buffer;
    data: string | Buffer;
    alg?: string | undefined;
    provider?: string | undefined;
}): boolean;
/**
 * 根据公钥信息计算其bitcoin地址
 *
 * @param {string} pubKeyPEM 符合PKCS#8标准的pem格式公钥信息，或符合X.509标准的公钥证书信息，目前只支持EC-secp256k1曲线
 * @returns {Buffer} bitcoin地址的Buffer数据
 */
export function CalculateAddr(pubKeyPEM: string): Buffer;
/**
 * 生成PEM格式的证书签名请求(Certificate Signing Request, CSR)信息
 *
 * @param {Object} params 生成csr所需参数
 * @param {string} params.subject 证书拥有者唯一标识名
 * @param {string} params.subjectPubKey 证书拥有者PEM格式的公钥信息
 * @param {string} params.signAlg 生成csr的签名算法
 * @param {string} params.subjectPrvKey 证书拥有者PEM格式的私钥信息(无加密)
 * @returns {string} PEM格式的csr信息
 */
export function CreateCSR({ subject, subjectPubKey, signAlg, subjectPrvKey, }: {
    subject: string;
    subjectPubKey: string;
    signAlg: string;
    subjectPrvKey: string;
}): string;
/**
 * 从PEM格式的csr中获取csr信息
 *
 * @param {string} csrPEM PEM格式的csr信息
 * @returns {Object} csrInfo 解析后的csr信息
 * csrInfo.subject - subject name parameters
 * csrInfo.sbjpubkey - pem subject pubkey
 * csrInfo.extreq - extensionRequest parameters
 * csrInfo.sigalg - signature algorithm
 * csrInfo.sighex - hex string format signature
 */
export function GetCSRInfoFromPEM(csrPEM: string): Object;
/**
 * @callback gmCertificateCallback
 * @param {string} certPEM pem格式证书
 */
/**
 * 生成符合X.509标准的证书信息
 *
 * @param {Object} certFields 证书的具体信息，
 * @param {number} certFields.serialNumber 证书序列号
 * @param {string} certFields.sigAlg 签发证书时使用的签名算法
 * @param {string} certFields.issuerDN 符合X500标准的代表证书发行方身份标识的Distinguished Name
 * @param {string} certFields.subjectDN 符合X500标准的代表证书拥有方标识的Distinguished Name
 * @param {number} certFields.notBefore 代表证书有效性起始时间的unix时间戳（秒）
 * @param {number} certFields.notAfter 代表证书有效性终止时间的unix时间戳（秒）
 * @param {Object} certFields.subjectPubKey 证书拥有方的公钥对象，使用jsrsasign提供的pubKeyObj
 * @param {Object} certFields.issuerPrvKey 证书发行方的私钥对象，使用jsrsasign提供的prvKeyObj
 * @param {Object} [certFields.extensions] 证书扩展，jsrsasign支持的扩展属性(https://kjur.github.io/jsrsasign/api/symbols/KJUR.asn1.x509.TBSCertificate.html#appendExtension)，
 * 如：
 * {
 *     BasicConstraints: { cA: true, critical: true },
 *     KeyUsage: { names: ["digitalSignature", "keyCertSign"] },
 *     AuthorityKeyIdentifier: {kid: "1234ab..."},
 *     SubjectAltName: {critical: true, array: [{uri: "http://aaa.com"}, {uri: "http://bbb.com"}]}
 * }
 * @param {string} [certFields.gmUserID] 证书拥有者的id标识, 当creator为gm时，需要该属性，并且不需要其他属性；
 * 当creator为jsrsasign时，不需要该属性
 * @param {string} [creator] 证书生成者，支持使用jsrsasign或gm(即国密)，默认使用jsrsasign
 * @param {gmCertificateCallback} [cb] 回调函数，gm证书生成实现是异步的，所以当creator为gm时，需要提供该参数
 * @returns {string} certPEM pem格式的证书信息，当creator为jsrsasign时，将返回该信息
 */
export function CreateCertificate(certFields: {
    serialNumber: number;
    sigAlg: string;
    issuerDN: string;
    subjectDN: string;
    notBefore: number;
    notAfter: number;
    subjectPubKey: Object;
    issuerPrvKey: Object;
    extensions?: Object | undefined;
    gmUserID?: string | undefined;
}, creator?: string | undefined, cb?: gmCertificateCallback | undefined): string;
/**
 * 生成符合X.509标准的自签名证书信息
 *
 * @param {Object} certFields 证书具体信息
 * @param {number} certFields.serialNumber 证书序列号
 * @param {string} certFields.sigAlg 证书签名算法
 * @param {string} certFields.DN 符合X500标准的代表证书所有者身份标识的Distinguished Name
 * @param {number} certFields.notBefore 代表证书有效性起始时间的unix时间戳
 * @param {number} certFields.notAfter 代表证书有效性终止时间的unix时间戳
 * @param {Object} certFields.keypair 证书拥有方的密钥对，含有jsrsasign提供的prvKeyObj和pubKeyObj对象
 * @param {Object} [certFields.extensions] v3证书扩展属性
 * @returns {string} certPEM pem格式的自签名证书信息
 */
export function CreateSelfSignedCertificate(certFields: {
    serialNumber: number;
    sigAlg: string;
    DN: string;
    notBefore: number;
    notAfter: number;
    keypair: Object;
    extensions?: Object | undefined;
}): string;
/**
 * 验证证书签名信息
 *
 * @param {string} certPEM 符合X509标准的公钥证书信息
 * @param {Object} pubKey 证书签发者的公钥对象
 * @returns {boolean} 证书签名验证结果
 */
export function VerifyCertificateSignature(certPEM: string, pubKey: Object): boolean;
/**
 * 导入已有的公钥证书信息
 *
 * @param {string} certPEM 符合X.509标准的公钥证书信息
 * @returns {Object} x509 jsrsasign提供的X509对象实例
 */
export function ImportCertificate(certPEM: string): Object;
