"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.Transaction = void 0;
var long_1 = __importDefault(require("long"));
var rc2_1 = require("./protos/rc2"); // use the generated static js code
var crypto_1 = require("./crypto");
var txMsgClass = rc2_1.rep.proto.Transaction;
var chaincodeIdMsgClass = rc2_1.rep.proto.ChaincodeId;
var chaincodeDeployMsgClass = rc2_1.rep.proto.ChaincodeDeploy;
var chaincodeInvokeMsgClass = rc2_1.rep.proto.ChaincodeInput;
var signatureMsgClass = rc2_1.rep.proto.Signature;
var Transaction = /** @class */ (function () {
    /**
     * 创建交易对象实例
     * @param iTransaction 创建交易对象所需参数
     */
    function Transaction(iTransaction) {
        var _a;
        this.txMsg = txMsgClass.create(iTransaction);
        this.txSignedTxid = iTransaction.id;
        // If the transaction already been signed
        if ((_a = this.txMsg.signature) === null || _a === void 0 ? void 0 : _a.signature) {
            this.txSignedBuffer = Buffer.from(txMsgClass.encode(this.txMsg).finish());
            this.txSignedSignature = iTransaction.signature;
            // Here txid will not be null
            this.txSignedTxid = iTransaction.id;
        }
        else {
            this.txSignedBuffer = null;
            this.txSignedSignature = null;
        }
    }
    /**
     * A factory method
     * 创建交易实例
     * @param params 创建交易对象所需参数
     * @returns
     */
    Transaction.create = function (params) {
        if (params.type === txMsgClass.Type.CHAINCODE_DEPLOY) {
            return new DeployTransaction(params);
        }
        if (params.type === txMsgClass.Type.CHAINCODE_INVOKE) {
            return new InvokeTransaction(params);
        }
        if (params.type === txMsgClass.Type.CHAINCODE_SET_STATE) {
            return new SetStateTransaction(params);
        }
        throw new Error("Not supported to create transaction with type: ".concat(params.type));
    };
    /**
     * 解析二进制交易数据，构建交易对象实例
     * @param txBytes 二进制交易数据
     * @returns
     */
    Transaction.fromBytes = function (txBytes) {
        var txMsg = txMsgClass.decode(txBytes);
        return new Transaction(txMsg);
    };
    /**
     * 解析json对象格式交易数据，构建交易对象实例
     * @param txJson json格式交易数据
     * @returns
     */
    Transaction.fromJson = function (txJson) {
        var txMsg = txMsgClass.fromObject(txJson);
        return new Transaction(txMsg);
    };
    /**
     * 获取交易对象实例内部由protobuf协议定义的底层交易对象
     * @returns 由protobuf协议定义的交易对象
     */
    Transaction.prototype.getTxMsg = function () {
        return this.txMsg;
    };
    /**
     * 获取交易对象实例经签名后得到的签名交易数据
     * @returns 签名交易数据
     */
    Transaction.prototype.getTxSignedBuffer = function () {
        return this.txSignedBuffer;
    };
    /**
     * 获取已签名交易的唯一标识
     * @returns
     */
    Transaction.prototype.getTxSignedTxid = function () {
        return this.txSignedTxid;
    };
    /**
     * 获取已签名交易数据中的签名数据
     * @returns
     */
    Transaction.prototype.getTxSignedSignature = function () {
        return this.txSignedSignature;
    };
    /**
     * 重置交易唯一标识，重置唯一标识后可作为新的不同交易签名后提交给RepChain区块链
     * @param txid 交易唯一标识
     */
    Transaction.prototype.setTxid = function (txid) {
        this.txMsg.id = txid;
    };
    /**
     * 获取交易数据的hash值，应只在交易签名方法中调用本方法
     * （因为RepChain交易数据结构中时间戳数据只存在于签名字段（结构体）中，若计算hash值时不包含时间戳数据则不同交易的hash值可能完全相同
     * ，比如调用转账合约方法的两笔交易所调用的合约名、合约版本、合约方法及合约方法参数可能完全相同，但在业务上是两次不同操作）
     * @returns 交易hash值(sha256)(hex format string)
     */
    Transaction.prototype._getTxHash = function (txMsg) {
        // 在Browser环境下protobufjs中的encode().finish()返回原始的Uint8Array，
        // 为了屏蔽其与Buffer经browserify或webpack转译后的Uint8Array的差异，这里需转为Buffer
        var txBuffer = Buffer.from(txMsgClass.encode(txMsg).finish());
        return (0, crypto_1.GetHashVal)({ data: txBuffer, alg: "sha256", provider: "jsrsasign" }).toString("hex");
    };
    /**
     * 获取protobuf协议所定义的时间戳对象
     * @param millis
     * @returns
     */
    Transaction.prototype._getTimestamp = function (millis) {
        var timestampMillis = millis || Date.now();
        var timestamp = {
            // 时间戳需再加8小时，以与RepChain使用的时间戳值在同一时区 :(
            seconds: new long_1["default"](timestampMillis / 1000 + 8 * 60 * 60),
            nanos: ((timestampMillis % 1000) * 1000000)
        };
        return rc2_1.google.protobuf.Timestamp.create(timestamp);
    };
    ;
    /**
     * 对新创建的交易对象实例进行签名。产生RepChain区块链可识别的签名交易数据
     * 允许对同一交易对象实例进行多次签名，每次签名将产生新的签名数据并覆盖以前的签名数据，且若交易对象未被指定txid，则每次签名后产生的签名交易具有不同txid，
     * RepChain区块链把具有不同txid的交易视为不同交易
     * @param param0 对交易对象实例进行签名所需参数
     */
    Transaction.prototype.sign = function (_a) {
        var prvKey = _a.prvKey, pubKey = _a.pubKey, alg = _a.alg, pass = _a.pass, creditCode = _a.creditCode, certName = _a.certName, _b = _a.provider, provider = _b === void 0 ? "nodecrypto" : _b;
        var finalPubKey = pubKey;
        // if (this.txMsg.signature?.signature) {
        //     throw new Error("The transaction has been signed already");
        // }
        // Prepare for the calculation of the tx hash including the timestamp 
        var iSignature = {
            certId: {
                creditCode: creditCode,
                certName: certName
            },
            tmLocal: this._getTimestamp(null)
        };
        // Deep clone a txMsg varibale to process
        var err = txMsgClass.verify(this.txMsg);
        if (err) {
            throw new Error(err);
        }
        var txMsgBuffer = Buffer.from(txMsgClass.encode(this.txMsg).finish());
        var txMsgNew = txMsgClass.decode(txMsgBuffer);
        txMsgNew.signature = signatureMsgClass.create(iSignature);
        // 还没有设置txid(即使使用空字符串也等同于未设置) 
        // 将交易哈希值作为其txid
        if (!this.txMsg.id) {
            txMsgNew.id = this._getTxHash(txMsgNew);
        }
        // 删除交易数据中不需要被签名的部分
        txMsgNew.signature = null;
        err = txMsgClass.verify(txMsgNew);
        if (err) {
            throw new Error(err);
        }
        var txBuffer = Buffer.from(txMsgClass.encode(txMsgNew).finish());
        // To sign the tx
        var prvKeyObj = (0, crypto_1.ImportKey)(prvKey, pass); // 私钥解密
        if (prvKeyObj.pubKeyHex === undefined) {
            // A workaround to a bug from jsrassign: 
            // 对于椭圆曲线加密，当使用ImportKey方法从pem格式转object格式时，若其pubKeyHex为undefined则需在该object中补充pubKeyHex
            // 否则签名将出错
            prvKeyObj.pubKeyHex = (0, crypto_1.ImportKey)(finalPubKey).pubKeyHex;
        }
        var prvkeyPEM = (0, crypto_1.GetKeyPEM)(prvKeyObj);
        var signature = (0, crypto_1.Sign)({
            prvKey: prvkeyPEM, data: txBuffer,
            provider: provider,
            alg: alg,
            codeType: "hex"
        });
        iSignature.signature = signature;
        txMsgNew.signature = signatureMsgClass.create(iSignature);
        err = txMsgClass.verify(txMsgNew);
        if (err) {
            throw new Error(err);
        }
        this.txSignedBuffer = Buffer.from(txMsgClass.encode(txMsgNew).finish());
        this.txSignedTxid = txMsgNew.id;
        this.txSignedSignature = txMsgNew.signature;
    };
    /**
     * 对已被签名的交易对象进行签名验证
     * @param param0 验证交易签名所需参数
     * @returns 验签是否成功
     */
    Transaction.prototype.verifySignature = function (_a) {
        var _b;
        var pubKey = _a.pubKey, alg = _a.alg, _c = _a.provider, provider = _c === void 0 ? "nodecrypto" : _c;
        if (!this.txSignedBuffer) {
            throw new Error("The transaction has not been signed");
        }
        var txMsgNew = txMsgClass.decode(this.txSignedBuffer);
        var signature = (_b = txMsgNew.signature) === null || _b === void 0 ? void 0 : _b.signature;
        delete txMsgNew.signature;
        var txMsgNewBuffer = Buffer.from(txMsgClass.encode(txMsgNew).finish());
        var valid = (0, crypto_1.VerifySign)({
            pubKey: pubKey,
            sigValue: Buffer.from(signature), data: txMsgNewBuffer,
            alg: alg,
            provider: provider
        });
        return valid;
    };
    return Transaction;
}());
exports.Transaction = Transaction;
var DeployTransaction = /** @class */ (function (_super) {
    __extends(DeployTransaction, _super);
    /**
     * 创建部署合约交易对象实例
     * @param param0 创建部署合约交易对象所需参数
     */
    function DeployTransaction(_a) {
        /* 当某些字段值为其对应类型的默认值时，将其置为null，确保protobufjs序列化时不编码默认值字段，否则将与其他语言平台的protobuf工具不一致
         * null || null -> null
         * undefined || null -> null
         * 0 || null -> null
         * "" || null -> null
         * false || null -> null
        */
        var chaincodeDeployParams = _a.chaincodeDeployParams, txid = _a.txid, type = _a.type, chaincodeName = _a.chaincodeName, chaincodeVersion = _a.chaincodeVersion, gasLimit = _a.gasLimit, oid = _a.oid;
        var iChaincodeId = {
            chaincodeName: chaincodeName || null,
            version: chaincodeVersion || null
        };
        var chaincodeIdMsg = chaincodeIdMsgClass.create(iChaincodeId);
        var iChaincodeDeploy = {
            timeout: chaincodeDeployParams.timeout || 5000,
            codePackage: chaincodeDeployParams.codePackage || null,
            legalProse: chaincodeDeployParams.legalProse || null,
            cType: chaincodeDeployParams.codeLanguageType || null,
            rType: chaincodeDeployParams.runType || chaincodeDeployMsgClass.RunType.RUN_SERIAL,
            sType: chaincodeDeployParams.stateType || chaincodeDeployMsgClass.StateType.STATE_BLOCK,
            initParameter: chaincodeDeployParams.initParameters || null,
            cclassification: chaincodeDeployMsgClass.ContractClassification.CONTRACT_CUSTOM
        };
        var chaincodeDeployMsg = chaincodeDeployMsgClass.create(iChaincodeDeploy);
        var iTransaction = {
            id: txid || null,
            type: type,
            cid: chaincodeIdMsg,
            spec: chaincodeDeployMsg,
            gasLimit: gasLimit || null,
            oid: oid || null
        };
        return _super.call(this, iTransaction) || this;
    }
    return DeployTransaction;
}(Transaction));
var InvokeTransaction = /** @class */ (function (_super) {
    __extends(InvokeTransaction, _super);
    /**
     * 创建调用合约交易对象实例
     * @param param0 创建调用合约交易对象所需参数
     */
    function InvokeTransaction(_a) {
        // 当某些字段值为其对应类型的默认值时，将其置为null，确保protobufjs序列化时不编码默认值字段，否则将与其他语言平台的protobuf工具不一致
        var chaincodeInvokeParams = _a.chaincodeInvokeParams, txid = _a.txid, type = _a.type, chaincodeName = _a.chaincodeName, chaincodeVersion = _a.chaincodeVersion, gasLimit = _a.gasLimit, oid = _a.oid;
        var iChaincodeId = {
            chaincodeName: chaincodeName || null,
            version: chaincodeVersion || null
        };
        var chaincodeIdMsg = chaincodeIdMsgClass.create(iChaincodeId);
        var iChaincodeInvoke = {
            "function": chaincodeInvokeParams.chaincodeFunction || null,
            args: chaincodeInvokeParams.chaincodeFunctionArgs === [] ? null : chaincodeInvokeParams.chaincodeFunctionArgs
        };
        var chaincodeInvokeMsg = chaincodeInvokeMsgClass.create(iChaincodeInvoke);
        var iTransaction = {
            id: txid || null,
            type: type,
            cid: chaincodeIdMsg,
            ipt: chaincodeInvokeMsg,
            gasLimit: gasLimit || null,
            oid: oid || null
        };
        return _super.call(this, iTransaction) || this;
    }
    return InvokeTransaction;
}(Transaction));
var SetStateTransaction = /** @class */ (function (_super) {
    __extends(SetStateTransaction, _super);
    /**
     * 创建更改合约有效性状态交易对象实例
     * @param param0 创建更改合约有效性状态交易对象所需参数
     */
    function SetStateTransaction(_a) {
        // 当某些字段值为其对应类型的默认值时，将其置为null，确保protobufjs序列化时不编码默认值字段，否则将与其他语言平台的protobuf工具不一致
        var chaincodeSetStateParams = _a.chaincodeSetStateParams, txid = _a.txid, type = _a.type, chaincodeName = _a.chaincodeName, chaincodeVersion = _a.chaincodeVersion, gasLimit = _a.gasLimit, oid = _a.oid;
        var iChaincodeId = {
            chaincodeName: chaincodeName || null,
            version: chaincodeVersion || null
        };
        var chaincodeIdMsg = chaincodeIdMsgClass.create(iChaincodeId);
        var iTransaction = {
            id: txid || null,
            type: type,
            cid: chaincodeIdMsg,
            // Note for encoding with protobuf: 标准实现中oneof类型中的直接标量类型取默认值时在被序列化时依然会被编码(https://developers.google.com/protocol-buffers/docs/proto3?hl=en#oneof)，
            // 这里需要保证protobufjs对oneof中boolean类型的默认值(false)在被序列化时依然能被编码，即不能使其取null值
            state: chaincodeSetStateParams.state,
            gasLimit: gasLimit || null,
            oid: oid || null
        };
        return _super.call(this, iTransaction) || this;
    }
    return SetStateTransaction;
}(Transaction));
