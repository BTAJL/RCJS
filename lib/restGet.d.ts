export default restGet;
/**
 * 实现在Node端通过RestAPI从Repchain节点获取数据信息
 *
 * @param {string} url RepChain Restful API url
 * @returns {Promise<any> | Promise<Buffer>} 返回json对象或二进制数据
 * @memberof RestAPI
 * @private
 */
declare function restGet(url: string): Promise<any> | Promise<Buffer>;
