export declare class EventTube {
    private address;
    private cb;
    private timeout;
    private timer;
    private ws;
    private shouldReconnect;
    private logger;
    /**
     * 构建事件订阅对象实例
     *
     * @param address websocket服务地址
     * @param cb 处理返回信息的回调函数
     * @param timeout 重连的时间间隔
     */
    constructor(address: string, cb?: ((_: {
        [key: string]: any;
    }) => void), timeout?: number, logger?: any);
    /**
     * 获取Websocket连接的目标节点Url
     * @returns 目标节点Url
     */
    getAddress(): string;
    setCallback(cb: ((_: {
        [key: string]: any;
    }) => void)): void;
    reconnect(): void;
    connect(): void;
    /**
     * 主动关闭websocket连接
     *
     * @param reason 解释主动关闭连接的原因，不超过123字节
     * @param code 主动关闭连接的状态码
     */
    close(reason: string, code?: number): void;
}
