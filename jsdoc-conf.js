module.exports = {
    source: {
        include: "src",
        exclude: "src/protos/*",
    },
    opts: {
        template: "node_modules/docdash",
        readme: "README.md",
        destination: "doc/api",
        // Output to directory named with package name and version parsed from package.json
        package: "package.json",
    },
    templates: {
        cleverLinks: true,
        monospaceLinks: false,
        default: {
            staticFiles: {
                include: [
                    "./doc/custom.jsdoc.css",
                ],
            },
        },
    },
    docdash: {
        search: true,
        collapse: false,
        wrap: true,
        commonNav: true,
        scripts: ["custom.jsdoc.css"],
    },
};
