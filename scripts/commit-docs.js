const path = require("path");
const shell = require("shelljs");
const fs = require("fs-extra");
const { name, version } = require("../package.json");

const apiDocsRootDir = path.join(__dirname, "..", "doc", "api");
const apiDocsDir = path.join(apiDocsRootDir, name, version);
const apiDocsBranch = "api-docs";
const silent = true;

const commit = () => {
    if (!fs.existsSync(apiDocsDir)) {
        console.warn(`The api docs(${apiDocsDir}) are not existed`);
        return;
    }

    const curBranch = shell.exec("git branch --show-current", { silent }).stdout.trim();

    // Save the modified and untracked files
    shell.exec("git stash save", { silent: true });

    // Judge whether the apiDocs git branch is existed
    // If existed, already checkouted to it 
    const existed = !shell.exec(`git checkout ${apiDocsBranch}`, { silent }).code;
    // If not existed, create a new orphan branch and checkout to it
    if (!existed) {
        shell.exec(`git switch --discard-changes --orphan ${apiDocsBranch}`, { silent });
    }
    shell.exec(`cp -r ${apiDocsDir} ./ \
    && git add ${version} \
    && git commit -n -m "Commit for the version ${version}"`, { silent });

    // Go back to the working branch and restore the modified and untracked files,
    // with command: git stash pop --index to restore the staged status
    shell.exec(`git checkout ${curBranch} \
    && git stash pop --index`, { silent });
};

commit();
