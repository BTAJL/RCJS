import { rep } from "../src/protos/rc2";
import { EVENTS } from "../src/index";

import EventTube = EVENTS.EventTube;

const websocketUrl = process.env.PEER_WS_URL || "ws://localhost:8081/event"

// 依赖RepChain v1.1.x 自动产生交易模式
describe("事件订阅与数据获取", () => {
    const eventMsgType = rep.proto.Event;
    const blockMsgType = rep.proto.Block;

    it("订阅RepChain区块相关事件，获得区块数据后可以主动关闭", (done) => {
        const et = new EventTube(websocketUrl, ((evt) => {
            const ed = Buffer.from(evt.data);
            const msg = eventMsgType.decode(ed);
            expect(msg.action).toBeGreaterThan(-1);
            expect(msg.action).toBeLessThan(12);
            if (msg.blk) {
                expect(blockMsgType.verify(msg.blk)).toBeNull();
                et.close("works done");
                done();
            }
        }));
        expect(et).toBeDefined();
    }, 100000);
});
