// Karma configuration
// Generated on Thu Jan 17 2019 15:45:00 GMT+0800 (CST)

const webpack = require("webpack");

module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: "../",


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ["jasmine"],


        // list of files / patterns to load in the browser
        files: [
            "crypto.test.js",
            "transaction.test.ts",
            "client.test.ts",
            "browser/*.test.ts",
        ],


        // list of files / patterns to exclude
        exclude: [
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            "*.test.ts": ["webpack", "sourcemap"],
            "*.test.js": ["webpack", "sourcemap"],
            "browser/*.test.ts": ["webpack", "sourcemap"],
            "browser/*.test.js": ["webpack", "sourcemap"],
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ["progress", "dots"],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || 
        // config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ["ChromeHeadless"],

        failOnEmptyTestSuite: false,


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false,

        webpack: {
            mode: "development",
            devtool: "inline-source-map",
            // output: {
            //     filename: "[name].js",
            // },
            // optimization: {
            //     splitChunks: {
            //         minChunks: 2,
            //         chunks: "initial",
            //         name: "common",
            //     },
            // },
            resolve: {
                // root: helpers.root('src'),
                extensions: ['.js', '.ts'],
                fallback: {
                    "crypto": require.resolve("crypto-browserify"),
                    "stream": require.resolve("stream-browserify"),
                    "vm": require.resolve("vm-browserify"),
                },
                alias: {
                    process: "process/browser"
                }
            },
            module: {
                rules: [
                    {
                        test: /\.ts$/,
                        exclude: /node_modules/,
                        use: {
                            loader: 'ts-loader',
                            options: {
                                configFile: "browser/karma.tsconfig.json"
                            }
                        }
                    },

                    {
                        test: /\.(js|ts)$/,
                        use: ["source-map-loader"],
                        enforce: "pre",
                    },
                ],
            },
            plugins: [
                new webpack.ProvidePlugin({
                    process: 'process/browser',
                    Buffer: ["buffer", "Buffer"]
                }),
                new webpack.DefinePlugin({
                    'process.env': {
                        PEER_URL: `'${process.env.PEER_URL}'`,
                        PEER_WS_URL: `'${process.env.PEER_WS_URL}'`,
                    }
                })
            ],
            // plugins: [
            //     new webpack.DefinePlugin({
            //         'process.env': {
            //             PEER_URL: process.env.PEER_URL,
            //             PEER_WS_URL: process.env.PEER_WS_URL,
            //         }
            //     })
            // ],
        },

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity,
    });
};
