import { rep } from "../../src/protos/rc2";
import { EVENTS } from "../../src/index";

import EventTube = EVENTS.EventTube;

const websocketUrl = process.env.PEER_WS_URL || "ws://localhost:8081/event"

// 依赖RepChain v1.1.x 自动产生交易模式
describe("事件订阅与数据获取", () => {
    const eventMsgType = rep.proto.Event;

    it("订阅RepChain区块相关事件，获得区块数据后可以主动关闭", (done) => {
        const et = new EventTube(websocketUrl, ((evt) => {
            // convert blob to Buffer
            const fileReader = new FileReader();
            fileReader.onload = () => {
                const ed = Buffer.from(fileReader.result as ArrayBuffer);
                const msg = eventMsgType.decode(ed);
                expect(msg.action).toBeGreaterThan(-1);
                expect(msg.action).toBeLessThan(12);
                if (msg.blk) {
                    expect(msg.blk).toBeDefined();
                    et.close("works done");
                    done();
                }
            };
            fileReader.readAsArrayBuffer(evt.data);
        }));
        expect(et).toBeDefined();
    }, 100000);
});
