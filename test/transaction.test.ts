import SHA256withECDSA from "../src/algorithmNames";
import { Crypto, TRANSACTION, RCPROTO } from "../src/index";

import Transaction = TRANSACTION.Transaction;
import DeployTxConsArgs = TRANSACTION.DeployTxConsArgs;
import InvokeTxConsArgs = TRANSACTION.InvokeTxConsArgs;
import SetStateTxConsArgs = TRANSACTION.SetStateTxConsArgs;

const { CreateKeypair, GetKeyPEM } = Crypto;

describe("测试交易生成功能", () => {
    const kp1 = CreateKeypair("EC", "secp256k1") as any;
    const kp2 = CreateKeypair("EC", "secp256k1") as any;
    const txToDeployConsArgs: DeployTxConsArgs = {
        type: RCPROTO.rep.proto.Transaction.Type.CHAINCODE_DEPLOY,
        chaincodeName: "ContractAssetsTPL",
        chaincodeVersion: 2,
        chaincodeDeployParams: {
            timeout: 5000,
            codeLanguageType: RCPROTO.rep.proto.ChaincodeDeploy.CodeType.CODE_SCALA,
            legalProse: "fake-content",
            codePackage: "fake-content",
        },
    };
    const txToInvokeConsArgs: InvokeTxConsArgs = {
        type: RCPROTO.rep.proto.Transaction.Type.CHAINCODE_INVOKE,
        chaincodeName: "ContractAssetsTPL",
        chaincodeVersion: 2,
        chaincodeInvokeParams: {
            chaincodeFunction: "transfer",
            chaincodeFunctionArgs: [`{
                "from" : "121000005l35120456",
                "to" : "12110107bi45jh675g",
                "amount" : 5
            }`],
        },
        txid: "some-unique-id",
        gasLimit: 1000,
        oid: "some-oid"
    };

    const txToSetStateConsArgs: SetStateTxConsArgs = {
        type: RCPROTO.rep.proto.Transaction.Type.CHAINCODE_SET_STATE,
        chaincodeName: "ContractAssetsTPL",
        chaincodeVersion: 2,
        chaincodeSetStateParams: {
            state: false,
        },
    };
    const txToDeploy = Transaction.create(txToDeployConsArgs);
    const txToInvoke = Transaction.create(txToInvokeConsArgs);
    const txToSetState = Transaction.create(txToSetStateConsArgs);
    txToDeploy.sign({
        prvKey: GetKeyPEM(kp1.prvKeyObj),
        pubKey: GetKeyPEM(kp1.pubKeyObj),
        alg: SHA256withECDSA,
        creditCode: "121000005l35120456",
        certName: "node1",
    });
    txToInvoke.sign({
        prvKey: GetKeyPEM(kp1.prvKeyObj),
        pubKey: GetKeyPEM(kp1.pubKeyObj),
        alg: SHA256withECDSA,
        creditCode: "121000005l35120456",
        certName: "node1",
    });
    txToSetState.sign({
        prvKey: GetKeyPEM(kp1.prvKeyObj),
        pubKey: GetKeyPEM(kp1.pubKeyObj),
        alg: SHA256withECDSA,
        creditCode: "121000005l35120456",
        certName: "node1",
    });

    const signAlg = SHA256withECDSA;

    it("使用未加密私钥生成的签名交易信息，使用相应的公钥验证应能通过", () => {
        expect(txToDeploy.verifySignature({ pubKey: GetKeyPEM(kp1.pubKeyObj), alg: signAlg })).toBeTruthy();
        expect(txToInvoke.verifySignature({ pubKey: GetKeyPEM(kp1.pubKeyObj), alg: signAlg })).toBeTruthy();
        expect(txToSetState.verifySignature({ pubKey: GetKeyPEM(kp1.pubKeyObj), alg: signAlg })).toBeTruthy();
    });
    it("使用未加密私钥生成的签名交易信息，使用非对应公钥验证不应通过", () => {
        expect(txToDeploy.verifySignature({ pubKey: GetKeyPEM(kp2.pubKeyObj), alg: signAlg })).toBeFalsy();
        expect(txToInvoke.verifySignature({ pubKey: GetKeyPEM(kp2.pubKeyObj), alg: signAlg })).toBeFalsy();
        expect(txToSetState.verifySignature({ pubKey: GetKeyPEM(kp2.pubKeyObj), alg: signAlg })).toBeFalsy();
    });
    it("使用未加密私钥生成的签名交易信息，使用错误签名算法验证不应通过", () => {
        const wrongSignAlg = "sha512";
        expect(txToDeploy.verifySignature({ pubKey: GetKeyPEM(kp1.pubKeyObj), alg: wrongSignAlg })).toBeFalsy();
        expect(txToInvoke.verifySignature({ pubKey: GetKeyPEM(kp1.pubKeyObj), alg: wrongSignAlg })).toBeFalsy();
        expect(txToSetState.verifySignature({ pubKey: GetKeyPEM(kp1.pubKeyObj), alg: wrongSignAlg })).toBeFalsy();
    });
    it("使用二进制交易数据构造交易对象实例，应能成功", () => {
        const txSignedBytes = txToDeploy.getTxSignedBuffer() as Buffer;
        const txSignedParsed = Transaction.fromBytes(txSignedBytes);
        expect(txSignedBytes).toEqual(txSignedParsed.getTxSignedBuffer());
        const txSignedParsedBytes = Buffer.from(RCPROTO.rep.proto.Transaction.encode(txSignedParsed.getTxMsg()).finish());
        expect(txSignedParsedBytes).toEqual(txSignedBytes);
    });
    it("使用Json交易数据构造交易对象实例，应能成功", () => {
        const txSignedBytes = txToInvoke.getTxSignedBuffer() as Buffer;
        let txSignedParsed = Transaction.fromBytes(txSignedBytes);
        const txSignedJson = txSignedParsed.getTxMsg().toJSON();
        txSignedParsed = Transaction.fromJson(txSignedJson);
        expect(txSignedParsed.getTxMsg().cid).toEqual(txToInvoke.getTxMsg().cid);
        expect(txSignedParsed.getTxMsg().ipt).toEqual(txToInvoke.getTxMsg().ipt);
        expect(txSignedParsed.getTxMsg().oid).toEqual(txToInvoke.getTxMsg().oid);
        expect(txSignedParsed.getTxMsg().gasLimit).toEqual(txToInvoke.getTxMsg().gasLimit);
        expect(txSignedParsed.getTxSignedBuffer()).toEqual(txToInvoke.getTxSignedBuffer());
    });
    it("对已签名交易进行签名，应产生新的已签名交易数据", () => {
        const txSignedBufferOld = Buffer.from(txToInvoke.getTxSignedBuffer() as Buffer);
        txToInvoke.sign({
            prvKey: GetKeyPEM(kp1.prvKeyObj),
            pubKey: GetKeyPEM(kp1.pubKeyObj),
            alg: signAlg,
            creditCode: "121000005l35120456",
            certName: "node1",
        });
        const txSignedBufferNew = txToInvoke.getTxSignedBuffer();
        expect(txSignedBufferOld).not.toEqual(txSignedBufferNew)
    });
    it("对未签名交易对象进行验签，应抛出异常", () => {
        const txNotSignedYet = Transaction.create(txToInvokeConsArgs);
        expect(() => {
            txNotSignedYet.verifySignature({ pubKey: GetKeyPEM(kp1.pubKeyObj), alg: signAlg });
        }).toThrowError("The transaction has not been signed");
    });
    it("使用加密私钥，输入正确密码生成的签名交易信息，使用相应公钥验证应能通过", () => {
        const txToDeployNew = Transaction.create(txToDeployConsArgs);
        const pass = "123";
        const prvKeyEncrypted = GetKeyPEM(kp1.prvKeyObj, pass);
        txToDeployNew.sign({
            prvKey: prvKeyEncrypted,
            pubKey: GetKeyPEM(kp1.pubKeyObj),
            alg: signAlg,
            pass,
            creditCode: "121000005l35120456",
            certName: "node1",
        });
        expect(txToDeploy.verifySignature({ pubKey: GetKeyPEM(kp1.pubKeyObj), alg: signAlg })).toBeTruthy();
    });
    it("使用加密私钥生成签名交易信息，输入密码错误时，应该抛出异常", () => {
        const txToDeployNew = Transaction.create(txToDeployConsArgs);
        const pass = "123";
        const prvKeyEncrypted = GetKeyPEM(kp1.prvKeyObj, pass);
        expect(() => {
            txToDeployNew.sign({
                prvKey: prvKeyEncrypted,
                pubKey: GetKeyPEM(kp1.pubKeyObj),
                alg: signAlg,
                pass: "456",
                creditCode: "121000005l35120456",
                certName: "node1",
            });
        }).toThrowError("提供的私钥信息无效或解密密码无效");
        expect(() => {
            txToDeployNew.sign({
                prvKey: prvKeyEncrypted,
                pubKey: GetKeyPEM(kp1.pubKeyObj),
                alg: signAlg,
                creditCode: "121000005l35120456",
                certName: "node1",
            });
        }).toThrowError("提供的私钥信息无效或解密密码无效");
    });
});
