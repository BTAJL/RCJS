import validator from "validator";
import _ from "lodash";
import SHA256withECDSA from "../src/algorithmNames";
import { CLIENT, TRANSACTION, Crypto, RCPROTO } from "../src/index";

import Client = CLIENT.Client;
import Block = RCPROTO.rep.proto.Block;
import Transaction = TRANSACTION.Transaction;

const peerUrl: string = process.env.PEER_URL || "http://localhost:8081";
const { CreateKeypair, GetKeyPEM } = Crypto;

// 依赖RepChain v2.0+ 自动产生交易模式，且已产生超过1个区块
describe("Client功能验证", () => {
    const client = new Client(peerUrl);

    describe("chainInfo相关方法测试", () => {
        it("使用chainInfo方法, 应能得到区块链的当前概要信息", (done) => {
            client.chainInfo().then((ci) => {
                expect(ci.height).toBeGreaterThan(0);
                expect(ci.totalTransactions).toBeGreaterThan(0);
                expect(validator.isBase64(ci.currentBlockHash)).toBeTruthy();
                expect(validator.isBase64(ci.previousBlockHash)).toBeTruthy();
                expect(validator.isBase64(ci.currentStateHash) || ci.currentStateHash === "").toBeTruthy();
                done();
            });
        });
        it("使用chainHeight方法，应能得到区块链的最新区块高度", () => client.chainHeight().then((height) => {
            expect(height).toBeGreaterThan(0);
        }));
        it("使用chainCurrentBlockHash方法，应能得到区块链的最新区块哈希值", () => client.chainCurrentBlockHash().then((curHash) => {
            expect(validator.isBase64(curHash)).toBeTruthy();
        }));
        it("使用chainPreviousBlockHash方法，应能得到区块链的最新区块的父区块哈希值", () => client.chainPreviousBlockHash().then((prevHash) => {
            expect(validator.isBase64(prevHash)).toBeTruthy();
        }));
        it("使用chainCurrentStateHash方法，应能得到区块链的最新世界状态哈希值", () => client.chainCurrentStateHash().then((curStateHash) => {
            expect(validator.isBase64(curStateHash) || curStateHash === "").toBeTruthy();
        }));
        it("使用chainTotalTransactions方法，应能得到区块链的最新交易总量", () => client.chainTotalTransactions().then((totalTransactions) => {
            expect(totalTransactions).toBeGreaterThan(0);
        }));
    });

    describe("block相关方法测试", () => {
        it("使用block方法, 根据区块高度可以获取json格式的区块数据", async () => {
            // default blockFormat(JSON)
            await client.block(1).then((blk: any) => {
                expect(_.isPlainObject(blk)).toBeTruthy();
                expect(blk.transactions.length).toBeGreaterThan(0);
            });
            // JSON blockFormat
            await client.block(1, CLIENT.RESPONSE_FORMAT.JSON).then((blk: any) => {
                expect(_.isPlainObject(blk)).toBeTruthy();
                expect(blk.transactions.length).toBeGreaterThan(0);
            });
        });
        it("使用block方法, 根据区块高度可以获取stream格式的区块数据", (done) => {
            client.block(1, CLIENT.RESPONSE_FORMAT.STREAM).then((blk) => {
                expect(Buffer.isBuffer(blk)).toBeTruthy();
                const blkObj = Block.decode(blk as Buffer);
                expect(blkObj.transactions.length).toBeGreaterThan(0);
                done();
            });
        });
        it("使用block方法, 根据区块哈希值可以获取json格式的区块数据", async () => {
            const blkHash = await client.block(1).then((blk: any) => blk.header.hashPresent);
            const blkHashBuffer = Buffer.from(blkHash, "base64");
            // string hash and default blockFormat(JSON)
            await client.block(blkHash).then((blk: any) => {
                expect(_.isPlainObject(blk)).toBeTruthy();
                expect(blk.transactions.length).toBeGreaterThan(0);
            });
            // string hash and JSON blockFormat
            await client.block(blkHash, CLIENT.RESPONSE_FORMAT.JSON).then((blk: any) => {
                expect(_.isPlainObject(blk)).toBeTruthy();
                expect(blk.transactions.length).toBeGreaterThan(0);
            });
            // buffer hash and default blockFormat(JSON)
            await client.block(blkHashBuffer).then((blk: any) => {
                expect(_.isPlainObject(blk)).toBeTruthy();
                expect(blk.transactions.length).toBeGreaterThan(0);
            });
            // buffer hash and JSON blockFormat
            await client.block(blkHashBuffer, CLIENT.RESPONSE_FORMAT.JSON).then((blk: any) => {
                expect(_.isPlainObject(blk)).toBeTruthy();
                expect(blk.transactions.length).toBeGreaterThan(0);
            });
        });
        // 404 yet
        // it("使用block方法, 根据区块哈希值可以获取stream格式的区块数据", async () => {
        //     const blkHash = await client.block(1).then(blk => blk.hashOfBlock);
        //     const blkHashBuffer = Buffer.from(blkHash, "base64");
        //     await client.block(blkHash, "STREAM").then((blk) => {
        //         expect(Buffer.isBuffer(blk)).toBeTruthy();
        //         const blkObj = Block.decode(blk);
        //         expect(blkObj.transactions.length).toBeGreaterThan(0);
        //     });
        //     await client.block(blkHashBuffer, "STREAM").then((blk) => {
        //         expect(Buffer.isBuffer(blk)).toBeTruthy();
        //         const blkObj = Block.decode(blk);
        //         expect(blkObj.transactions.length).toBeGreaterThan(0);
        //     });
        // });
        it("使用block方法，通过相同区块id获取的json格式与stream格式区块数据应等价", async () => {
            const height = await client.chainHeight();
            const blockJson = await client.block(height);
            const blockBuffer = await client.block(height, CLIENT.RESPONSE_FORMAT.STREAM);
            // A work around to parse json format message from other protobuf tools
            const parseProtoJson = (obj: any) => {
                for (let key in obj) {
                    if(obj.hasOwnProperty(key)) {
                        if(typeof obj[key] === "object") {
                            parseProtoJson(obj[key]);
                        } else if (key === "tmLocal") {
                            const timestamp = new Date(obj[key]);
                            obj[key] = {
                                seconds: Math.floor(timestamp.getTime() / 1000) + 8 * 3600,
                                nanos: timestamp.getMilliseconds() * 1000000,
                            }
                        }
                    }
                }
            }
            const blockMsgFromBuffer = Block.decode(blockBuffer as Buffer);
            parseProtoJson(blockJson);
            const blockMsgFromJson = Block.fromObject(blockJson);
            expect(_.isMatch(blockMsgFromJson, blockMsgFromBuffer)).toBeTruthy();
        });
    });

    describe("transaction相关方法测试", () => {
        const prvKeyPEM = `-----BEGIN PRIVATE KEY-----
MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgMwOg+N/MxN5qRBo2
X/wLmUHqEoDZEF3/aSpTVUvvkuugCgYIKoZIzj0DAQehRANCAASbuz0+GbOYaqjY
WvUqz+tE3nCA1WBq8tL+msleZT/MORSdlLURAq6VMbqDtqX2h38cmCFwmQPMO2Gm
VNLOAhH3
-----END PRIVATE KEY-----`;
        const pubKeyPEM = `-----BEGIN CERTIFICATE-----
MIIBTDCB9KADAgECAgRd7wBCMAoGCCqGSM49BAMCMC8xETAPBgNVBAoMCHJlcGNo
YWluMQ4wDAYDVQQLDAVpc2NhczEKMAgGA1UEAwwBMTAeFw0xOTEyMTAwMjE3Mzha
Fw0yMDEyMDkwMjE3MzhaMC8xETAPBgNVBAoMCHJlcGNoYWluMQ4wDAYDVQQLDAVp
c2NhczEKMAgGA1UEAwwBMTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABJu7PT4Z
s5hqqNha9SrP60TecIDVYGry0v6ayV5lP8w5FJ2UtRECrpUxuoO2pfaHfxyYIXCZ
A8w7YaZU0s4CEfcwCgYIKoZIzj0EAwIDRwAwRAIgcAxkxmiNexPx8CF+DvII7168
eeVcfsJwoMcFFLKCWrECICSIkc9vC6Vwze3s2UwBuIiSlwNxZ0YDJcdlbcmESWHy
-----END CERTIFICATE-----`;
        const txToInvokeConsArgs: TRANSACTION.InvokeTxConsArgs = {
            type: RCPROTO.rep.proto.Transaction.Type.CHAINCODE_INVOKE,
            chaincodeName: "ContractAssetsTPL",
            chaincodeVersion: 1,
            chaincodeInvokeParams: {
                chaincodeFunction: "transfer",
                chaincodeFunctionArgs: [`{
                "from" : "121000005l35120456",
                "to" : "12110107bi45jh675g",
                "amount" : 5
            }`],
            },
        };

        const tx = Transaction.create(txToInvokeConsArgs);
        tx.sign({
            prvKey: prvKeyPEM,
            pubKey: pubKeyPEM,
            alg: SHA256withECDSA,
            creditCode: "121000005l35120456",
            certName: "node1",
        });
        const txSignedBuffer1 = Buffer.from(tx.getTxSignedBuffer() as Buffer);

        // const kp: any = CreateKeypair();
        // tx.sign({
        //     prvKey: GetKeyPEM(kp.prvKeyObj),
        //     pubKey: GetKeyPEM(kp.prvKeyObj),
        //     alg: SHA256withECDSA,
        //     creditCode: "121000005l35120456",
        //     certName: "node1",
        // });
        // const txSignedBuffer2 = Buffer.from(tx.getTxSignedBuffer() as Buffer);

        // tx.sign({
        //     prvKey: prvKeyPEM,
        //     pubKey: pubKeyPEM,
        //     alg: "sha512",
        //     creditCode: "121000005l35120456",
        //     certName: "node1",
        // });
        // const txSignedBuffer3 = Buffer.from(tx.getTxSignedBuffer() as Buffer);

        tx.sign({
            prvKey: prvKeyPEM,
            pubKey: pubKeyPEM,
            alg: SHA256withECDSA,
            creditCode: "121000005l35120456",
            certName: "node1",
        });
        const txSignedBuffer4 = Buffer.from(tx.getTxSignedBuffer() as Buffer);

        it("使用sendTransaction方法，以hex字符串形式提交正确的签名交易，应能返回成功信息", async () => {
            const result: any = await client.sendTransaction(txSignedBuffer1.toString("hex"));
            const txSignedParsed = Transaction.fromBytes(txSignedBuffer1);
            expect(result.txid).toBe(txSignedParsed.getTxMsg().id);
            expect(result.err).toBeUndefined();
        });
        it("使用sendTransaction方法，以非hex编码的字符串形式提交交易，应返回交易解析错误信息", async () => {
            let result: any = await client.sendTransaction(txSignedBuffer1.toString("base64"));
            expect(result.err).toMatch("transaction parser error");
            result = await client.sendTransaction(txSignedBuffer1.toString("utf8"));
            expect(result.err).toMatch("transaction parser error");
        });

        // RepChain v2 doesn not return error message now
        // it("使用sendTransaction方法，以hex字符串形式提交错误私钥签名的交易，应返回验签错误信息", async () => {
        //     const result: any = await client.sendTransaction(txSignedBuffer2.toString("hex"));
        //     expect(result.err).toBe("验证签名出错");
        // });
        // it("使用sendTransaction方法，以hex字符串形式提交错误算法签名的交易，应返回验签错误信息", async () => {
        //     const result: any = await client.sendTransaction(txSignedBuffer3.toString("hex"));
        //     expect(result.err).toBe("验证签名出错");
        // });
        it("使用sendTransaction方法，以二进制形式提交正确的签名交易，应能返回成功信息", async () => {
            const result: any = await client.sendTransaction(txSignedBuffer4);
            const txSignedParsed = Transaction.fromBytes(txSignedBuffer4);
            expect(result.txid).toBe(txSignedParsed.getTxMsg().id);
            expect(result.err).toBeUndefined();
        });
        // it("使用sendTransaction方法，不传入公钥，以hex字符串形式提交正确的签名交易，应能返回成功信息", async () => {
        //     const result = await client.sendTransaction(txSignedBuffer4.toString("hex"));
        //     expect(result.txid).toBe(tx4.getTxMsg().id);
        //     expect(result.err).toBeUndefined();
        // });
    });
});
