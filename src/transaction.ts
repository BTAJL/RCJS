import Long from "long";
import { rep, google } from "./protos/rc2"; // use the generated static js code
import {
    GetHashVal, ImportKey, Sign, VerifySign, GetKeyPEM,
} from "./crypto";

import txMsgClass = rep.proto.Transaction;
import chaincodeIdMsgClass = rep.proto.ChaincodeId;
import chaincodeDeployMsgClass = rep.proto.ChaincodeDeploy;
import chaincodeInvokeMsgClass = rep.proto.ChaincodeInput;
import signatureMsgClass = rep.proto.Signature;

type TxType = rep.proto.Transaction.Type;
type IChaincodeDeploy = rep.proto.IChaincodeDeploy;
type IChaincodeInvoke = rep.proto.IChaincodeInput;
type ISignture = rep.proto.ISignature;
type ITransaction = rep.proto.ITransaction;
type IChaincodeId = rep.proto.IChaincodeId

export interface TxConsArgs {
    /** 交易唯一标识 */
    txid?: string | null,
    /** 交易类型 */
    type: TxType;
    /** 目标合约的名称 */
    chaincodeName: string;
    /** 目标合约的版本号 */
    chaincodeVersion: number;
    /** 交易执行消耗的gas资源上限，允许为null以在序列化时不编码默认值 */
    gasLimit?: number | null;
    /** 目标合约实例，允许为null以在序列化时不编码默认值 */
    oid?: string | null;
}

export interface DeployTxConsArgs extends TxConsArgs {
    chaincodeDeployParams: {
        /** 部署合约过程的超时限制 */
        timeout?: number;
        /** 合约代码 */
        codePackage: string;
        /** 合约规则法律描述 */
        legalProse?: string;
        /** 合约语言类型 */
        codeLanguageType: chaincodeDeployMsgClass.CodeType;
        /** 合约串行/并行方式 */
        runType?: chaincodeDeployMsgClass.RunType;
        /** 合约状态类型 */
        stateType?: chaincodeDeployMsgClass.StateType;
        /** 合约初始化方法参数 */
        initParameters?: string;
    };
}

export interface InvokeTxConsArgs extends TxConsArgs {
    chaincodeInvokeParams: {
        /** 待被调用的合约方法名 */
        chaincodeFunction: string;
        /** 待调用的合约方法的参数 */
        chaincodeFunctionArgs: Array<string>;
    };
}

export interface SetStateTxConsArgs extends TxConsArgs {
    chaincodeSetStateParams: {
        /** 目标合约的新状态，当值为false时表示使该合约无效 */
        state: boolean;
    };
}

export interface SignTxArgs {
    /** 签名者的pem格式私钥 */
    prvKey: string,
    /** 签名者的pem格式公钥 */
    pubKey: string,
    /** 使用的签名算法名称 */
    alg: string,
    /** 私钥解密密码，如果prvKey为已加密的pem格式私钥，则需要提供此解密密码 */
    pass?: string,
    /** 签名者的信用代码 */
    creditCode: string,
    /** 代表签名者的证书名 */
    certName: string,
    /** 签名时使用的签名方法提供者，可为nodecrypto(默认值)或jsrsasign */
    provider?: string,
}

export interface VerifySignatureArgs {
    /** pem格式的公钥 */
    pubKey: string,
    /** 使用的签名算法 */
    alg: string,
    /** 验签时使用的验签方法提供者，可为nodecrypto(默认值)或jsrsasign */
    provider?: string
}

export class Transaction {
    /** 底层protobuf定义的交易对象 */
    private txMsg: txMsgClass;
    /** 已签名交易数据 */
    private txSignedBuffer: Buffer | null;
    /** 已签名交易的唯一标识（若构建交易时未指定txid，则签名后会自动生成） */
    private txSignedTxid: string | null;
    /** 已签名交易数据中的签名数据 */
    private txSignedSignature: ISignture | null;

    /**
     * 创建交易对象实例 
     * @param iTransaction 创建交易对象所需参数 
     */
    protected constructor(iTransaction: ITransaction) {
        this.txMsg = txMsgClass.create(iTransaction);
        this.txSignedTxid = iTransaction.id as string | null;

        // If the transaction already been signed
        if (this.txMsg.signature?.signature) {
            this.txSignedBuffer = Buffer.from(txMsgClass.encode(this.txMsg).finish());
            this.txSignedSignature = iTransaction.signature as ISignture;
            // Here txid will not be null
            this.txSignedTxid = iTransaction.id as string;
        } else {
            this.txSignedBuffer = null;
            this.txSignedSignature = null;
        }
    }

    /**
     * A factory method
     * 创建交易实例
     * @param params 创建交易对象所需参数
     * @returns 
     */
    public static create(params: TxConsArgs): Transaction {
        if (params.type === txMsgClass.Type.CHAINCODE_DEPLOY) {
            return new DeployTransaction(params as DeployTxConsArgs);
        }
        if (params.type === txMsgClass.Type.CHAINCODE_INVOKE) {
            return new InvokeTransaction(params as InvokeTxConsArgs);
        }
        if (params.type === txMsgClass.Type.CHAINCODE_SET_STATE) {
            return new SetStateTransaction(params as SetStateTxConsArgs);
        }
        throw new Error(`Not supported to create transaction with type: ${params.type}`);
    }

    /**
     * 解析二进制交易数据，构建交易对象实例
     * @param txBytes 二进制交易数据
     * @returns 
     */
    public static fromBytes(txBytes: Buffer): Transaction {
        const txMsg = txMsgClass.decode(txBytes);
        return new Transaction(txMsg);
    }

    /**
     * 解析json对象格式交易数据，构建交易对象实例 
     * @param txJson json格式交易数据
     * @returns 
     */
    public static fromJson(txJson: { [key: string]: any }): Transaction {
        const txMsg = txMsgClass.fromObject(txJson);
        return new Transaction(txMsg);
    }

    /**
     * 获取交易对象实例内部由protobuf协议定义的底层交易对象
     * @returns 由protobuf协议定义的交易对象
     */
    getTxMsg(): txMsgClass {
        return this.txMsg;
    }

    /**
     * 获取交易对象实例经签名后得到的签名交易数据 
     * @returns 签名交易数据 
     */
    getTxSignedBuffer(): Buffer | null {
        return this.txSignedBuffer;
    }

    /**
     * 获取已签名交易的唯一标识 
     * @returns 
     */
    getTxSignedTxid(): string | null {
        return this.txSignedTxid;
    }

    /**
     * 获取已签名交易数据中的签名数据 
     * @returns 
     */
    getTxSignedSignature(): ISignture | null {
        return this.txSignedSignature;
    }

    /**
     * 重置交易唯一标识，重置唯一标识后可作为新的不同交易签名后提交给RepChain区块链
     * @param txid 交易唯一标识
     */
    public setTxid(txid: string) {
        this.txMsg.id = txid;
    }

    /**
     * 获取交易数据的hash值，应只在交易签名方法中调用本方法
     * （因为RepChain交易数据结构中时间戳数据只存在于签名字段（结构体）中，若计算hash值时不包含时间戳数据则不同交易的hash值可能完全相同
     * ，比如调用转账合约方法的两笔交易所调用的合约名、合约版本、合约方法及合约方法参数可能完全相同，但在业务上是两次不同操作）
     * @returns 交易hash值(sha256)(hex format string)
     */
    private _getTxHash(txMsg: txMsgClass): string {
        // 在Browser环境下protobufjs中的encode().finish()返回原始的Uint8Array，
        // 为了屏蔽其与Buffer经browserify或webpack转译后的Uint8Array的差异，这里需转为Buffer
        const txBuffer = Buffer.from(txMsgClass.encode(txMsg).finish());
        return GetHashVal({ data: txBuffer, alg: "sha256", provider: "jsrsasign" }).toString("hex");
    }

    /**
     * 获取protobuf协议所定义的时间戳对象
     * @param millis 
     * @returns 
     */
    private _getTimestamp(millis: number | null): google.protobuf.ITimestamp {
        const timestampMillis = millis || Date.now();
        const timestamp: google.protobuf.ITimestamp = {
            // 时间戳需再加8小时，以与RepChain使用的时间戳值在同一时区 :(
            seconds: new Long(timestampMillis / 1000 + 8 * 60 * 60),
            nanos: ((timestampMillis % 1000) * 1000000),
        };

        return google.protobuf.Timestamp.create(timestamp);
    };

    /**
     * 对新创建的交易对象实例进行签名。产生RepChain区块链可识别的签名交易数据
     * 允许对同一交易对象实例进行多次签名，每次签名将产生新的签名数据并覆盖以前的签名数据，且若交易对象未被指定txid，则每次签名后产生的签名交易具有不同txid，
     * RepChain区块链把具有不同txid的交易视为不同交易
     * @param param0 对交易对象实例进行签名所需参数
     */
    sign({
        prvKey, pubKey, alg, pass, creditCode, certName, provider = "nodecrypto",
    }: SignTxArgs) {
        const finalPubKey = pubKey;

        // if (this.txMsg.signature?.signature) {
        //     throw new Error("The transaction has been signed already");
        // }

        // Prepare for the calculation of the tx hash including the timestamp 
        const iSignature: ISignture = {
            certId: {
                creditCode,
                certName,
            },
            tmLocal: this._getTimestamp(null),
        };

        // Deep clone a txMsg varibale to process
        let err = txMsgClass.verify(this.txMsg);
        if (err) {
            throw new Error(err);
        }
        const txMsgBuffer = Buffer.from(txMsgClass.encode(this.txMsg).finish());
        const txMsgNew = txMsgClass.decode(txMsgBuffer);
        txMsgNew.signature = signatureMsgClass.create(iSignature);
        // 还没有设置txid(即使使用空字符串也等同于未设置) 
        // 将交易哈希值作为其txid
        if (!this.txMsg.id) {
            txMsgNew.id = this._getTxHash(txMsgNew);
        }

        // 删除交易数据中不需要被签名的部分
        txMsgNew.signature = null;

        err = txMsgClass.verify(txMsgNew);
        if (err) {
            throw new Error(err);
        }
        let txBuffer = Buffer.from(txMsgClass.encode(txMsgNew).finish());

        // To sign the tx
        const prvKeyObj: any = ImportKey(prvKey, pass); // 私钥解密
        if (prvKeyObj.pubKeyHex === undefined) {
            // A workaround to a bug from jsrassign: 
            // 对于椭圆曲线加密，当使用ImportKey方法从pem格式转object格式时，若其pubKeyHex为undefined则需在该object中补充pubKeyHex
            // 否则签名将出错
            prvKeyObj.pubKeyHex = (ImportKey(finalPubKey) as any).pubKeyHex;
        }
        const prvkeyPEM = GetKeyPEM(prvKeyObj);
        const signature = Sign({
            prvKey: prvkeyPEM, data: txBuffer, provider, alg, codeType: "hex",
        });

        iSignature.signature = signature;
        txMsgNew.signature = signatureMsgClass.create(iSignature);

        err = txMsgClass.verify(txMsgNew);
        if (err) {
            throw new Error(err);
        }
        this.txSignedBuffer = Buffer.from(txMsgClass.encode(txMsgNew).finish());
        this.txSignedTxid = txMsgNew.id;
        this.txSignedSignature = txMsgNew.signature;
    }

    /**
     * 对已被签名的交易对象进行签名验证
     * @param param0 验证交易签名所需参数
     * @returns 验签是否成功
     */
    verifySignature({ pubKey, alg, provider = "nodecrypto" }: VerifySignatureArgs): boolean {
        if (!this.txSignedBuffer) {
            throw new Error("The transaction has not been signed");
        }
        const txMsgNew = txMsgClass.decode(this.txSignedBuffer);
        const signature = txMsgNew.signature?.signature;
        delete txMsgNew.signature;
        const txMsgNewBuffer = Buffer.from(txMsgClass.encode(txMsgNew).finish());
        const valid = VerifySign({
            pubKey, sigValue: Buffer.from(signature as Uint8Array), data: txMsgNewBuffer, alg, provider,
        });
        return valid;
    }
}

class DeployTransaction extends Transaction {
    /**
     * 创建部署合约交易对象实例
     * @param param0 创建部署合约交易对象所需参数
     */
    constructor({ chaincodeDeployParams, txid, type, chaincodeName, chaincodeVersion, gasLimit, oid}: DeployTxConsArgs) {

        /* 当某些字段值为其对应类型的默认值时，将其置为null，确保protobufjs序列化时不编码默认值字段，否则将与其他语言平台的protobuf工具不一致
         * null || null -> null
         * undefined || null -> null
         * 0 || null -> null
         * "" || null -> null
         * false || null -> null
        */

        const iChaincodeId: IChaincodeId = {
            chaincodeName: chaincodeName || null,
            version: chaincodeVersion || null,
        };
        const chaincodeIdMsg = chaincodeIdMsgClass.create(iChaincodeId);
        const iChaincodeDeploy: IChaincodeDeploy = {
            timeout: chaincodeDeployParams.timeout || 5000,
            codePackage: chaincodeDeployParams.codePackage || null,
            legalProse: chaincodeDeployParams.legalProse || null,
            cType: chaincodeDeployParams.codeLanguageType || null,
            rType: chaincodeDeployParams.runType || chaincodeDeployMsgClass.RunType.RUN_SERIAL,
            sType: chaincodeDeployParams.stateType || chaincodeDeployMsgClass.StateType.STATE_BLOCK,
            initParameter: chaincodeDeployParams.initParameters || null,
            cclassification: chaincodeDeployMsgClass.ContractClassification.CONTRACT_CUSTOM,
        };
        const chaincodeDeployMsg = chaincodeDeployMsgClass.create(iChaincodeDeploy);

        const iTransaction: ITransaction = {
            id: txid || null,
            type: type,
            cid: chaincodeIdMsg,
            spec: chaincodeDeployMsg,
            gasLimit: gasLimit || null,
            oid: oid || null,
        };

        super(iTransaction);
    }
}

class InvokeTransaction extends Transaction {
    /**
     * 创建调用合约交易对象实例
     * @param param0 创建调用合约交易对象所需参数
     */
    constructor({ chaincodeInvokeParams, txid, type, chaincodeName, chaincodeVersion, gasLimit, oid}: InvokeTxConsArgs) {

        // 当某些字段值为其对应类型的默认值时，将其置为null，确保protobufjs序列化时不编码默认值字段，否则将与其他语言平台的protobuf工具不一致

        const iChaincodeId: IChaincodeId = {
            chaincodeName: chaincodeName || null,
            version: chaincodeVersion || null,
        };
        const chaincodeIdMsg = chaincodeIdMsgClass.create(iChaincodeId);
        const iChaincodeInvoke: IChaincodeInvoke = {
            function: chaincodeInvokeParams.chaincodeFunction || null,
            args: chaincodeInvokeParams.chaincodeFunctionArgs === [] ? null : chaincodeInvokeParams.chaincodeFunctionArgs,
        }
        const chaincodeInvokeMsg = chaincodeInvokeMsgClass.create(iChaincodeInvoke);

        const iTransaction: ITransaction = {
            id: txid || null,
            type: type,
            cid: chaincodeIdMsg,
            ipt: chaincodeInvokeMsg,
            gasLimit: gasLimit || null,
            oid: oid || null,
        };

        super(iTransaction);
    }
}

class SetStateTransaction extends Transaction {
    /**
     * 创建更改合约有效性状态交易对象实例 
     * @param param0 创建更改合约有效性状态交易对象所需参数
     */
    constructor({ chaincodeSetStateParams, txid, type, chaincodeName, chaincodeVersion, gasLimit, oid }: SetStateTxConsArgs) {

        // 当某些字段值为其对应类型的默认值时，将其置为null，确保protobufjs序列化时不编码默认值字段，否则将与其他语言平台的protobuf工具不一致
        
        const iChaincodeId: IChaincodeId = {
            chaincodeName: chaincodeName || null,
            version: chaincodeVersion || null,
        };
        const chaincodeIdMsg = chaincodeIdMsgClass.create(iChaincodeId);
        const iTransaction: ITransaction = {
            id: txid || null,
            type,
            cid: chaincodeIdMsg,
            // Note for encoding with protobuf: 标准实现中oneof类型中的直接标量类型取默认值时在被序列化时依然会被编码(https://developers.google.com/protocol-buffers/docs/proto3?hl=en#oneof)，
            // 这里需要保证protobufjs对oneof中boolean类型的默认值(false)在被序列化时依然能被编码，即不能使其取null值
            state: chaincodeSetStateParams.state,
            gasLimit: gasLimit || null,
            oid: oid || null,
        };

        super(iTransaction);
    }
}