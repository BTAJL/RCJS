// To use for nodejs crypto
const SHA256withECDSA = "sha256";
export default SHA256withECDSA;

// To use for jsrsasign 
export const SHA256withECDSAForJsrsasign = "SHA256withECDSA";
