// 使用w3cwebsocket对象，兼容Browser和Node环境
import { w3cwebsocket as WebSocket } from "websocket";
type WebSocketType = WebSocket;

const defaultLogger = {
    info: (info: string) => console.log(info),
    error: (error: string) => console.error(error),
};

export class EventTube {
    private address: string;
    private cb;
    private timeout;
    private timer: NodeJS.Timeout | null;
    private ws: WebSocketType | null;
    private shouldReconnect: Boolean;
    private logger: any;

    /**
     * 构建事件订阅对象实例
     * 
     * @param address websocket服务地址
     * @param cb 处理返回信息的回调函数
     * @param timeout 重连的时间间隔
     */
    constructor(address: string, cb?: ((_: { [key: string]: any }) => void), timeout?: number, logger?: any) {
        this.address = address;
        this.cb = cb;
        this.timeout = timeout || 5000;
        this.timer = null;
        this.ws = null;
        this.shouldReconnect = false;
        this.logger = logger || defaultLogger;
        this.connect();
    }

    /**
     * 获取Websocket连接的目标节点Url 
     * @returns 目标节点Url
     */
    getAddress(): string {
        return this.address;
    }

    setCallback(cb: ((_: { [key: string]: any }) => void)) {
        this.cb = cb;
    }

    reconnect() {
        const me = this;
        this.logger.info(`To rebuild the websocket connection(with peer: ${me.address})`);
        const timeout = me.timeout;
        if (!me.timer) {
            me.timer = setTimeout(() => {
                if (me.shouldReconnect) {
                    me.connect();
                }
                else {
                    this.logger.info(`Intentionally to revoke the attempting to rebuild the websocket connection(with peer: ${me.address})`);
                }
            }, timeout);
        }
    }

    connect() {
        const me = this;
        me.timer = null;
        me.shouldReconnect = true;
        this.logger.info(`Connecting peer with websocket url: ${me.address}`);
        const ws = new WebSocket(me.address);
        me.ws = ws;
        ws.onerror = (e) => {
            this.logger.error(`Websocket connection(with peer: ${me.address}) error: ${e.message}`);
            me.reconnect();
        };
        ws.onmessage = (m) => {
            if (me.cb) {
                me.cb(m);
            }
        };
        ws.onopen = () => {
            this.logger.info(`Built websocket connection(with peer: ${me.address})`);
        };
        ws.onclose = (e) => {
            if (e.code !== 4000) {
                this.logger.error(`Webscoket connection(with peer: ${me.address}) closed with the code: ${e.code} for the reason: ${e.reason}`);
                me.reconnect();
            } else {
                this.logger.info(`Disconnected websocket connection(with peer: ${me.address}) intentionally, with the code ${e.code} for the reason: ${e.reason}`);
            }
        };
    }

    /**
     * 主动关闭websocket连接
     * 
     * @param reason 解释主动关闭连接的原因，不超过123字节
     * @param code 主动关闭连接的状态码
     */
    close(reason: string, code = 4000) {
        if (!this.ws) {
            throw new Error("No websocket connection yet");
        }
        if (code === 4000) {
            // Not to reconnect when meet error 
            this.shouldReconnect = false;
        }
        this.ws.close(code, reason);
    }
}
