// 在package.json中的browser属性中设置{lib/rest.js : lib/browser/rest.js}以使用相应环境下的实现
import _ from "lodash";
import restSendTX from "./restSendTX";
import restGet from "./restGet";
import { rep } from "./protos/rc2";

/** 区块链的概要信息 */
export interface ChainInfo {
    /** 最新区块高度 */
    height: number;
    /** 最新区块哈希值(base64编码字符串) */
    currentBlockHash: string;
    /** 最新区块的父区块哈希值(base64编码字符串) */
    previousBlockHash: string;
    /** 世界状态哈希值(base64编码字符串) */
    currentStateHash: string;
    /** 交易总量 */
    totalTransactions: number;
}

export enum RESPONSE_FORMAT {
    JSON,
    STREAM
}

export class Client {
    private _address: string;

    /**
     * Creates an instance of RestAPI.
     * <br />
     * 与RepChain区块链节点交互的客户端.
     * 
     * @param address RepChain节点的Restful API服务地址
     * @returns Client对象实例
     */
    constructor(address: string) {
        this._address = this._sanitizeUrl(address);
    }

    _sanitizeUrl(address: string): string {
        return address.replace(/\/$/, "");
    }

    /**
     * 获取客户端欲连接的节点Url
     * @returns 客户端欲连接的节点Url
     */
    getAddress(): string {
        return this._address;
    }

    /**
     * 修改客户端欲连接的节点Url
     * @param address 欲连接的节点Url
     */
    setAddress(address: string) {
        this._address = this._sanitizeUrl(address);
    }

    /**
     * 获取区块链的当前概要信息
     *
     * @returns Json格式信息
     */
    chainInfo(): Promise<ChainInfo> {
        const url = `${this._address}/chaininfo`;
        return restGet(url).then((chainInfo) => {
            const blockchainInfoMessage = rep.proto.BlockchainInfo.fromObject(chainInfo);
            return rep.proto.BlockchainInfo.toObject(blockchainInfoMessage, { longs: Number, bytes: String }) as ChainInfo;
        });
    }

    /**
     * 获取区块链最新区块高度
     * 
     * @returns 区块高度
     */
    chainHeight(): Promise<number> {
        return this.chainInfo().then(chainInfo => chainInfo.height);
    }

    /**
     * 获取最新区块哈希值
     *
     * @returns 区块哈希值(base64编码字符串)
     */
    chainCurrentBlockHash(): Promise<string> {
        return this.chainInfo().then(chainInfo => chainInfo.currentBlockHash);
    }

    /**
     * 获取最新区块的父区块哈希值
     *
     * @returns 最新区块的父区块哈希值(base64编码字符串)
     */
    chainPreviousBlockHash(): Promise<string> {
        return this.chainInfo().then(chainInfo => chainInfo.previousBlockHash);
    }

    /**
     * 获取当前的世界状态哈希值
     *
     * @returns 世界状态哈希值(base64编码字符串)
     */
    chainCurrentStateHash(): Promise<string> {
        return this.chainInfo().then(chainInfo => chainInfo.currentStateHash);
    }

    /**
     * 获取区块链中的交易总数量
     *
     * @returns 交易总量
     * @memberof RestAPI
     */
    chainTotalTransactions(): Promise<number> {
        return this.chainInfo().then(chainInfo => chainInfo.totalTransactions);
    }

    /**
     * 获取区块数据
     * 
     * @param id 区块唯一标识，可为区块高度(number)或区块哈希值(base64编码字符串或二进制数据)
     * @param [blockFormat=RESPONSE_FORMAT.JSON] 期望返回的区块数据的格式，可为RESPONSE_FORMAT.JSON(json对象，默认)或RESPONSE_FORMAT.STREAM(二进制数据)
     * @returns 区块数据
     */
    block(id: number | string | Buffer, blockFormat = RESPONSE_FORMAT.JSON): Promise<{[key: string]: any}> | Promise<Buffer> {
        let blockID = id;

        let url = `${this._address}/block`;
        if (blockFormat === RESPONSE_FORMAT.STREAM) {
            url = `${url}/stream`;
        }
        if (_.isString(blockID) || Buffer.isBuffer(blockID)) {
            url = `${url}/hash`;
            if (Buffer.isBuffer(blockID)) blockID = blockID.toString("base64");
        }
        url = `${url}/${blockID}`;

        return restGet(url);
    }

    /**
     * 获取交易数据
     *
     * @param id 交易唯一标识，即txid
     * @param [txFormat=REPONSE_FORMAT.JSON] 期望返回的交易数据的格式JSON或STREAM
     * @param [withBlockHeight=false] 返回交易数据中是否包含区块高度，当txFormat=时才有效
     * @returns 交易数据
     */
    transaction(id: string, txFormat = RESPONSE_FORMAT.JSON, withBlockHeight = false): Promise<{[key: string]: any}> | Promise<Buffer> {
        let url = `${this._address}/transaction`;
        if (txFormat === RESPONSE_FORMAT.STREAM) {
            url = `${url}/stream`;
        } else if (withBlockHeight) {
            url = `${url}/tranInfoAndHeight`;
        }
        url = `${url}/${id}`;

        return restGet(url);
    }

    /**
     * 发送签名交易
     * 
     * @param tx 待发送的已签名交易数据，支持使用Buffer类型数据或其hex编码字符串数据
     * @returns 接收交易后RepChain节点的返回信息
     */
    sendTransaction(tx: Buffer | string): Promise<{ txid: string }> | Promise<{ err: string }> {
        return restSendTX(tx, this._address);
    }
}

export type RestAPI = Client;
