import * as $protobuf from "protobufjs";
/** Namespace rep. */
export namespace rep {

    /** Namespace proto. */
    namespace proto {

        /** Properties of an Event. */
        interface IEvent {

            /** Event from */
            from?: (string|null);

            /** Event to */
            to?: (string|null);

            /** Event action */
            action?: (rep.proto.Event.Action|null);

            /** Event blk */
            blk?: (rep.proto.IBlock|null);
        }

        /** Represents an Event. */
        class Event implements IEvent {

            /**
             * Constructs a new Event.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IEvent);

            /** Event from. */
            public from: string;

            /** Event to. */
            public to: string;

            /** Event action. */
            public action: rep.proto.Event.Action;

            /** Event blk. */
            public blk?: (rep.proto.IBlock|null);

            /**
             * Creates a new Event instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Event instance
             */
            public static create(properties?: rep.proto.IEvent): rep.proto.Event;

            /**
             * Encodes the specified Event message. Does not implicitly {@link rep.proto.Event.verify|verify} messages.
             * @param message Event message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IEvent, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Event message, length delimited. Does not implicitly {@link rep.proto.Event.verify|verify} messages.
             * @param message Event message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IEvent, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an Event message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Event
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.Event;

            /**
             * Decodes an Event message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Event
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.Event;

            /**
             * Verifies an Event message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an Event message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Event
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.Event;

            /**
             * Creates a plain object from an Event message. Also converts values to other types if specified.
             * @param message Event
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.Event, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Event to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace Event {

            /** Action enum. */
            enum Action {
                SUBSCRIBE_TOPIC = 0,
                TRANSACTION = 1,
                BLOCK_NEW = 2,
                BLOCK_ENDORSEMENT = 3,
                ENDORSEMENT = 4,
                MEMBER_UP = 5,
                MEMBER_DOWN = 6,
                CANDIDATOR = 7,
                GENESIS_BLOCK = 8,
                BLOCK_SYNC = 9,
                BLOCK_SYNC_DATA = 10,
                BLOCK_SYNC_SUC = 11
            }
        }

        /** Properties of a Signer. */
        interface ISigner {

            /** Signer name */
            name?: (string|null);

            /** Signer creditCode */
            creditCode?: (string|null);

            /** Signer mobile */
            mobile?: (string|null);

            /** Signer certNames */
            certNames?: (string[]|null);

            /** Signer authorizeIds */
            authorizeIds?: (string[]|null);

            /** Signer operateIds */
            operateIds?: (string[]|null);

            /** Signer credentialMetadataIds */
            credentialMetadataIds?: (string[]|null);

            /** Signer authenticationCerts */
            authenticationCerts?: (rep.proto.ICertificate[]|null);

            /** Signer signerInfo */
            signerInfo?: (string|null);

            /** Signer createTime */
            createTime?: (google.protobuf.ITimestamp|null);

            /** Signer disableTime */
            disableTime?: (google.protobuf.ITimestamp|null);

            /** Signer signerValid */
            signerValid?: (boolean|null);

            /** Signer version */
            version?: (string|null);
        }

        /** Represents a Signer. */
        class Signer implements ISigner {

            /**
             * Constructs a new Signer.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.ISigner);

            /** Signer name. */
            public name: string;

            /** Signer creditCode. */
            public creditCode: string;

            /** Signer mobile. */
            public mobile: string;

            /** Signer certNames. */
            public certNames: string[];

            /** Signer authorizeIds. */
            public authorizeIds: string[];

            /** Signer operateIds. */
            public operateIds: string[];

            /** Signer credentialMetadataIds. */
            public credentialMetadataIds: string[];

            /** Signer authenticationCerts. */
            public authenticationCerts: rep.proto.ICertificate[];

            /** Signer signerInfo. */
            public signerInfo: string;

            /** Signer createTime. */
            public createTime?: (google.protobuf.ITimestamp|null);

            /** Signer disableTime. */
            public disableTime?: (google.protobuf.ITimestamp|null);

            /** Signer signerValid. */
            public signerValid: boolean;

            /** Signer version. */
            public version: string;

            /**
             * Creates a new Signer instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Signer instance
             */
            public static create(properties?: rep.proto.ISigner): rep.proto.Signer;

            /**
             * Encodes the specified Signer message. Does not implicitly {@link rep.proto.Signer.verify|verify} messages.
             * @param message Signer message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.ISigner, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Signer message, length delimited. Does not implicitly {@link rep.proto.Signer.verify|verify} messages.
             * @param message Signer message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.ISigner, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Signer message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Signer
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.Signer;

            /**
             * Decodes a Signer message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Signer
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.Signer;

            /**
             * Verifies a Signer message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Signer message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Signer
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.Signer;

            /**
             * Creates a plain object from a Signer message. Also converts values to other types if specified.
             * @param message Signer
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.Signer, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Signer to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a CertId. */
        interface ICertId {

            /** CertId creditCode */
            creditCode?: (string|null);

            /** CertId certName */
            certName?: (string|null);

            /** CertId version */
            version?: (string|null);
        }

        /** Represents a CertId. */
        class CertId implements ICertId {

            /**
             * Constructs a new CertId.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.ICertId);

            /** CertId creditCode. */
            public creditCode: string;

            /** CertId certName. */
            public certName: string;

            /** CertId version. */
            public version: string;

            /**
             * Creates a new CertId instance using the specified properties.
             * @param [properties] Properties to set
             * @returns CertId instance
             */
            public static create(properties?: rep.proto.ICertId): rep.proto.CertId;

            /**
             * Encodes the specified CertId message. Does not implicitly {@link rep.proto.CertId.verify|verify} messages.
             * @param message CertId message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.ICertId, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified CertId message, length delimited. Does not implicitly {@link rep.proto.CertId.verify|verify} messages.
             * @param message CertId message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.ICertId, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a CertId message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns CertId
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.CertId;

            /**
             * Decodes a CertId message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns CertId
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.CertId;

            /**
             * Verifies a CertId message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a CertId message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns CertId
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.CertId;

            /**
             * Creates a plain object from a CertId message. Also converts values to other types if specified.
             * @param message CertId
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.CertId, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this CertId to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a Certificate. */
        interface ICertificate {

            /** Certificate certificate */
            certificate?: (string|null);

            /** Certificate algType */
            algType?: (string|null);

            /** Certificate certValid */
            certValid?: (boolean|null);

            /** Certificate reg_Time */
            reg_Time?: (google.protobuf.ITimestamp|null);

            /** Certificate unreg_Time */
            unreg_Time?: (google.protobuf.ITimestamp|null);

            /** Certificate certType */
            certType?: (rep.proto.Certificate.CertType|null);

            /** Certificate id */
            id?: (rep.proto.ICertId|null);

            /** Certificate certHash */
            certHash?: (string|null);

            /** Certificate version */
            version?: (string|null);
        }

        /** Represents a Certificate. */
        class Certificate implements ICertificate {

            /**
             * Constructs a new Certificate.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.ICertificate);

            /** Certificate certificate. */
            public certificate: string;

            /** Certificate algType. */
            public algType: string;

            /** Certificate certValid. */
            public certValid: boolean;

            /** Certificate reg_Time. */
            public reg_Time?: (google.protobuf.ITimestamp|null);

            /** Certificate unreg_Time. */
            public unreg_Time?: (google.protobuf.ITimestamp|null);

            /** Certificate certType. */
            public certType: rep.proto.Certificate.CertType;

            /** Certificate id. */
            public id?: (rep.proto.ICertId|null);

            /** Certificate certHash. */
            public certHash: string;

            /** Certificate version. */
            public version: string;

            /**
             * Creates a new Certificate instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Certificate instance
             */
            public static create(properties?: rep.proto.ICertificate): rep.proto.Certificate;

            /**
             * Encodes the specified Certificate message. Does not implicitly {@link rep.proto.Certificate.verify|verify} messages.
             * @param message Certificate message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.ICertificate, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Certificate message, length delimited. Does not implicitly {@link rep.proto.Certificate.verify|verify} messages.
             * @param message Certificate message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.ICertificate, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Certificate message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Certificate
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.Certificate;

            /**
             * Decodes a Certificate message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Certificate
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.Certificate;

            /**
             * Verifies a Certificate message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Certificate message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Certificate
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.Certificate;

            /**
             * Creates a plain object from a Certificate message. Also converts values to other types if specified.
             * @param message Certificate
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.Certificate, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Certificate to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace Certificate {

            /** CertType enum. */
            enum CertType {
                CERT_UNDEFINED = 0,
                CERT_AUTHENTICATION = 1,
                CERT_CUSTOM = 2
            }
        }

        /** Properties of an Operate. */
        interface IOperate {

            /** Operate opId */
            opId?: (string|null);

            /** Operate description */
            description?: (string|null);

            /** Operate register */
            register?: (string|null);

            /** Operate isPublish */
            isPublish?: (boolean|null);

            /** Operate operateType */
            operateType?: (rep.proto.Operate.OperateType|null);

            /** Operate operateServiceName */
            operateServiceName?: (string[]|null);

            /** Operate operateEndpoint */
            operateEndpoint?: (string|null);

            /** Operate authFullName */
            authFullName?: (string|null);

            /** Operate createTime */
            createTime?: (google.protobuf.ITimestamp|null);

            /** Operate disableTime */
            disableTime?: (google.protobuf.ITimestamp|null);

            /** Operate opValid */
            opValid?: (boolean|null);

            /** Operate version */
            version?: (string|null);
        }

        /** Represents an Operate. */
        class Operate implements IOperate {

            /**
             * Constructs a new Operate.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IOperate);

            /** Operate opId. */
            public opId: string;

            /** Operate description. */
            public description: string;

            /** Operate register. */
            public register: string;

            /** Operate isPublish. */
            public isPublish: boolean;

            /** Operate operateType. */
            public operateType: rep.proto.Operate.OperateType;

            /** Operate operateServiceName. */
            public operateServiceName: string[];

            /** Operate operateEndpoint. */
            public operateEndpoint: string;

            /** Operate authFullName. */
            public authFullName: string;

            /** Operate createTime. */
            public createTime?: (google.protobuf.ITimestamp|null);

            /** Operate disableTime. */
            public disableTime?: (google.protobuf.ITimestamp|null);

            /** Operate opValid. */
            public opValid: boolean;

            /** Operate version. */
            public version: string;

            /**
             * Creates a new Operate instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Operate instance
             */
            public static create(properties?: rep.proto.IOperate): rep.proto.Operate;

            /**
             * Encodes the specified Operate message. Does not implicitly {@link rep.proto.Operate.verify|verify} messages.
             * @param message Operate message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IOperate, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Operate message, length delimited. Does not implicitly {@link rep.proto.Operate.verify|verify} messages.
             * @param message Operate message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IOperate, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an Operate message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Operate
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.Operate;

            /**
             * Decodes an Operate message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Operate
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.Operate;

            /**
             * Verifies an Operate message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an Operate message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Operate
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.Operate;

            /**
             * Creates a plain object from an Operate message. Also converts values to other types if specified.
             * @param message Operate
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.Operate, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Operate to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace Operate {

            /** OperateType enum. */
            enum OperateType {
                OPERATE_UNDEFINED = 0,
                OPERATE_CONTRACT = 1,
                OPERATE_SERVICE = 2
            }
        }

        /** Properties of an Authorize. */
        interface IAuthorize {

            /** Authorize id */
            id?: (string|null);

            /** Authorize grant */
            grant?: (string|null);

            /** Authorize granted */
            granted?: (string[]|null);

            /** Authorize opId */
            opId?: (string[]|null);

            /** Authorize isTransfer */
            isTransfer?: (rep.proto.Authorize.TransferType|null);

            /** Authorize createTime */
            createTime?: (google.protobuf.ITimestamp|null);

            /** Authorize disableTime */
            disableTime?: (google.protobuf.ITimestamp|null);

            /** Authorize authorizeValid */
            authorizeValid?: (boolean|null);

            /** Authorize version */
            version?: (string|null);
        }

        /** Represents an Authorize. */
        class Authorize implements IAuthorize {

            /**
             * Constructs a new Authorize.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IAuthorize);

            /** Authorize id. */
            public id: string;

            /** Authorize grant. */
            public grant: string;

            /** Authorize granted. */
            public granted: string[];

            /** Authorize opId. */
            public opId: string[];

            /** Authorize isTransfer. */
            public isTransfer: rep.proto.Authorize.TransferType;

            /** Authorize createTime. */
            public createTime?: (google.protobuf.ITimestamp|null);

            /** Authorize disableTime. */
            public disableTime?: (google.protobuf.ITimestamp|null);

            /** Authorize authorizeValid. */
            public authorizeValid: boolean;

            /** Authorize version. */
            public version: string;

            /**
             * Creates a new Authorize instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Authorize instance
             */
            public static create(properties?: rep.proto.IAuthorize): rep.proto.Authorize;

            /**
             * Encodes the specified Authorize message. Does not implicitly {@link rep.proto.Authorize.verify|verify} messages.
             * @param message Authorize message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IAuthorize, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Authorize message, length delimited. Does not implicitly {@link rep.proto.Authorize.verify|verify} messages.
             * @param message Authorize message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IAuthorize, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an Authorize message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Authorize
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.Authorize;

            /**
             * Decodes an Authorize message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Authorize
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.Authorize;

            /**
             * Verifies an Authorize message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an Authorize message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Authorize
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.Authorize;

            /**
             * Creates a plain object from an Authorize message. Also converts values to other types if specified.
             * @param message Authorize
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.Authorize, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Authorize to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace Authorize {

            /** TransferType enum. */
            enum TransferType {
                TRANSFER_DISABLE = 0,
                TRANSFER_ONCE = 1,
                TRANSFER_REPEATEDLY = 2
            }
        }

        /** Properties of a BindCertToAuthorize. */
        interface IBindCertToAuthorize {

            /** BindCertToAuthorize authorizeId */
            authorizeId?: (string|null);

            /** BindCertToAuthorize granted */
            granted?: (rep.proto.ICertId|null);

            /** BindCertToAuthorize version */
            version?: (string|null);
        }

        /** Represents a BindCertToAuthorize. */
        class BindCertToAuthorize implements IBindCertToAuthorize {

            /**
             * Constructs a new BindCertToAuthorize.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IBindCertToAuthorize);

            /** BindCertToAuthorize authorizeId. */
            public authorizeId: string;

            /** BindCertToAuthorize granted. */
            public granted?: (rep.proto.ICertId|null);

            /** BindCertToAuthorize version. */
            public version: string;

            /**
             * Creates a new BindCertToAuthorize instance using the specified properties.
             * @param [properties] Properties to set
             * @returns BindCertToAuthorize instance
             */
            public static create(properties?: rep.proto.IBindCertToAuthorize): rep.proto.BindCertToAuthorize;

            /**
             * Encodes the specified BindCertToAuthorize message. Does not implicitly {@link rep.proto.BindCertToAuthorize.verify|verify} messages.
             * @param message BindCertToAuthorize message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IBindCertToAuthorize, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified BindCertToAuthorize message, length delimited. Does not implicitly {@link rep.proto.BindCertToAuthorize.verify|verify} messages.
             * @param message BindCertToAuthorize message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IBindCertToAuthorize, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a BindCertToAuthorize message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns BindCertToAuthorize
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.BindCertToAuthorize;

            /**
             * Decodes a BindCertToAuthorize message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns BindCertToAuthorize
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.BindCertToAuthorize;

            /**
             * Verifies a BindCertToAuthorize message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a BindCertToAuthorize message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns BindCertToAuthorize
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.BindCertToAuthorize;

            /**
             * Creates a plain object from a BindCertToAuthorize message. Also converts values to other types if specified.
             * @param message BindCertToAuthorize
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.BindCertToAuthorize, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this BindCertToAuthorize to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a CreClaStruct. */
        interface ICreClaStruct {

            /** CreClaStruct version */
            version?: (string|null);

            /** CreClaStruct id */
            id?: (string|null);

            /** CreClaStruct name */
            name?: (string|null);

            /** CreClaStruct ccsVersion */
            ccsVersion?: (string|null);

            /** CreClaStruct description */
            description?: (string|null);

            /** CreClaStruct creator */
            creator?: (string|null);

            /** CreClaStruct created */
            created?: (string|null);

            /** CreClaStruct valid */
            valid?: (boolean|null);

            /** CreClaStruct attributes */
            attributes?: (rep.proto.ICreAttr[]|null);
        }

        /** Represents a CreClaStruct. */
        class CreClaStruct implements ICreClaStruct {

            /**
             * Constructs a new CreClaStruct.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.ICreClaStruct);

            /** CreClaStruct version. */
            public version: string;

            /** CreClaStruct id. */
            public id: string;

            /** CreClaStruct name. */
            public name: string;

            /** CreClaStruct ccsVersion. */
            public ccsVersion: string;

            /** CreClaStruct description. */
            public description: string;

            /** CreClaStruct creator. */
            public creator: string;

            /** CreClaStruct created. */
            public created: string;

            /** CreClaStruct valid. */
            public valid: boolean;

            /** CreClaStruct attributes. */
            public attributes: rep.proto.ICreAttr[];

            /**
             * Creates a new CreClaStruct instance using the specified properties.
             * @param [properties] Properties to set
             * @returns CreClaStruct instance
             */
            public static create(properties?: rep.proto.ICreClaStruct): rep.proto.CreClaStruct;

            /**
             * Encodes the specified CreClaStruct message. Does not implicitly {@link rep.proto.CreClaStruct.verify|verify} messages.
             * @param message CreClaStruct message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.ICreClaStruct, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified CreClaStruct message, length delimited. Does not implicitly {@link rep.proto.CreClaStruct.verify|verify} messages.
             * @param message CreClaStruct message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.ICreClaStruct, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a CreClaStruct message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns CreClaStruct
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.CreClaStruct;

            /**
             * Decodes a CreClaStruct message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns CreClaStruct
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.CreClaStruct;

            /**
             * Verifies a CreClaStruct message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a CreClaStruct message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns CreClaStruct
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.CreClaStruct;

            /**
             * Creates a plain object from a CreClaStruct message. Also converts values to other types if specified.
             * @param message CreClaStruct
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.CreClaStruct, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this CreClaStruct to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a CreAttr. */
        interface ICreAttr {

            /** CreAttr name */
            name?: (string|null);

            /** CreAttr type */
            type?: (string|null);

            /** CreAttr required */
            required?: (boolean|null);

            /** CreAttr description */
            description?: (string|null);
        }

        /** Represents a CreAttr. */
        class CreAttr implements ICreAttr {

            /**
             * Constructs a new CreAttr.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.ICreAttr);

            /** CreAttr name. */
            public name: string;

            /** CreAttr type. */
            public type: string;

            /** CreAttr required. */
            public required: boolean;

            /** CreAttr description. */
            public description: string;

            /**
             * Creates a new CreAttr instance using the specified properties.
             * @param [properties] Properties to set
             * @returns CreAttr instance
             */
            public static create(properties?: rep.proto.ICreAttr): rep.proto.CreAttr;

            /**
             * Encodes the specified CreAttr message. Does not implicitly {@link rep.proto.CreAttr.verify|verify} messages.
             * @param message CreAttr message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.ICreAttr, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified CreAttr message, length delimited. Does not implicitly {@link rep.proto.CreAttr.verify|verify} messages.
             * @param message CreAttr message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.ICreAttr, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a CreAttr message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns CreAttr
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.CreAttr;

            /**
             * Decodes a CreAttr message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns CreAttr
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.CreAttr;

            /**
             * Verifies a CreAttr message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a CreAttr message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns CreAttr
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.CreAttr;

            /**
             * Creates a plain object from a CreAttr message. Also converts values to other types if specified.
             * @param message CreAttr
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.CreAttr, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this CreAttr to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a VerCreStatus. */
        interface IVerCreStatus {

            /** VerCreStatus version */
            version?: (string|null);

            /** VerCreStatus id */
            id?: (string|null);

            /** VerCreStatus status */
            status?: (string|null);

            /** VerCreStatus revokedClaimIndex */
            revokedClaimIndex?: (string[]|null);

            /** VerCreStatus creator */
            creator?: (string|null);
        }

        /** Represents a VerCreStatus. */
        class VerCreStatus implements IVerCreStatus {

            /**
             * Constructs a new VerCreStatus.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IVerCreStatus);

            /** VerCreStatus version. */
            public version: string;

            /** VerCreStatus id. */
            public id: string;

            /** VerCreStatus status. */
            public status: string;

            /** VerCreStatus revokedClaimIndex. */
            public revokedClaimIndex: string[];

            /** VerCreStatus creator. */
            public creator: string;

            /**
             * Creates a new VerCreStatus instance using the specified properties.
             * @param [properties] Properties to set
             * @returns VerCreStatus instance
             */
            public static create(properties?: rep.proto.IVerCreStatus): rep.proto.VerCreStatus;

            /**
             * Encodes the specified VerCreStatus message. Does not implicitly {@link rep.proto.VerCreStatus.verify|verify} messages.
             * @param message VerCreStatus message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IVerCreStatus, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified VerCreStatus message, length delimited. Does not implicitly {@link rep.proto.VerCreStatus.verify|verify} messages.
             * @param message VerCreStatus message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IVerCreStatus, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a VerCreStatus message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns VerCreStatus
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.VerCreStatus;

            /**
             * Decodes a VerCreStatus message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns VerCreStatus
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.VerCreStatus;

            /**
             * Verifies a VerCreStatus message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a VerCreStatus message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns VerCreStatus
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.VerCreStatus;

            /**
             * Creates a plain object from a VerCreStatus message. Also converts values to other types if specified.
             * @param message VerCreStatus
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.VerCreStatus, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this VerCreStatus to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a Signature. */
        interface ISignature {

            /** Signature certId */
            certId?: (rep.proto.ICertId|null);

            /** Signature tmLocal */
            tmLocal?: (google.protobuf.ITimestamp|null);

            /** Signature signature */
            signature?: (Uint8Array|null);
        }

        /** Represents a Signature. */
        class Signature implements ISignature {

            /**
             * Constructs a new Signature.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.ISignature);

            /** Signature certId. */
            public certId?: (rep.proto.ICertId|null);

            /** Signature tmLocal. */
            public tmLocal?: (google.protobuf.ITimestamp|null);

            /** Signature signature. */
            public signature: Uint8Array;

            /**
             * Creates a new Signature instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Signature instance
             */
            public static create(properties?: rep.proto.ISignature): rep.proto.Signature;

            /**
             * Encodes the specified Signature message. Does not implicitly {@link rep.proto.Signature.verify|verify} messages.
             * @param message Signature message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.ISignature, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Signature message, length delimited. Does not implicitly {@link rep.proto.Signature.verify|verify} messages.
             * @param message Signature message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.ISignature, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Signature message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Signature
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.Signature;

            /**
             * Decodes a Signature message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Signature
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.Signature;

            /**
             * Verifies a Signature message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Signature message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Signature
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.Signature;

            /**
             * Creates a plain object from a Signature message. Also converts values to other types if specified.
             * @param message Signature
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.Signature, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Signature to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a ChaincodeInput. */
        interface IChaincodeInput {

            /** ChaincodeInput function */
            "function"?: (string|null);

            /** ChaincodeInput args */
            args?: (string[]|null);
        }

        /** Represents a ChaincodeInput. */
        class ChaincodeInput implements IChaincodeInput {

            /**
             * Constructs a new ChaincodeInput.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IChaincodeInput);

            /** ChaincodeInput function. */
            public function: string;

            /** ChaincodeInput args. */
            public args: string[];

            /**
             * Creates a new ChaincodeInput instance using the specified properties.
             * @param [properties] Properties to set
             * @returns ChaincodeInput instance
             */
            public static create(properties?: rep.proto.IChaincodeInput): rep.proto.ChaincodeInput;

            /**
             * Encodes the specified ChaincodeInput message. Does not implicitly {@link rep.proto.ChaincodeInput.verify|verify} messages.
             * @param message ChaincodeInput message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IChaincodeInput, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified ChaincodeInput message, length delimited. Does not implicitly {@link rep.proto.ChaincodeInput.verify|verify} messages.
             * @param message ChaincodeInput message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IChaincodeInput, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a ChaincodeInput message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns ChaincodeInput
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.ChaincodeInput;

            /**
             * Decodes a ChaincodeInput message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns ChaincodeInput
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.ChaincodeInput;

            /**
             * Verifies a ChaincodeInput message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a ChaincodeInput message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns ChaincodeInput
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.ChaincodeInput;

            /**
             * Creates a plain object from a ChaincodeInput message. Also converts values to other types if specified.
             * @param message ChaincodeInput
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.ChaincodeInput, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this ChaincodeInput to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a ChaincodeId. */
        interface IChaincodeId {

            /** ChaincodeId chaincodeName */
            chaincodeName?: (string|null);

            /** ChaincodeId version */
            version?: (number|null);
        }

        /** Represents a ChaincodeId. */
        class ChaincodeId implements IChaincodeId {

            /**
             * Constructs a new ChaincodeId.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IChaincodeId);

            /** ChaincodeId chaincodeName. */
            public chaincodeName: string;

            /** ChaincodeId version. */
            public version: number;

            /**
             * Creates a new ChaincodeId instance using the specified properties.
             * @param [properties] Properties to set
             * @returns ChaincodeId instance
             */
            public static create(properties?: rep.proto.IChaincodeId): rep.proto.ChaincodeId;

            /**
             * Encodes the specified ChaincodeId message. Does not implicitly {@link rep.proto.ChaincodeId.verify|verify} messages.
             * @param message ChaincodeId message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IChaincodeId, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified ChaincodeId message, length delimited. Does not implicitly {@link rep.proto.ChaincodeId.verify|verify} messages.
             * @param message ChaincodeId message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IChaincodeId, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a ChaincodeId message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns ChaincodeId
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.ChaincodeId;

            /**
             * Decodes a ChaincodeId message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns ChaincodeId
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.ChaincodeId;

            /**
             * Verifies a ChaincodeId message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a ChaincodeId message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns ChaincodeId
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.ChaincodeId;

            /**
             * Creates a plain object from a ChaincodeId message. Also converts values to other types if specified.
             * @param message ChaincodeId
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.ChaincodeId, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this ChaincodeId to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a Block. */
        interface IBlock {

            /** Block header */
            header?: (rep.proto.IBlockHeader|null);

            /** Block transactions */
            transactions?: (rep.proto.ITransaction[]|null);

            /** Block transactionResults */
            transactionResults?: (rep.proto.ITransactionResult[]|null);

            /** Block regTx */
            regTx?: (rep.proto.ITransaction|null);
        }

        /** Represents a Block. */
        class Block implements IBlock {

            /**
             * Constructs a new Block.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IBlock);

            /** Block header. */
            public header?: (rep.proto.IBlockHeader|null);

            /** Block transactions. */
            public transactions: rep.proto.ITransaction[];

            /** Block transactionResults. */
            public transactionResults: rep.proto.ITransactionResult[];

            /** Block regTx. */
            public regTx?: (rep.proto.ITransaction|null);

            /**
             * Creates a new Block instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Block instance
             */
            public static create(properties?: rep.proto.IBlock): rep.proto.Block;

            /**
             * Encodes the specified Block message. Does not implicitly {@link rep.proto.Block.verify|verify} messages.
             * @param message Block message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IBlock, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Block message, length delimited. Does not implicitly {@link rep.proto.Block.verify|verify} messages.
             * @param message Block message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IBlock, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Block message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Block
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.Block;

            /**
             * Decodes a Block message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Block
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.Block;

            /**
             * Verifies a Block message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Block message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Block
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.Block;

            /**
             * Creates a plain object from a Block message. Also converts values to other types if specified.
             * @param message Block
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.Block, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Block to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a BlockHeader. */
        interface IBlockHeader {

            /** BlockHeader version */
            version?: (number|null);

            /** BlockHeader height */
            height?: (number|Long|null);

            /** BlockHeader commitTx */
            commitTx?: (Uint8Array|null);

            /** BlockHeader commitTxResult */
            commitTxResult?: (Uint8Array|null);

            /** BlockHeader hashPresent */
            hashPresent?: (Uint8Array|null);

            /** BlockHeader hashPrevious */
            hashPrevious?: (Uint8Array|null);

            /** BlockHeader commitState */
            commitState?: (Uint8Array|null);

            /** BlockHeader commitStateGlobal */
            commitStateGlobal?: (Uint8Array|null);

            /** BlockHeader heightExpired */
            heightExpired?: (number|Long|null);

            /** BlockHeader endorsements */
            endorsements?: (rep.proto.ISignature[]|null);
        }

        /** Represents a BlockHeader. */
        class BlockHeader implements IBlockHeader {

            /**
             * Constructs a new BlockHeader.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IBlockHeader);

            /** BlockHeader version. */
            public version: number;

            /** BlockHeader height. */
            public height: (number|Long);

            /** BlockHeader commitTx. */
            public commitTx: Uint8Array;

            /** BlockHeader commitTxResult. */
            public commitTxResult: Uint8Array;

            /** BlockHeader hashPresent. */
            public hashPresent: Uint8Array;

            /** BlockHeader hashPrevious. */
            public hashPrevious: Uint8Array;

            /** BlockHeader commitState. */
            public commitState: Uint8Array;

            /** BlockHeader commitStateGlobal. */
            public commitStateGlobal: Uint8Array;

            /** BlockHeader heightExpired. */
            public heightExpired: (number|Long);

            /** BlockHeader endorsements. */
            public endorsements: rep.proto.ISignature[];

            /**
             * Creates a new BlockHeader instance using the specified properties.
             * @param [properties] Properties to set
             * @returns BlockHeader instance
             */
            public static create(properties?: rep.proto.IBlockHeader): rep.proto.BlockHeader;

            /**
             * Encodes the specified BlockHeader message. Does not implicitly {@link rep.proto.BlockHeader.verify|verify} messages.
             * @param message BlockHeader message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IBlockHeader, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified BlockHeader message, length delimited. Does not implicitly {@link rep.proto.BlockHeader.verify|verify} messages.
             * @param message BlockHeader message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IBlockHeader, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a BlockHeader message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns BlockHeader
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.BlockHeader;

            /**
             * Decodes a BlockHeader message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns BlockHeader
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.BlockHeader;

            /**
             * Verifies a BlockHeader message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a BlockHeader message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns BlockHeader
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.BlockHeader;

            /**
             * Creates a plain object from a BlockHeader message. Also converts values to other types if specified.
             * @param message BlockHeader
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.BlockHeader, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this BlockHeader to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a TransactionError. */
        interface ITransactionError {

            /** TransactionError txId */
            txId?: (string|null);

            /** TransactionError code */
            code?: (number|null);

            /** TransactionError reason */
            reason?: (string|null);
        }

        /** Represents a TransactionError. */
        class TransactionError implements ITransactionError {

            /**
             * Constructs a new TransactionError.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.ITransactionError);

            /** TransactionError txId. */
            public txId: string;

            /** TransactionError code. */
            public code: number;

            /** TransactionError reason. */
            public reason: string;

            /**
             * Creates a new TransactionError instance using the specified properties.
             * @param [properties] Properties to set
             * @returns TransactionError instance
             */
            public static create(properties?: rep.proto.ITransactionError): rep.proto.TransactionError;

            /**
             * Encodes the specified TransactionError message. Does not implicitly {@link rep.proto.TransactionError.verify|verify} messages.
             * @param message TransactionError message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.ITransactionError, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified TransactionError message, length delimited. Does not implicitly {@link rep.proto.TransactionError.verify|verify} messages.
             * @param message TransactionError message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.ITransactionError, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a TransactionError message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns TransactionError
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.TransactionError;

            /**
             * Decodes a TransactionError message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns TransactionError
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.TransactionError;

            /**
             * Verifies a TransactionError message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a TransactionError message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns TransactionError
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.TransactionError;

            /**
             * Creates a plain object from a TransactionError message. Also converts values to other types if specified.
             * @param message TransactionError
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.TransactionError, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this TransactionError to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a TransactionResult. */
        interface ITransactionResult {

            /** TransactionResult txId */
            txId?: (string|null);

            /** TransactionResult statesGet */
            statesGet?: ({ [k: string]: Uint8Array }|null);

            /** TransactionResult statesSet */
            statesSet?: ({ [k: string]: Uint8Array }|null);

            /** TransactionResult statesDel */
            statesDel?: ({ [k: string]: Uint8Array }|null);

            /** TransactionResult err */
            err?: (rep.proto.IActionResult|null);
        }

        /** Represents a TransactionResult. */
        class TransactionResult implements ITransactionResult {

            /**
             * Constructs a new TransactionResult.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.ITransactionResult);

            /** TransactionResult txId. */
            public txId: string;

            /** TransactionResult statesGet. */
            public statesGet: { [k: string]: Uint8Array };

            /** TransactionResult statesSet. */
            public statesSet: { [k: string]: Uint8Array };

            /** TransactionResult statesDel. */
            public statesDel: { [k: string]: Uint8Array };

            /** TransactionResult err. */
            public err?: (rep.proto.IActionResult|null);

            /**
             * Creates a new TransactionResult instance using the specified properties.
             * @param [properties] Properties to set
             * @returns TransactionResult instance
             */
            public static create(properties?: rep.proto.ITransactionResult): rep.proto.TransactionResult;

            /**
             * Encodes the specified TransactionResult message. Does not implicitly {@link rep.proto.TransactionResult.verify|verify} messages.
             * @param message TransactionResult message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.ITransactionResult, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified TransactionResult message, length delimited. Does not implicitly {@link rep.proto.TransactionResult.verify|verify} messages.
             * @param message TransactionResult message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.ITransactionResult, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a TransactionResult message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns TransactionResult
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.TransactionResult;

            /**
             * Decodes a TransactionResult message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns TransactionResult
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.TransactionResult;

            /**
             * Verifies a TransactionResult message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a TransactionResult message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns TransactionResult
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.TransactionResult;

            /**
             * Creates a plain object from a TransactionResult message. Also converts values to other types if specified.
             * @param message TransactionResult
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.TransactionResult, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this TransactionResult to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a Transaction. */
        interface ITransaction {

            /** Transaction id */
            id?: (string|null);

            /** Transaction type */
            type?: (rep.proto.Transaction.Type|null);

            /** Transaction cid */
            cid?: (rep.proto.IChaincodeId|null);

            /** Transaction spec */
            spec?: (rep.proto.IChaincodeDeploy|null);

            /** Transaction ipt */
            ipt?: (rep.proto.IChaincodeInput|null);

            /** Transaction state */
            state?: (boolean|null);

            /** Transaction gasLimit */
            gasLimit?: (number|null);

            /** Transaction oid */
            oid?: (string|null);

            /** Transaction signature */
            signature?: (rep.proto.ISignature|null);
        }

        /** Represents a Transaction. */
        class Transaction implements ITransaction {

            /**
             * Constructs a new Transaction.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.ITransaction);

            /** Transaction id. */
            public id: string;

            /** Transaction type. */
            public type: rep.proto.Transaction.Type;

            /** Transaction cid. */
            public cid?: (rep.proto.IChaincodeId|null);

            /** Transaction spec. */
            public spec?: (rep.proto.IChaincodeDeploy|null);

            /** Transaction ipt. */
            public ipt?: (rep.proto.IChaincodeInput|null);

            /** Transaction state. */
            public state?: (boolean|null);

            /** Transaction gasLimit. */
            public gasLimit: number;

            /** Transaction oid. */
            public oid: string;

            /** Transaction signature. */
            public signature?: (rep.proto.ISignature|null);

            /** Transaction para. */
            public para?: ("spec"|"ipt"|"state");

            /**
             * Creates a new Transaction instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Transaction instance
             */
            public static create(properties?: rep.proto.ITransaction): rep.proto.Transaction;

            /**
             * Encodes the specified Transaction message. Does not implicitly {@link rep.proto.Transaction.verify|verify} messages.
             * @param message Transaction message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.ITransaction, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Transaction message, length delimited. Does not implicitly {@link rep.proto.Transaction.verify|verify} messages.
             * @param message Transaction message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.ITransaction, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Transaction message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Transaction
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.Transaction;

            /**
             * Decodes a Transaction message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Transaction
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.Transaction;

            /**
             * Verifies a Transaction message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Transaction message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Transaction
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.Transaction;

            /**
             * Creates a plain object from a Transaction message. Also converts values to other types if specified.
             * @param message Transaction
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.Transaction, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Transaction to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace Transaction {

            /** Type enum. */
            enum Type {
                UNDEFINED = 0,
                CHAINCODE_DEPLOY = 1,
                CHAINCODE_INVOKE = 2,
                CHAINCODE_SET_STATE = 3
            }
        }

        /** Properties of a ChaincodeDeploy. */
        interface IChaincodeDeploy {

            /** ChaincodeDeploy timeout */
            timeout?: (number|null);

            /** ChaincodeDeploy codePackage */
            codePackage?: (string|null);

            /** ChaincodeDeploy legalProse */
            legalProse?: (string|null);

            /** ChaincodeDeploy cType */
            cType?: (rep.proto.ChaincodeDeploy.CodeType|null);

            /** ChaincodeDeploy rType */
            rType?: (rep.proto.ChaincodeDeploy.RunType|null);

            /** ChaincodeDeploy sType */
            sType?: (rep.proto.ChaincodeDeploy.StateType|null);

            /** ChaincodeDeploy initParameter */
            initParameter?: (string|null);

            /** ChaincodeDeploy cclassification */
            cclassification?: (rep.proto.ChaincodeDeploy.ContractClassification|null);
        }

        /** Represents a ChaincodeDeploy. */
        class ChaincodeDeploy implements IChaincodeDeploy {

            /**
             * Constructs a new ChaincodeDeploy.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IChaincodeDeploy);

            /** ChaincodeDeploy timeout. */
            public timeout: number;

            /** ChaincodeDeploy codePackage. */
            public codePackage: string;

            /** ChaincodeDeploy legalProse. */
            public legalProse: string;

            /** ChaincodeDeploy cType. */
            public cType: rep.proto.ChaincodeDeploy.CodeType;

            /** ChaincodeDeploy rType. */
            public rType: rep.proto.ChaincodeDeploy.RunType;

            /** ChaincodeDeploy sType. */
            public sType: rep.proto.ChaincodeDeploy.StateType;

            /** ChaincodeDeploy initParameter. */
            public initParameter: string;

            /** ChaincodeDeploy cclassification. */
            public cclassification: rep.proto.ChaincodeDeploy.ContractClassification;

            /**
             * Creates a new ChaincodeDeploy instance using the specified properties.
             * @param [properties] Properties to set
             * @returns ChaincodeDeploy instance
             */
            public static create(properties?: rep.proto.IChaincodeDeploy): rep.proto.ChaincodeDeploy;

            /**
             * Encodes the specified ChaincodeDeploy message. Does not implicitly {@link rep.proto.ChaincodeDeploy.verify|verify} messages.
             * @param message ChaincodeDeploy message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IChaincodeDeploy, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified ChaincodeDeploy message, length delimited. Does not implicitly {@link rep.proto.ChaincodeDeploy.verify|verify} messages.
             * @param message ChaincodeDeploy message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IChaincodeDeploy, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a ChaincodeDeploy message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns ChaincodeDeploy
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.ChaincodeDeploy;

            /**
             * Decodes a ChaincodeDeploy message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns ChaincodeDeploy
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.ChaincodeDeploy;

            /**
             * Verifies a ChaincodeDeploy message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a ChaincodeDeploy message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns ChaincodeDeploy
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.ChaincodeDeploy;

            /**
             * Creates a plain object from a ChaincodeDeploy message. Also converts values to other types if specified.
             * @param message ChaincodeDeploy
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.ChaincodeDeploy, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this ChaincodeDeploy to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace ChaincodeDeploy {

            /** CodeType enum. */
            enum CodeType {
                CODE_UNDEFINED = 0,
                CODE_JAVASCRIPT = 1,
                CODE_SCALA = 2,
                CODE_VCL_DLL = 3,
                CODE_VCL_EXE = 4,
                CODE_VCL_WASM = 5,
                CODE_WASM = 6
            }

            /** RunType enum. */
            enum RunType {
                RUN_UNDEFINED = 0,
                RUN_SERIAL = 1,
                RUN_PARALLEL = 2,
                RUN_OPTIONAL = 3
            }

            /** StateType enum. */
            enum StateType {
                STATE_UNDEFINED = 0,
                STATE_BLOCK = 1,
                STATE_GLOBAL = 2
            }

            /** ContractClassification enum. */
            enum ContractClassification {
                CONTRACT_UNDEFINED = 0,
                CONTRACT_SYSTEM = 1,
                CONTRACT_CUSTOM = 2
            }
        }

        /** Properties of an ActionResult. */
        interface IActionResult {

            /** ActionResult code */
            code?: (number|null);

            /** ActionResult reason */
            reason?: (string|null);
        }

        /** Represents an ActionResult. */
        class ActionResult implements IActionResult {

            /**
             * Constructs a new ActionResult.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IActionResult);

            /** ActionResult code. */
            public code: number;

            /** ActionResult reason. */
            public reason: string;

            /**
             * Creates a new ActionResult instance using the specified properties.
             * @param [properties] Properties to set
             * @returns ActionResult instance
             */
            public static create(properties?: rep.proto.IActionResult): rep.proto.ActionResult;

            /**
             * Encodes the specified ActionResult message. Does not implicitly {@link rep.proto.ActionResult.verify|verify} messages.
             * @param message ActionResult message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IActionResult, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified ActionResult message, length delimited. Does not implicitly {@link rep.proto.ActionResult.verify|verify} messages.
             * @param message ActionResult message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IActionResult, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an ActionResult message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns ActionResult
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.ActionResult;

            /**
             * Decodes an ActionResult message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns ActionResult
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.ActionResult;

            /**
             * Verifies an ActionResult message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an ActionResult message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns ActionResult
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.ActionResult;

            /**
             * Creates a plain object from an ActionResult message. Also converts values to other types if specified.
             * @param message ActionResult
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.ActionResult, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this ActionResult to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a StateProof. */
        interface IStateProof {

            /** StateProof key */
            key?: (string|null);

            /** StateProof value */
            value?: (Uint8Array|null);

            /** StateProof proof */
            proof?: (Uint8Array|null);
        }

        /** Represents a StateProof. */
        class StateProof implements IStateProof {

            /**
             * Constructs a new StateProof.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IStateProof);

            /** StateProof key. */
            public key: string;

            /** StateProof value. */
            public value: Uint8Array;

            /** StateProof proof. */
            public proof: Uint8Array;

            /**
             * Creates a new StateProof instance using the specified properties.
             * @param [properties] Properties to set
             * @returns StateProof instance
             */
            public static create(properties?: rep.proto.IStateProof): rep.proto.StateProof;

            /**
             * Encodes the specified StateProof message. Does not implicitly {@link rep.proto.StateProof.verify|verify} messages.
             * @param message StateProof message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IStateProof, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified StateProof message, length delimited. Does not implicitly {@link rep.proto.StateProof.verify|verify} messages.
             * @param message StateProof message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IStateProof, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a StateProof message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns StateProof
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.StateProof;

            /**
             * Decodes a StateProof message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns StateProof
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.StateProof;

            /**
             * Verifies a StateProof message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a StateProof message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns StateProof
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.StateProof;

            /**
             * Creates a plain object from a StateProof message. Also converts values to other types if specified.
             * @param message StateProof
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.StateProof, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this StateProof to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a BlockchainInfo. */
        interface IBlockchainInfo {

            /** BlockchainInfo height */
            height?: (number|Long|null);

            /** BlockchainInfo totalTransactions */
            totalTransactions?: (number|Long|null);

            /** BlockchainInfo currentBlockHash */
            currentBlockHash?: (Uint8Array|null);

            /** BlockchainInfo previousBlockHash */
            previousBlockHash?: (Uint8Array|null);

            /** BlockchainInfo currentStateHash */
            currentStateHash?: (Uint8Array|null);
        }

        /** Represents a BlockchainInfo. */
        class BlockchainInfo implements IBlockchainInfo {

            /**
             * Constructs a new BlockchainInfo.
             * @param [properties] Properties to set
             */
            constructor(properties?: rep.proto.IBlockchainInfo);

            /** BlockchainInfo height. */
            public height: (number|Long);

            /** BlockchainInfo totalTransactions. */
            public totalTransactions: (number|Long);

            /** BlockchainInfo currentBlockHash. */
            public currentBlockHash: Uint8Array;

            /** BlockchainInfo previousBlockHash. */
            public previousBlockHash: Uint8Array;

            /** BlockchainInfo currentStateHash. */
            public currentStateHash: Uint8Array;

            /**
             * Creates a new BlockchainInfo instance using the specified properties.
             * @param [properties] Properties to set
             * @returns BlockchainInfo instance
             */
            public static create(properties?: rep.proto.IBlockchainInfo): rep.proto.BlockchainInfo;

            /**
             * Encodes the specified BlockchainInfo message. Does not implicitly {@link rep.proto.BlockchainInfo.verify|verify} messages.
             * @param message BlockchainInfo message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: rep.proto.IBlockchainInfo, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified BlockchainInfo message, length delimited. Does not implicitly {@link rep.proto.BlockchainInfo.verify|verify} messages.
             * @param message BlockchainInfo message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: rep.proto.IBlockchainInfo, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a BlockchainInfo message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns BlockchainInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): rep.proto.BlockchainInfo;

            /**
             * Decodes a BlockchainInfo message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns BlockchainInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): rep.proto.BlockchainInfo;

            /**
             * Verifies a BlockchainInfo message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a BlockchainInfo message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns BlockchainInfo
             */
            public static fromObject(object: { [k: string]: any }): rep.proto.BlockchainInfo;

            /**
             * Creates a plain object from a BlockchainInfo message. Also converts values to other types if specified.
             * @param message BlockchainInfo
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: rep.proto.BlockchainInfo, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this BlockchainInfo to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }
    }
}

/** Namespace google. */
export namespace google {

    /** Namespace protobuf. */
    namespace protobuf {

        /** Properties of a Timestamp. */
        interface ITimestamp {

            /** Timestamp seconds */
            seconds?: (number|Long|null);

            /** Timestamp nanos */
            nanos?: (number|null);
        }

        /** Represents a Timestamp. */
        class Timestamp implements ITimestamp {

            /**
             * Constructs a new Timestamp.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.ITimestamp);

            /** Timestamp seconds. */
            public seconds: (number|Long);

            /** Timestamp nanos. */
            public nanos: number;

            /**
             * Creates a new Timestamp instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Timestamp instance
             */
            public static create(properties?: google.protobuf.ITimestamp): google.protobuf.Timestamp;

            /**
             * Encodes the specified Timestamp message. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
             * @param message Timestamp message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.ITimestamp, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Timestamp message, length delimited. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
             * @param message Timestamp message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.ITimestamp, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Timestamp message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Timestamp
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Timestamp;

            /**
             * Decodes a Timestamp message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Timestamp
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Timestamp;

            /**
             * Verifies a Timestamp message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Timestamp message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Timestamp
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Timestamp;

            /**
             * Creates a plain object from a Timestamp message. Also converts values to other types if specified.
             * @param message Timestamp
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Timestamp, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Timestamp to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }
    }
}
